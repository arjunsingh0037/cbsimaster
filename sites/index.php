<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once dirname(__FILE__)."/../config.php";
require_once dirname(__FILE__)."/../config-multi.php";

$maindomain = $MULTI->maindomain;
$confdir = $MULTI->confdir;
$alert = "";

// get subdomain
$url = $_SERVER['HTTP_HOST'];
$subdomain = strstr(str_replace(array('www.', $maindomain),"",$url), ".",true);

if(!empty($subdomain)) {
    header("Location: http://" . $maindomain);
    die();
}

if(!is_siteadmin())
{
    header("Location: http://" . $maindomain);
    die();
}

global $DB, $CFG;

// new site request, delete, disable, or enable
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST['newsite'])) {
        $newsite = $_POST['newsite'];
        $newsite = preg_replace('/\PL/u', '', $newsite);

        if(is_dir("$confdir/$newsite")) {
            $alert = "siteexists";
        } else {

            mkdir("$confdir/$newsite", 0775);

            $alert = "sitecreated";
        }

    } else if(!empty($_POST['delete'])) {
        $delete = $_POST['delete'];

        if(file_exists("$confdir/$delete/config.php")) {
            $alert = "siteexistsdelete";
        } else {
            rmdir("$confdir/$delete");
            $alert = "siteremoved";
        }

    } else if(!empty($_POST['disable'])) {
	$disable_dir = "$confdir/" . $_POST['disable'];
	if(is_dir($disable_dir)) {
	    $dis_file = fopen("$disable_dir/disabled", "w");
	    $alert = "sitedisabled";
	} else {
	    $alert = "nosite";
	}

    } else if(!empty($_POST['enable'])) {
    	$enable_dir = "$confdir/" . $_POST['enable'];
	if(file_exists("$enable_dir/disabled")) {
	    unlink("$enable_dir/disabled");
	    $alert = "siteenabled";
	} else {
	    $alert = "nosite";
	}

    }
}

// Start setting up the page
$PAGE->set_context($context);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add("Sites", new moodle_url("/sites"));
$PAGE->set_pagelayout('base');
$PAGE->set_pagetype('general');
$PAGE->blocks->add_region('content');
//$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($SITE->fullname . " Client Sites");
$PAGE->set_heading($SITE->fullname . " Client Sites");
$PAGE->set_url(new moodle_url("/sites"));
// HACK WARNING!  This loads up all this page's blocks in the system context

$CFG->blockmanagerclass = 'shop_syspage_block_manager';


echo $OUTPUT->header();

echo "<h3>List of client websites</h3>";

if($alert === "siteexists") {
    echo "<p class='alertfail'>Site folder already exists for $newsite. Make sure the new site is unique and plain text.</p>";
} else if($alert === "sitecreated") {
    echo "<p class='alertsuccess'>Site folder created and ready to install. <a href='http://$newsite.$maindomain'>$newsite.$maindomain</a></p>";
} else if($alert === "siteexistsdelete") {
    echo "<p class='alertfail'>Site is already installed or partially installed. Must remove manually.</p>";
} else if($alert === "siteremoved") {
    echo "<p class='alertsuccess'>Site folder has been removed.</p>";
} else if($alert === "sitedisabled") {
    echo "<p class='alertsuccess'>Site successfully disabled</p>";
} else if($alert === "siteenabled") {
    echo "<p class='alertsuccess'>Site successfully re-enabled.</p>";
} else if($alert === "nosite") {
    echo "<p class='alertfail'>Site name not found. No action taken.</p>";
}

echo "<form method='post' action='index.php'>
        <input id='websitename' style='display:none' type='text' name='newsite' placeholder='website name'>
        <input id='addnewbutton' type='button' value='Add new' onclick='addNew()'>
        <input id='addnewsubmit' style='display:none' type='submit' value = 'Add new'>
";
echo "<table border='1'>";
echo "	<tr>
			<th>Site domain</th>
			<th>Status</th>
            <th>Action</th>
		</tr>
";

$results = scandir($confdir);

foreach ($results as $result) {

    if ($result === '.' or $result === '..') continue;

    if (is_dir($confdir . '/' . $result)) {

    	$status = "Ready to install";
    	if(file_exists($confdir . '/' . $result . '/config.php')) {
    		if(file_exists($confdir . '/' . $result . '/disabled')) {
			$status = "Disabled";
		} else {
	  		$status = "Installed";
		}
    	}

    	if($status === "Installed") {
    		$class = "installed";
            $action = "<td>
                            <form method='post' action='index.php'>
                                <input name='disable' type='hidden' value='$result'>
                                <input type='submit' class='link' value='Disable'>
                            </form>
	    	       </td>";
    	} else if($status === "Disabled") {
    		$class = "disabled";
		$action = "<td>
                            <form method='post' action='index.php'>
                                <input name='enable' type='hidden' value='$result'>
                                <input type='submit' class='link' value='Enable'>
                            </form>
	    	       </td>";

	}else {
    		$class = "ready";
            $action = "<td>
                            <form method='post' action='index.php'>
                                <input name='delete' type='hidden' value='$result'>
                                <input type='submit' class='link' value='Delete'>
                            </form>

                        </td>
            ";
    	}

        echo "<tr>
        		<td><a href='http://$result.$maindomain'>$result</a></td>
        		<td class='$class'>$status</td>
                $action
        	</tr>
        ";
    }
}

echo "</table>";

echo $OUTPUT->footer();

?>

<style type="text/css">

a
{
    cursor:pointer;
}
table td, table th
{
	padding:7px 7px;
}
table td:first-child, table th:first-child
{
	min-width:200px;
}
td.ready
{
	background-color:rgb(224, 224, 95);
}
td.installed
{
	background-color:rgb(166, 213, 148);
}
td.disabled
{
	background-color:rgb(255, 165, 0);
}
#websitename
{
    margin-right:10px;
    float:left;
}
.alertfail, .alertsuccess
{
    border-radius:7px;
    padding:3px 7px;
    display:inline-block;
}
.alertfail
{
    background-color:rgb(243, 165, 165);
}
.alertsuccess
{
    background-color:rgb(166, 213, 148);
}
input[type=submit].link
{
    margin:0;
}

</style>

<script type="text/javascript">

function addNew() {
    document.getElementById('addnewbutton').style.display = "none";
    document.getElementById('websitename').style.display = "block";
    document.getElementById('addnewsubmit').style.display = "block";
}

</script>
