<?php

require_once("$CFG->libdir/blocklib.php");

class shop_syspage_block_manager extends block_manager {
    public function load_blocks($includeinvisible = null) {
        $origcontext = $this->page->context;
        $this->page->context = context_system::instance();
        parent::load_blocks($includeinvisible);
        $this->page->context = $origcontext;
    }
}

?>