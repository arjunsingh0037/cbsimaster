<?php  // Moodle configuration file

//show this is the dev server. breaks themes when upgrading though :(
//echo '<style>body {border:5px red solid;}</style>';

error_reporting(E_ALL);
ini_set('display_errors', '1');


// ---- multisite code ----

require 'config-multi.php';

$maindomain = $MULTI->maindomain;
$confdir = $MULTI->confdir;
$edited_subdomain = "";
/* 
var_dump($_SERVER['HTTP_HOST']);
var_dump($maindomain);
var_dump($confdir); 
 */


if (isset($_SERVER['HTTP_HOST'])) {

	// check domain against list of client domains
	$client_file = "/var/moodledata/domains.xml";
	if (file_exists($client_file)) {

		$xml = new SimpleXMLElement(file_get_contents($client_file));
		
		foreach ($xml as $xmldata => $value) {
			if($_SERVER['HTTP_HOST'] == $value->data) {
				
				//var_dump($value);
				//require_once "$confdir/$value->client/config.php";
				$edited_subdomain = $value->client;
				//die();
			}
		}	
	}

	// get subdomain
	// at this point the URL is not a custom domain, it's either elighten.me or *.elighten.me
	$url = $_SERVER['HTTP_HOST'];
	$subdomain = strstr(str_replace(array('www.', $maindomain),"",$url), ".",true);
	
	// echo 'subdomain is...'.$subdomain;
}

//var_dump($edited_subdomain);
if(!empty($subdomain)) {
	if ($edited_subdomain != "") {
		$disabledpath = "$confdir/$edited_subdomain/disabled";
		$filepath = "$confdir/$edited_subdomain/config.php";
		$folderpath = "$confdir/$edited_subdomain/";
	}
	else {
		$disabledpath = "$confdir/$subdomain/disabled";
		$filepath = "$confdir/$subdomain/config.php";
		$folderpath = "$confdir/$subdomain/";
	}

	if(file_exists($disabledpath)) {
		//website was disabled due to non-payment. redirecting...
		$disable_redirect = "http://$url/disabled.php";
		header("Location: $disable_redirect");
		die();
	}

/* var_dump($filepath);
var_dump($folderpath); */



	if(file_exists($filepath)) {
		

		require_once $filepath;

	} elseif (is_dir($folderpath)) {

		// website is ready to install
		header("Location: install.php?client=" . $subdomain);
		die();

	} else {

		// website is not found
		header("Location: http://" . $MULTI->maindomain . "/404");
		die();
	}

} else {


// ---- end of multisite code ----

	unset($CFG);
	global $CFG;
	$CFG = new stdClass();

	// get all moodle $CFG values from config-multi.php
	load_config($CFG);

	require_once($MULTI->installdir . '/lib/setup.php');
}


// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!

// changes
# edited install.php only near the top and down a bit when the $config is created
# edited lib/installlib.php install_print_header() install_print_help_page() install_generate_config()

