<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace Multitenant\Core\Utils;

global $CFG;

use Multitenant\Core\Classes\Client;
use Multitenant\Core\Classes\ClientUsage;
use Multitenant\Core\Utils\FileManager;
use Exception;
use xmldb_table;
use stdClass;
use moodle_exception;
use dml_read_exception;
use core_plugin_manager;
use dml_connection_exception;
/**
 * Client manager Class
 * 
 */

final class DbManager {

    private $instance;

    /**
     * Executes a Psr\Http\Message\RequestInterface and (if applicable) automatically retries
     * when errors occur.
     *
     * @param Google_Client $client
     * @param Psr\Http\Message\RequestInterface $req
     * @return array decoded result
     * @throws Google_Service_Exception on server side error (ie: not authenticated,
     *  invalid or malformed post body, invalid url)
     * $DB->get_manager()->install_from_xmldb_file("$CFG->libdir/db/install.xml");
     */
    public function add_client_to_master(Client $client) {
        global $DB;
        self::master_table();
        try {
            $transaction = $DB->start_delegated_transaction();
            // Insert a record
            $dataobject = $client->get(true);
            $dataobject['max_datastorage'] = $client->get_client_usage()->get_max_datastorage();
            $dataobject['max_dbstorage'] = $client->get_client_usage()->get_max_dbstorage();
            $newclientid = $DB->insert_record('multitenant_master', $dataobject);
            // Assuming the both inserts work, we get to the following line.
            $transaction->allow_commit();
            return $newclientid;
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    public function update_client_to_master(Client $client) {
        global $DB;
        self::master_table();
        try {
            $transaction = $DB->start_delegated_transaction();
            // Insert a record
            $row = $client->get();
            $insert = new stdClass();
            $insert->id = $row['id'];
            $insert->name = $row['name'];
            $insert->pocname = $row['pocname'];
            $insert->pocphone = $row['pocphone'];
            $insert->email = $row['email'];
            $insert->start_date = $row['start_date'];
            $insert->end_date = $row['end_date'];
            $insert->max_users = $row['max_users'];
            $insert->max_courses = $row['max_courses'];
            $insert->site_url = $row['site_url'];
            $insert->max_dbstorage = $client->get_max_dbstorage();
            $insert->max_datastorage = $client->get_max_datastorage();
            $DB->update_record('multitenant_master', $insert);
            // Assuming the both inserts work, we get to the following line.
            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    public function get_master_table() {
        global $DB;
        self::master_table();

        return $DB->get_records('multitenant_master');
    }

    public static function master_table() {
        global $DB;
        $dbman = $DB->get_manager();
        if ($dbman->table_exists('multitenant_master')) {
            return true;
        }

        $table = new xmldb_table('multitenant_master');
        
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('pocname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('pocphone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '65', null, null, null, null);
        $table->add_field('sub_domain', XMLDB_TYPE_CHAR, '225', null, null, null, null);
        $table->add_field('site_url', XMLDB_TYPE_CHAR, '225', null, null, null, null);
        $table->add_field('start_date', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('end_date', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('max_courses', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('max_users', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('max_datastorage', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('max_dbstorage', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('limit_concurrentlogins', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        // Adding keys to table multitenant_master
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_table($table, $continue = true, $feedback = true);
    }


    public static function master_courses() {
        global $DB;
        $dbman = $DB->get_manager();
        if ($dbman->table_exists('multitenant_courses')) {
            return true;
        }

        $table = new xmldb_table('multitenant_courses');
        
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('clientid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('mastercourse', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('clientcourse', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifierid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('coursestatus', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        // Adding keys to table multitenant_course
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_table($table, $continue = true, $feedback = true);
    }
    
    public static function master_logs() {
        global $DB;
        $dbman = $DB->get_manager();
        if ($dbman->table_exists('multitenant_logs')) {
            return true;
        }

        $table = new xmldb_table('multitenant_logs');
        // Adding fields to table block_recent_activity.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('level', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('message', XMLDB_TYPE_CHAR, '65', null, null, null, null);
        $table->add_field('url', XMLDB_TYPE_CHAR, '16', null, null, null, null);
        $table->add_field('category', XMLDB_TYPE_CHAR, '125', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('clientid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_table($table, $continue = true, $feedback = true);
    }
    
    public static function client_config_plugins() {
        global $DB;
        $dbman = $DB->get_manager();
        if ($dbman->table_exists('multitenant_config_plugins')) {
            return true;
        }

        $table = new xmldb_table('multitenant_config_plugins');
        // Adding fields to table block_recent_activity.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('clientid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('value', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_table($table, $continue = true, $feedback = true);
    }
    
    public static function advance_features() {
        global $DB;
        $dbman = $DB->get_manager();
        if ($dbman->table_exists('multitenant_advance_features')) {
            return true;
        }

        $table = new xmldb_table('multitenant_advance_features');
        // Adding fields to table block_recent_activity.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('clientid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '65', null, null, null, null);
        $table->add_field('value', XMLDB_TYPE_CHAR, '225', null, null, null, null);
        
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_table($table, $continue = true, $feedback = true);
    }


   

    /**
     * Executes a Psr\Http\Message\RequestInterface and (if applicable) automatically retries
     * when errors occur.
     *
     * @param Google_Client $client
     * @param Psr\Http\Message\RequestInterface $req
     * @return array decoded result
     * @throws Google_Service_Exception on server side error (ie: not authenticated,
     *  invalid or malformed post body, invalid url)
     */

    public function get($id) {
        global $DB;
        if ($record = $DB->get_record('multitenant_master', array('id' => $id))) {
            return $record;
        }

        return null;
    }

    public function get_all($completeinfo = true) {
        global $DB, $CFG;
        self::master_table();
        if ($records = $DB->get_records('multitenant_master')) {
            if($completeinfo) {
                foreach($records as $key => $row) {
                    $usage = new ClientUsage();
                    $schema = 'moodle_'.$row->sub_domain;
                    $rs = $DB->get_record_sql("SELECT * ,sum( data_length + index_length ) 'usedspace',sum( data_free ) 'freespace' FROM information_schema.TABLES where table_schema='$schema' GROUP BY table_schema");
                    if($rs) {
                        $usage->set_database_freespace($rs->freespace);
                        $usage->set_database_usedspace($rs->usedspace);
                    }
                    $file = new FileManager();
                    $disksize = $file->folder_size($CFG->dataroot.'/'.$row->sub_domain);
                    $usage->set_disk_usedspace($disksize);
                    try {
                        $database = get_class($DB);
                        $db = new $database();
                        $db->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $schema,$CFG->prefix, $CFG->dboptions);
                        $dbm = $db->get_manager();
                        if($dbm->table_exists('user')) {
                            $activeusers = $db->count_records('user', array('deleted'=>0, 'confirmed' => 1 , 'suspended' => 0)); 
                            $inactiveusers = $db->count_records('user', array('deleted'=>0, 'confirmed' => 1 , 'suspended' => 1));
                            if(isset($activeusers[1]) && isset($activeusers[2])) {
                                unset($activeusers[1]);
                                unset($activeusers[2]);
                            }
                            if(isset($inactiveusers[1]) && isset($inactiveusers[2])) {
                                unset($inactiveusers[1]);
                                unset($inactiveusers[2]);
                            }
                            $usage->set_active_users($activeusers);
                            $usage->set_inactive_users($inactiveusers);
                        } else {
                            $usage->set_active_users('-');        
                            $usage->set_inactive_users('-');        
                        }
                    } catch (dml_connection_exception $e) {
                        $usage->set_active_users('-');        
                        $usage->set_inactive_users('-');        
                    }
                    $records[$key]->client_usage = $usage;
                }
            }
            return $records;
        }
        return null;
    }
    
    public function get_client_courses($clientid) {
        global $DB;
        self::master_courses();
        if($row = $DB->get_records('multitenant_courses', array('clientid' => $clientid))) {
            return $row;
        }
        return null;
    }

    /**
     * Change user password to the client
     * 
     * @param $userid
     * @return boolean
     */
    public function change_password($data) {
        global $CFG, $DB;
        $client = $this->get($data->id);
        $clientdb = 'moodle_' .$client->sub_domain;
        try {
            $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
            // Connect client database if exists
            $user = $DB->get_record('user', array('id' => $data->userid));
            $authplugin = get_auth_plugin($user->auth);
            if ($authplugin->can_change_password()) {
                if (!$authplugin->user_update_password($user, $data->password)) {
                    print_error('cannotupdatepasswordonextauth', '', '', $user->auth);
                }
                unset_user_preference('create_password', $user);
            }
            return true;
        } catch (dml_connection_exception $e) {
         $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('dbconn', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()), array('clientid' => $clientid));
            return 'Data connection problem!!';
        } finally {
            try {
                $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            } catch(dml_connection_exception $e) {
               $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('dbconn', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()), array('clientid' => $clientid));
            }
        }
        return false;
    }
    /**
     *  Returns all admins present in client site.
     *
     * @param Client $client
     * @return array list of admins in array
     */
    public function get_client_admins($client) {
        global $DB, $CFG;
        $clientdb = 'moodle_' .$client->get_sub_domain();
        try {
            $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
            // Connect client database if exists
            return get_admins();
        } catch (dml_connection_exception $e) {
         $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('dbconn', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()), array('clientid' => $clientid));
            return 'Data connection problem!!';
        } finally {
            try {
                $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            } catch(dml_connection_exception $e) {
               $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('dbconn', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()), array('clientid' => $clientid));
            }
        }
    }

    public function get_config_plugins($client) {
        global $DB, $CFG;
        self::client_config_plugins();
        if($plugins = $DB->get_records('multitenant_config_plugins', array('clientid' => $client->get_id()))) {
            $data = new stdClass();
            foreach($plugins as $plugin) {
                $type = $plugin->type.'_'.$plugin->name;
                $data->$type = $plugin->value;
            }
            if(!empty($data)){
                return $data;
            }
        }
        $clientid = $client->get_id();
        $clientdb = 'moodle_' .$client->get_sub_domain();
        $tables = array(
                        'mod' => 'modules',
                        'assignsubmission' => 'config_plugins',
                        'assignfeedback' => 'config_plugins',
                        'qtype' => 'config_plugins',
                        'block' => 'block',
                        'auth' => 'config_plugins',
                        'enrol' => 'config_plugins',
                        );
        try {
            $DB->dispose();
            $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
        } catch (moodle_exception $e) {
            try {
                $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            } catch(dml_connection_exception $ex) {
                throw $ex;
            }
            throw new dml_read_exception('Table not found client database');
        }
        
        if(!count($DB->get_tables())) { 
            try {
                $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            } catch(moodle_connection_exception $e) {
                throw $e;
            }
            return null;
        }


        $data = array();
        core_plugin_manager::reset_caches();
        $modules = $DB->get_records('modules');
        $blocks = $DB->get_records('block');
        foreach($modules as $row) {
            $data['mod_'.$row->name] = $row->visible;
        }
        foreach($blocks as $row) {
            $data['block_'.$row->name] = $row->visible;
        }
        $core = core_plugin_manager::instance();
        /*
        |------------------
        | Question types
        |------------------
        */
        $qtypes = $core->get_plugins_of_type('qtype');
        $disabled = $DB->get_records('config_plugins', array('plugin'=>'question'));
        $questiontypes = array_keys($qtypes);
        $qtydisabled = array();
        foreach($disabled as $disable) {
            $array = explode('_', $disable->name);
            if(isset($array[1])) {
                if(in_array($array[0], $questiontypes)) {
                    $qtydisabled[($array[0])] = 0;
                }
            }
        }
        foreach($qtypes as $type => $useless) {
            if(isset($qtydisabled[$type])) {
                $data['qtype_'.$type] = 0;
            } else {
                $data['qtype_'.$type] = 1;
            }
        }
        /*
        |---------------
        | Auth plugins 
        |---------------
        */
        $auth = $core->get_plugins_of_type('auth');
        $authplugin = $DB->get_field('config', 'value', array('name' => 'auth'));
        if(!empty($authplugin)) {
            $authplugin = explode(',', $authplugin);
            foreach($auth as $type => $nouse) {
                if(in_array($type, $authplugin)) {
                    $data['auth_'.$type] = 1;
                } else {
                    $data['auth_'.$type] = 0;
                }
            }
        }   
        /*
        |----------------
        | Enrol plugins
        |----------------
        */
        $enrol = $core->get_plugins_of_type('enrol');
        $enrolplugin = $DB->get_field('config', 'value', array('name' => 'enrol_plugins_enabled'));
        if(!empty($enrolplugin)) {
            $enrolplugin = explode(',', $enrolplugin);
            foreach($enrol as $type => $nouse) {
                if(in_array($type, $enrolplugin)) {
                    $data['enrol_'.$type] = 1;
                } else {
                    $data['enrol_'.$type] = 0;
                }
            }
        }
        /*
        |-----------------------
        | Assignment submission
        |-----------------------
        */
        $assign = $core->get_plugins_of_type('assignsubmission');
        foreach($assign as $type =>$nouse) {
            $flag = $DB->get_field('config_plugins', 'value', array('plugin'=>'assignsubmission_'.$type,'name'=>'disabled'));
            if(empty($flag)) {
                $data['assignsubmission_'.$type] = 1;
            } else if($flag) {
                $data['assignsubmission_'.$type] = 0;
            } else {
                $data['assignsubmission_'.$type] = 1;
            }
        }
        /*
        |---------------------
        | Assignment feedback
        |---------------------
        */
        $assign = $core->get_plugins_of_type('assignfeedback');
        foreach($assign as $type =>$nouse) {
            $flag = $DB->get_field('config_plugins', 'value', array('plugin'=>'assignfeedback_'.$type,'name'=>'disabled'));
            if(empty($flag)) {
                $data['assignfeedback_'.$type] = 1;
            } else if($flag) {
                $data['assignfeedback_'.$type] = 0;
            } else {
                $data['assignfeedback_'.$type] = 1;
            }
        }
        try {
            $DB->dispose();
            $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            if(!empty($data)) {
                foreach($data as $field => $flag) {
                    $insert =new stdClass();
                    $type = explode('_', $field, 2);
                    $insert->clientid = $clientid;
                    $insert->type = $type[0];
                    $insert->name = $type[1];
                    $insert->value = $flag;
                    $DB->insert_record('multitenant_config_plugins', $insert);
                }
            }
        } catch(moodle_connection_exception $e) {
            throw $e;
        }
        
        return $data;
    }

    public function set_config_plugins($formpost) {
        global $DB, $CFG;
        $clientrow = $this->get($formpost->id); 
        $clientdb = 'moodle_'.$clientrow->sub_domain;
        try {
            $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
        } catch (dml_connection_exception $e) {
            try {
                $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            } catch(dml_connection_exception $e) {}
            return null;
        }
        $updatedata = array();
        $defaultsetting = (array)unserialize($formpost->defaultdata);
        unset($formpost->id); 
        unset($formpost->client); 
        unset($formpost->defaultdata); 
        unset($formpost->submitbutton);
        
        foreach($formpost as $field => $value) {
            if(isset($defaultsetting[$field]) && $defaultsetting[$field] == $value) {
                continue;
            }
            $exp = explode('_', $field, 2);
            $type = $exp[0];
            $attribute = $exp[1];

            switch($type) {
                case 'mod' :
                    $updatedata['mod'][$attribute] = $value;
                break;
                case 'block':
                    $updatedata['block'][$attribute] = $value;
                break;
                case 'assignsubmission':
                    $updatedata['assignsubmission'][$attribute] = $value;
                break;
                case 'assignfeedback':
                    $updatedata['assignfeedback'][$attribute] = $value;
                break;
                case 'enrol':
                    $updatedata['enrol'][$attribute] = $value;
                break;
                case 'auth':
                    $updatedata['auth'][$attribute] = $value;
                break;
                case 'qtype':
                    $updatedata['qtype'][$attribute] = $value;
                break;

            }
        }
        if(isset($updatedata['mod'])){
            foreach($updatedata['mod'] as $modname => $value) {
                $DB->execute('Update {modules} set visible='.$value.' where name="'.$modname.'"');
            }
        }
        
        if(isset($updatedata['block'])){
            foreach($updatedata['block'] as $modname => $value) {
                $DB->execute('Update {block} set visible='.$value.' where name="'.$modname.'"');
            }
        }
        
        if(isset($updatedata['assignsubmission'])) {
            foreach($updatedata['assignsubmission'] as $type => $ischecked) {
                if($ischecked) {
                  $DB->delete_records('config_plugins', array('plugin' => 'assignsubmission_'.$type,'name'=>'disabled'));
                } else {
                  $DB->insert_record('config_plugins',(object) array('plugin' => 'assignsubmission_'.$type,'name'=>'disabled', 'value' =>1));
                }
            }
        }
        
        if(isset($updatedata['assignfeedback'])) {
            foreach($updatedata['assignfeedback'] as $type => $ischecked) {
                if($ischecked) {
                  $DB->delete_records('config_plugins', array('plugin' => 'assignfeedback_'.$type,'name'=>'disabled'));
                } else {
                  $DB->insert_record('config_plugins',(object) array('plugin' => 'assignfeedback_'.$type,'name'=>'disabled', 'value' =>1));
                }
            }
        }

        if(isset($updatedata['qtype'])) {
            foreach($updatedata['qtype'] as $type => $ischecked) {
                if((int)$ischecked) {
                    $DB->delete_records('config_plugins', array('plugin' =>'question','name'=> $type.'_disabled'));
                } else {
                    $DB->insert_record('config_plugins', (object)array('plugin' =>'question','name'=> $type.'_disabled', 'value' => 1));
                }
            }
        }

        if(isset($updatedata['auth'])) {
            $field = $DB->get_field('config', 'value', array('name'=>'auth'));
            $defconfig = explode(',',$field);
            foreach($updatedata['auth'] as $auth => $ischecked) {
                if($ischecked && !in_array($auth, $defconfig))  {
                    $defconfig[] = $auth;
                } else {
                   $pos =array_search($auth,$defconfig);
                   unset($defconfig[$pos]);
                }
            }
            $auths = implode(',', $defconfig);
            $DB->set_field('config', 'value', $auths, array('name'=>'auth'));
        }
        
        if(isset($updatedata['enrol'])) {
            $field = $DB->get_field('config', 'value', array('name'=>'enrol_plugins_enabled'));
            $defconfig = explode(',',$field);
            foreach($updatedata['enrol'] as $auth => $flag) {
                if($flag) {
                    $defconfig[] = $auth;
                } else {
                   $pos = array_search($auth, $defconfig);
                   unset($defconfig[$pos]);
                }
            }
            $auths = implode(',', $defconfig);
            $DB->set_field('config', 'value', $auths, array('name'=>'enrol_plugins_enabled'));
        }

        try {
            $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            
            foreach($updatedata as $type => $attributes) {
                foreach($attributes as $field => $flag) {
                    $DB->execute('Update {multitenant_config_plugins} set value='.$flag.' where type="'.$type.'" and name="'.$field.'"');
                }
            }
        } catch(moodle_connection_exception $e) {
            throw $e;
        }
        return true;
    }

    public function set_advance_features($postdata) {
        global $DB;
        self::advance_features();
        $insertobjects = array();
        $clientid = $postdata->id;
        unset($postdata->id);
        if($rows = $DB->get_records('multitenant_advance_features', array('clientid'=> $clientid))) {
            $updateobjects = array();
            foreach($rows as $row) {
                $array = (array) $postdata;
                if($row->value != $array[$row->name]) {
                    $update = array();
                    $update['id'] = $row->id;
                    $update['value'] = $array[$row->name];
                    $DB->update_record('multitenant_advance_features', $update);
                }
            }
        } else {
            foreach($postdata as $key => $flag) {
                $insert = new stdClass();
                $insert->clientid = $clientid;
                $insert->name = $key;
                $insert->value = $flag;
                $insertobjects[] = $insert;
            }
            $DB->insert_records('multitenant_advance_features', $insertobjects);
        }
        return true;
    }

    public function get_advance_features($clientid) {
        global $DB;
        self::advance_features();
        $rows = $DB->get_records('multitenant_advance_features', array('clientid'=> $clientid));
        if($rows) {
            $result = new stdClass();
            foreach($rows as $row) {
                $field = $row->name;
                $result->$field = $row->value;
            }
            return $result;
        }

        return null;
    }
}
