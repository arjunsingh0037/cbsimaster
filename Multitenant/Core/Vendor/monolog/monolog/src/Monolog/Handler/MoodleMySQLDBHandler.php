<?php

namespace Monolog\Handler;
use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use xmldb_table;
/**
 * This class is a handler for Monolog, which can be used
 * to write records in a MySQL table
 *
 * Class MySQLHandler
 * @package wazaari\MysqlHandler
 */
class MoodleMySQLDBHandler extends AbstractProcessingHandler {

    /**
     * @var bool defines whether the Moodle database  connection is been initialized
     */
    private $initialized = false;

    /**
     * @var string the table to store the logs in
     */
    private $table = 'multitenant_logs';

    /**
     * Constructor of this class
     *
     * @param moodle_database $mdb      Moodle database connection object to store log in database
     * @param bool|int $level           Debug level which this handler should store
     * @param bool $bubble
     */
    public function __construct($mdb = null, $level = Logger::DEBUG, $bubble = true) {
    	if(!is_null($mdb)) {
        	$this->mdb = $mdb;
        }
        parent::__construct($level, $bubble);
    }

    /**
     * Initializes this handler by creating the table if it not exists
     */
    private function initialize() {
        $dbman = $this->mdb->get_manager();
        if ($dbman->table_exists($this->table)) {
            $this->initialized = true;
            return;
        }
        
        $table = new xmldb_table($this->table);
        // Adding fields to table block_recent_activity.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('channel', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('level', XMLDB_TYPE_INTEGER, '4', null, null, null, null);
        $table->add_field('message', XMLDB_TYPE_CHAR, '1000', null, null, null, null);
        $table->add_field('time', XMLDB_TYPE_CHAR, '16', null, null, null, null);
        $table->add_field('clientid', XMLDB_TYPE_INTEGER, '16', null, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table, $continue = true, $feedback = true);

        $this->initialized = true;
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  $record[]
     * @return void
     */
    protected function write(array $record) {
        if (!$this->initialized) {
            $this->initialize();
        }
        //'context' contains the array
        $contentArray = array_merge(array(
            'channel' => $record['channel'],
            'level' => $record['level'],
            'message' => $record['message'],
            'time' => $record['datetime']->format('U')
        ), $record['context']);

        $this->mdb->insert_record($this->table,(object) $contentArray);
    }
}
