<?php

/*
 * Copyright 2016 Moodle of India
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Multitenant\Core\Classes;

use Multitenant\Core\Classes\Client;
use Multitenant\Forms;
use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Utils\DbManager;
use Multitenant\Core\Exception\ClientNotFoundException;
use Multitenant\Core\Exception\ClientDbConnectException;
use Multitenant\Core\Exception\FileNotFoundException;
use Multitenant\Core\Exception\FileWriteException;
use Multitenant\Core\Exception\DirectoryWriteException;
use Multitenant\Core\Utils\FileManager;
use Multitenant\Core\Utils\XmlManager;
use Exception;
use stdClass;
global $CFG;
include_once($CFG->dirroot . '/course/lib.php');

/**
 * Course manager Class
 * 
 */
final class CourseManager {

    private $instance;

    /**
     * this function allocates the selected courses to client
     *
     * @param course/category object.
     * @return boolean 
     */
    public function allocate_course($data) {
        global $DB, $CFG,$USER;
        $cm = new ClientManager();
        DbManager::master_courses();

        // Hold master course to client course relation
        $coursetransfer = array();
        $courselist = array();

        // Hold master category to client category 
        $categorylist = array();
        /**
         * Find category and couses and put array
         */
        foreach ((array) $data as $keys => $value) {
            $flag = explode('_', $keys);
            if (isset($flag[0]) && $flag[0] == 'cat') {
                $categorylist[($flag[1])] = $flag[1];
           }
            if (isset($flag[0]) && $flag[0] == 'crs') {
                $courselist[($flag[1])] = $flag[1];
            }
        }

        if (!empty($categorylist)) {
            list($where, $params) = $DB->get_in_or_equal(array_keys($categorylist));
            unset($categorylist);
            $categorylist = $DB->get_records_select('course_categories', "id $where", $params);
        }

        if (!empty($courselist)) {
            list($where, $params) = $DB->get_in_or_equal(array_keys($courselist));
            unset($courselist);
            $courselist = $DB->get_records_select('course', "id $where", $params);
        }
        $masterdb = clone $DB;
        $clientdb = 'moodle_'.$cm->get_client($data->clientid)->get_sub_domain();

        try {
            $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        $cattransfer = array();
        $parent = array();
        if (!empty($categorylist)) {
            try {
                $transaction = $DB->start_delegated_transaction();
                foreach ($categorylist as $categories) {
                    $category = (array) $categories;
                    $tmpid = $category['id'];
                    $parent[($category['id'])] = $category['parent'];
                    unset($category['id']);
                    unset($category['parent']);
                    if ($record = $DB->get_record('course_categories', array('name' => $category['name']))) {
                        $cattransfer[$tmpid] = $record->id;
                    } else {
                        $newcategory = \coursecat::create($category);
                        $cattransfer[$tmpid] = $newcategory->id;
                    }
                }
                if (!empty($cattransfer)) {
                    foreach ($parent as $oldcatid => $oldparentid) {
                        $newcategory = $cattransfer[$oldcatid];
                        if ($oldparentid == 0) {
                            $DB->update_record('course_categories', array('id' => $newcategory, 'parent' => 0));
                        } else {
                            $newparentid = $cattransfer[$oldparentid];
                            $DB->update_record('course_categories', array('id' => $newcategory, 'parent' => $newparentid));
                        }
                    }
                }
                
                foreach ($courselist as $course) {
                    if (!$DB->record_exists('course', array('shortname' => $course->shortname))) {
                        $tmpcourseid = $course->id;
                        unset($course->id);
                        $course->category = $cattransfer[($course->category)];
                        $newcourse = create_course($course);
                        
                        $coursetransfer[$tmpcourseid] = $newcourse->id;
                    }
                }
                $transaction->allow_commit();
                
                $insertobjects = array();
                foreach($coursetransfer as $mastercourse => $clientcourse) {
                    $obj = new stdClass();
                    $obj->clientid = $data->clientid;
                    $obj->mastercourse = $mastercourse;
                    $obj->clientcourse = $clientcourse;
                    $obj->timecreated = time();
                    $obj->timemodified = time();
                    $obj->modifierid = $USER->id;
                    $obj->coursestatus = 1;
                    $insertobjects[] = $obj;
                }
                try {
                     $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
                  $DB->insert_records('multitenant_courses', $insertobjects);
                } catch (moodle_database $e) {
                    throw $e;
                }
            } catch (moodle_exception $e) {
                $transaction->rollback($e);
            }
        }
    }

    /**
     * Executes a Psr\Http\Message\RequestInterface and (if applicable) automatically retries
     * when errors occur.
     *
     * @param Google_Client $client
     * @param Psr\Http\Message\RequestInterface $req
     * @return array decoded result
     * @throws Google_Service_Exception on server side error (ie: not authenticated,
     *  invalid or malformed post body, invalid url)
     */
    
}
