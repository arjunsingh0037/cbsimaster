<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace Multitenant\Core\Classes;

class ClientUsage {

    private $_database_freespace;
    private $_database_usedspace;
    private $_disk_freespace;
    private $_max_dbstorage;
    private $_active_users;
    private $_inactive_users;
    private $_max_datastorage;
    
    public function get_max_datastorage() {
        return $this->_max_datastorage;   
    }
    public function get_max_dbstorage() {
        return $this->_max_dbstorage;    
    }

    public function set_max_datastorage($max_datastorage){
        $this->_max_datastorage = $max_datastorage;
    }


    public function set_max_dbstorage($max_dbstorage){
        $this->_max_dbstorage = $max_dbstorage;
    }

    public function get_database_freespace() {
        return $this->_database_freespace;
    }

    public function get_database_usedspace() {
        return $this->_database_usedspace;
    }

    public function get_disk_freespace() {
        return $this->_disk_freespace;
    }

    public function get_disk_usedspace() {
        return $this->_disk_usedspace;
    }

    public function get_database_totalspace() {
        return $this->_database_freespace + $this->_database_usedspace;
    }

    public function get_disk_totalspace() {
        return $this->_disk_freespace + $this->_disk_usedspace;
    }
    
    public function get_active_users() {
        return $this->_active_users;
    }
    
    public function get_inactive_users() {
        return $this->_inactive_users;
    }


    /**
     * Set methods
     */
    public function set_database_freespace($freespace) {
        $this->_database_freespace = $freespace;
    }

    public function set_database_usedspace($usedspace) {
        $this->_database_usedspace = $usedspace;
    }

    public function set_disk_freespace($freespace) {
        $this->_disk_freespace = $freespace;
    }

    public function set_disk_usedspace($usedspace) {
        $this->_disk_usedspace = $usedspace;
    }

    public function set_active_users($activeusers) {
        $this->_active_users = $activeusers;
    }
    
    public function set_inactive_users($inactiveusers) {
        $this->_inactive_users = $inactiveusers;
    }

}
