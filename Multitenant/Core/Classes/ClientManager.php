<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace Multitenant\Core\Classes;

use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\ClientNotFoundException;
use Multitenant\Core\Exception\ClientsNotFoundException;
use Multitenant\Core\Exception\DirectoryWriteException;
use Multitenant\Core\Utils\FileManager;
use Multitenant\Core\Utils\XmlManager;
use Multitenant\Core\Utils\DbManager;
use stdClass;
use dml_read_exception;
use dml_connection_exception;
use moodle_exception;
use Monolog\Logger;
use Monolog\Handler\MoodleMySQLDBHandler;

/**
 * Client manager Class
 * 
 */
class ClientManager {

    private $_log;

    /**
     *
     *
     */
    public function __construct() {
        global $DB;
        $log = new Logger('coloe');
        $this->_log = $log->pushHandler(new MoodleMySQLDBHandler($DB));
    }

    /**
     * this function create new client
     *
     * @param client object.
     * @return boolean 
     */
    public function add_client(Client $client) {
        global $DB;
        try {

            $file = new FileManager();
            if ($file->create_directory($client->get_sub_domain())) {
                $this->_log->addInfo(sprintf('folder create for %s client', $client->get_name()));
            }
           // $file->create_configuration();

            $clientdb = new DbManager();
            if ($newclientid = $clientdb->add_client_to_master($client)) {
                $this->_log->addInfo('Client "%s" information stored in multitenant master table');
            }

            $masterrow = $clientdb->get_master_table();
            $xml = new XmlManager();
            $xmlstr = $xml->add_client($masterrow);
            if ($file->write_xml($xmlstr)) {
                $this->_log->addInfo('Client.xml updated as per multitenant master table');
            }

            $this->_log->addInfo(lang('clientenvsetup', $client->get_name()), array('clientid' => $newclientid));
            return true;
        } catch (DirectoryWriteException $e) {
            $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('dirwriteerror', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()));
        } catch (XMLException $e) {
            $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('xmlwriteerror', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()));
        } catch (Exception $e) {
            $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), $e->getMessage(), __CLASS__, __FUNCTION__, $e->getLine()));
        }
    }

    /**
     * this function accept one parameter
     *
     * @param int/string  client id,name
     * @return object/array of objects
     */
    public function get_client($id) {
        $db = new DbManager();
        $client = new Client();
        $client->set_client_usage(new ClientUsage());
        if ($row = $db->get($id)) {
            foreach ((array) $row as $key => $value) {
                $method = 'set_' . $key;
                $client->$method($value);
            }
        }

        if (is_null($client)) {
            throw new ClientNotFoundException($id);
        }

        return $client;
    }

    public function get_clients($completeinfo = false) {
        $file = new FileManager();
        $xmllist = $file->read_xml_file();
        $clients = array();
        if (!$completeinfo) {
            if (!empty($xmllist)) {
                foreach ($xmllist as $xml) {
                    $client = new Client();
                    foreach ((array) $xml as $key => $value) {
                        $method = 'set_' . $key;
                        $client->$method($value);
                    }
                    $clients[$client->get_id()] = $client;
                }
            }
        }

        if (empty($clients)) {
            $db = new DbManager();
            $rows = null;
            if ($completeinfo) {
                $rows = $db->get_all($completeinfo);
            } else {
                $rows = $db->get_all();
            }
            if($rows) {
                foreach ($rows as $row) {
                    $client = new Client();
                    if(isset($row->client_usage)) {
                        $row->client_usage->set_max_datastorage($row->max_datastorage);
                        $row->client_usage->set_max_dbstorage($row->max_dbstorage);
                    }
                    foreach ((array) $row as $key => $value) {
                        $method = 'set_' . $key;
                        $client->$method($value);
                    }
                    $clients[$row->id] = $client;
                }
            }
        }
        if (empty($clients)) {
            throw new ClientsNotFoundException();
        }

        return $clients;
    }

    /**
     * Executes a Psr\Http\Message\RequestInterface and (if applicable) automatically retries
     * when errors occur.
     *
     * @param Google_Client $client
     * @param Psr\Http\Message\RequestInterface $req
     * @return array decoded result
     * @throws Google_Service_Exception on server side error (ie: not authenticated,
     *  invalid or malformed post body, invalid url)
     */
    /* public function set_client_from_xml(SimpleXMLElement $xmls) {
      $client = new Client();
      foreach($xmls as $xml) {

      }

      } */
    public function set_client_config($data, $quicksetting = false) {
        global $DB, $CFG;
        $masterdb = $CFG->dbname;
        $client = $this->get_client($data->id);
        $clientdb = 'moodle_' . $client->get_sub_domain();
        try {
            $db_class = get_class($DB);
            $source_db = new $db_class();
            $source_db->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
            // Connect client database if exists
        } catch (dml_connection_exception $e) {
            $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('dbconn', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()), array('clientid' => $data->id));
            return null;
        }
        // only if $quicksetting is true
        if($quicksetting) {
            if (isset($data->maintenance_message)) {
                $source_db->execute("update {config} set value='$data->maintenance_message' where name='maintenance_message'");
                $this->_log->addInfo(lang('maintenancemessage', $client->get_name(), $data->maintenance_message), array('clientid' => $data->id));
            }
            $source_db->execute("update {config} set value='$data->maintenance_enabled' where name='maintenance_enabled'");
            /*
            |--------------------------
            | Make cache clear
            |--------------------------
            */
            $CFGtmp = clone $CFG;
            $dataroot = $CFG->dataroot;
            $clientdataroot = $dataroot . '/' . $client->get_sub_domain();
            $CFG->dataroot = $clientdataroot;
            $CFG->tempdir = $clientdataroot . '/temp';
            $CFG->cachedir = $clientdataroot . '/cache';
            $CFG->localcachedir = $clientdataroot . '/localcache';
            $CFG->langotherroot = $clientdataroot . '/lang';
            $CFG->langlocalroot = $clientdataroot . '/lang';
            purge_all_caches();
            $source_db->dispose();
            $CFG = $CFGtmp;
            unset($CFGtmp);
            return true;
        }
        if (isset($data->theme)) {
            $source_db->execute("update {config} set value='$data->theme' where name='theme'");
            $this->_log->addInfo(lang('themechange', $client->get_name(), $data->theme), array('clientid' => $data->id));
        }
        if (isset($data->maintenance_message)) {
            $source_db->execute("update {config} set value='$data->maintenance_message' where name='maintenance_message'");
            $this->_log->addInfo(lang('maintenancemessage', $client->get_name(), $data->maintenance_message), array('clientid' => $data->id));
        }
        if (isset($data->isdeactive_message)) {
            $source_db->execute("update {config} set value='$data->isdeactive_message' where name='maintenance_message'");
            $this->_log->addInfo(lang('deactivemessage', $client->get_name(), $data->isdeactive_message), array('clientid' => $data->id));
        }
        if (!isset($data->passwordpolicy)) {
            $data->passwordpolicy = 0;
        }
        if (!isset($data->maintenance_enabled)) {
            $data->maintenance_enabled = 0;
        }
        if (isset($data->isdeactive)) {
            $data->maintenance_enabled = 1;
        }

        if (!isset($data->limitconcurrentlogins)) {
            $data->limitconcurrentlogins = 0;
        }

        $source_db->execute("update {config} set value='$data->passwordpolicy' where name='passwordpolicy'");
        $this->_log->addInfo(lang('passwordpolicy', $client->get_name(), $data->theme), array('clientid' => $data->id));

        $source_db->execute("update {config} set value='$data->maintenance_enabled' where name='maintenance_enabled'");
        $update = new stdClass();
        $update->id = $data->id;
        $update->status = !$data->maintenance_enabled;
        $update->limit_concurrentlogins  = $data->limitconcurrentlogins;
        $DB->update_record('multitenant_master', $update);
        $this->_log->addInfo(lang('maintenanaceenabled', $client->get_name(), $data->theme), array('clientid' => $data->id));

        $source_db->execute("update {config} set value='$data->limitconcurrentlogins' where name='limitconcurrentlogins'");
        $this->_log->addInfo(lang('limitconcurrentlogins', $client->get_name(), $data->theme), array('clientid' => $data->id));
        
        if (isset($data->changeownpassword)) {
            $data->changeownpassword = -1;
        } else {
            $data->changeownpassword = 1;
        }
        if ($row = $source_db->get_record('role_capabilities', array('capability' => 'moodle/user:changeownpassword', 'roleid' => 7))) {
            $row->permission = $data->changeownpassword;
            $source_db->update_record('role_capabilities', $row);
            $this->_log->addInfo(lang('rolecapabilitiesupdate', $client->get_name(), $data->changeownpassword), array('clientid' => $data->id));
        } else {
            $insert = new stdClass();
            $insert->contextid = 1;
            $insert->roleid = 7;
            $insert->capability = 'moodle/user:changeownpassword';
            $insert->permission = $data->changeownpassword;
            $insert->timemodified = time();
            $insert->modifierid = 2;
            $source_db->insert_record('role_capabilities', $insert);
            $this->_log->addInfo(lang('rolecapabilitiesinsert', $client->get_name(), $data->changeownpassword), array('clientid' => $data->id));
        }
        /*
         * make cache clear
         */
        $CFGtmp = clone $CFG;
        $dataroot = $CFG->dataroot;
        $clientdataroot = $dataroot . '/' . $client->get_sub_domain();
        $CFG->dataroot = $clientdataroot;
        $CFG->tempdir = $clientdataroot . '/temp';
        $CFG->cachedir = $clientdataroot . '/cache';
        $CFG->localcachedir = $clientdataroot . '/localcache';
        $CFG->langotherroot = $clientdataroot . '/lang';
        $CFG->langlocalroot = $clientdataroot . '/lang';
        purge_all_caches();
        $this->_log->addInfo(lang('cacheclear', $client->get_name(), $data->theme), array('clientid' => $data->id));
        $source_db->dispose();
        unset($source_db);
        $CFG = $CFGtmp;
        unset($CFGtmp);
        return true;
    }

    public function get_client_config($clientid = '') {
        global $DB, $CFG;

        if (empty($clientid)) {
            return null;
        }
        $masterdb = $CFG->dbname;
        $client =  $this->get_client($clientid);

        $clientdb = 'moodle_' .$client->get_sub_domain();
        try {
            $db_class = get_class($DB);
            $source_db = new $db_class();
            $source_db->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
            // Connect client database if exists
        } catch (dml_connection_exception $e) {
            $this->_log->addError(sprintf('%s : %s at %s.%s:%d', get_class($e), lang('dbconn', $client->get_name()), __CLASS__, __FUNCTION__, $e->getLine()), array('clientid' => $clientid));
            return 'Data connection problem!!';
        }

            $default = new stdClass();
            $default->theme = $source_db->get_field('config', 'value', array('name' => 'theme'));
            $default->passwordpolicy = $source_db->get_field('config', 'value', array('name' => 'passwordpolicy'));
            $default->maintenance_enabled = $source_db->get_field('config', 'value', array('name' => 'maintenance_enabled'));
            $default->isdeactive = $default->maintenance_enabled;
            $default->maintenance_message = $source_db->get_field('config', 'value', array('name' => 'maintenance_message'));
            $default->isdeactive_message = $default->maintenance_message;
            $default->limitconcurrentlogins = $source_db->get_field('config', 'value', array('name' => 'limitconcurrentlogins'));
            if ($row = $source_db->get_record('role_capabilities', array('capability' => 'moodle/user:changeownpassword', 'roleid' => 7))) {
                if ($row->permission == -1) {
                    $default->changeownpassword = 1;
                } else {
                    $default->changeownpassword = 0;
                }
            }
            $source_db->dispose();
            return $default;
    
    }

    public function update_client(Client $client) {
        $file = new FileManager();
        $clientdb = new DbManager();
        $clientdb->update_client_to_master($client);
        $masterrow = $clientdb->get_master_table();
        $xml = new XmlManager();
        $xmlstr = $xml->add_client($masterrow);
        $file->write_xml($xmlstr);
        return true;
    }
    public function get_client_transfer_master_courses($clientid) {
        $db = new DbManager();
        $rows = $db->get_client_courses($clientid);
        $courses =array();
        if($rows) {
            foreach($rows as $row) {
                $courses[$row->mastercourse] = $row->clientcourse;
            }
            return $courses;
        }
        return null;
    }

    public function change_client_password($data) {
        $db = new DbManager();     
        return $db->change_password($data);
    }

    public function get_client_admins($client) {
        $db = new DbManager();
        $admins = $db->get_client_admins($client);
        return $admins;
    }

    public function get_client_config_plugins($client) {
        $db = new DbManager();
        try {
            $config = $db->get_config_plugins($client);
            return $config;
        } catch(dml_read_exception $e) {
            //throw $e;            
        }
    }
    
    public function set_client_config_plugins($formpost) {
        $db = new DbManager();
        $config = $db->set_config_plugins($formpost);
        return $config;
    }

    public function set_advance_features($postdata) {
        $db = new DbManager();
        $config = $db->set_advance_features($postdata);
        return $config;
    }
    public function get_advance_features($clientid) {
        $db = new DbManager();
        $config = $db->get_advance_features($clientid);
        return $config;
    }
}
