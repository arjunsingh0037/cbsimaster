<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * Client class
 * @desc this class holds client variables.
 *
 *
 * @author Shambhu kumar { shambhu384@gmail.com }
 * @copyright Moodleofindia
 */

namespace Multitenant\Core\Classes;

use Multitenant\Core\Classes\ClientUsage;

class Client {

    private $_id;
    private $_name;
    private $_pocname;
    private $_pocphone;
    private $_email;
    private $_sub_domain;
    private $_site_url;
    private $_start_date;
    private $_end_date;
    private $_max_courses;
    private $_max_users;
    private $_db_name;
    private $_client_usage;
    private $_status;
    private $_limit_concurrentlogins;

    /**
     * 
     * @return type
     */
    public function get($all = false) {
        $instance = array();
        foreach (get_object_vars($this) as $key => $var) {
            if($all) {
                if ($var instanceof ClientUsage || (empty($var) && $var != 0))
                continue;
            }
            $instance[trim($key, '_')] = $var;
        }
        return $instance;
    }

    /**
     * 
     * @param type $data
     */
    public function set($data) {
        $this->set_client_usage(new ClientUsage);
        if (!is_null($data)) {
            foreach ((array) $data as $field => $value) {
                $variable = '_' . $field;
                if (property_exists($this, $variable)) {
                    $this->$variable = $value;
                }

                if(property_exists($this->_client_usage, $variable)) {
                    $method = 'set'.$variable;
                    $this->_client_usage->$method($value);
                }
            }
        }
    }
    /**
     * 
     * @param ClientUsage $usage
     */
    public function set_pocphone($pocphone) {
        $this->_pocphone = $pocphone;
    }

    /**
     * 
     * @return type
     */
    public function get_pocphone() {
        return $this->_pocphone;
    }
    public function get_status() {
        return $this->_status;
    }


     /**
     * 
     * @param ClientUsage $usage
     */
    public function set_pocname($pocname) {
        $this->_pocname = $pocname;
    }

    /**
     * 
     * @return type
     */
    public function get_pocname() {
        return $this->_pocname;
    }

   /**
     * 
     * @param ClientUsage $usage
     */
    public function set_client_usage(ClientUsage $usage) {
        $this->_client_usage = $usage;
    }

    /**
     * 
     * @return type
     */
    public function get_client_usage() {
        return $this->_client_usage;
    }

    /**
     * 
     * @param type $limitconcurrentlogins
     */
    public function set_limit_concurrentlogins($limitconcurrentlogins) {
        $this->_limit_concurrentlogins = $limitconcurrentlogins;
    }

    /**
     * 
     * @return type
     */
    public function get_limit_concurrentlogins() {
        return $this->_limit_concurrentlogins;
    }

    /**
     * 
     * @param type $maxcourses
     */
    public function set_max_courses($maxcourses) {
        $this->_max_courses = $maxcourses;
    }

    /**
     * 
     * @return type
     */
    public function get_max_courses() {
        return $this->_max_courses;
    }

    /**
     * 
     * @param type $email
     */
    public function set_email($email) {
        $this->_email = $email;
    }

    /**
     * 
     * @return type
     */
    public function get_email() {
        return $this->_email;
    }

    /**
     * 
     * @return type
     */
    public function get_id() {
        return $this->_id;
    }

    /**
     * 
     * @return type
     */
    public function get_name() {
        return $this->_name;
    }

    /**
     * 
     * @return type
     */
    public function get_sub_domain() {
        return $this->_sub_domain;
    }

    /**
     * 
     * @return type
     */
    public function get_site_url() {
        return $this->_site_url;
    }

    /**
     * 
     * @return type
     */
    public function get_start_date() {
        return $this->_start_date;
    }

    /**
     * 
     * @return type
     */
    public function get_end_date() {
        return $this->_end_date;
    }

    /**
     * 
     * @return type
     */
    public function get_max_users() {
        return $this->_max_users;
    }
    
    /**
     * 
     * @return type
     */
    public function get_db_name() {
        return $this->_db_name;
    }

    /**
     * 
     * @param type $name
     */
    public function set_name($name) {
        $this->_name = $name;
    }

    /**
     * 
     * @param type $id
     */
    public function set_id($id) {
        $this->_id = $id;
    }

    /**
     * 
     * @param type $domain
     */
    public function set_sub_domain($domain) {
        $this->_sub_domain = $domain;
    }

    /**
     * 
     * @param type $siteurl
     */
    public function set_site_url($siteurl) {
        $this->_site_url = $siteurl;
    }

    /**
     * 
     * @param type $startdate
     */
    public function set_start_date($startdate) {
        $this->_start_date = $startdate;
    }

    /**
     * 
     * @param type $enddate
     */
    public function set_end_date($enddate) {
        $this->_end_date = $enddate;
    }

    /**
     * 
     * @param type $maxusers
     */
    public function set_max_users($maxusers) {
        $this->_max_users = $maxusers;
    }

    /**
     * 
     * @param type $dbname
     */
    public function set_db_name($dbname) {
        $this->_db_name = $dbname;
    }

    /**
     * 
     * @param type $status
     */
    public function set_status($status) {
        $this->_status = $status;
    }
    
    public function __call($call,$params=null) {
        if(method_exists($this->_client_usage, $call)) {

            if(!empty($params)) {
                return $this->_client_usage->$call(reset($params));
            }
            return $this->_client_usage->$call();
        }
    }
}
