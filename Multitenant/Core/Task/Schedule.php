<?php

namespace Multitenant\Core\Task;

abstract class Schedule {

    public funciton cron();
    /**
     * Run the task every day at midnight
     */
    public function daily() {
        
    }
    /**
     *  Run the task every day at 13:00
     */
    public function dailyAt($default = '13:00') {
        
    };
    /**
     * Run the task every week
     */
    public function weekly() {
        
    }
    /**
     *  Run the task every month
     */
    public function monthly() {

    }
    public function yearly() {}

}
