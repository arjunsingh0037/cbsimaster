<?php

namespace Multitenant;

use stdClass;
use SplClassLoader;
// Include Moodle Configuragtion file

include dirname(dirname(__FILE__)) . '/config.php';
// Create Multitenant Config
//unset($MULTI);
global $MULTI;
$MULTI = new stdClass();
$maindomain = 'cbsi-connect.org';

if ($requesturl = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL)) {
    if ($requesturl != $maindomain) {
        header('HTTP/1.0 400 Bad Request', true, 400);
        die(sprintf('Un-authorised access request, this functionality is not part of your domain!!'));
    }
}

require_login(0, false);
if (!is_siteadmin()) {
    die(sprintf('Soory you don\'t have permission to access this page.'));
}
require 'SplClassLoader.php';
require dirname(__FILE__).'/Core/Vendor/autoload.php';

// Import classes from global namespace



/**
 * @var to hold Multitentant core path
 *
 */
$MULTI->core = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Core';

/*
 * Moodle forms 
 *
 * @return forms path
 */
$MULTI->forms = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Forms';

/**
 * 
 *
 */
$MULTI->cli = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cli';
$MULTI->vendor = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Vendor';
$MULTI->web = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'web';

/*
 * Load 
 */
$classLoader = new SplClassLoader('Multitenant', $CFG->dirroot);
$classLoader->register();
