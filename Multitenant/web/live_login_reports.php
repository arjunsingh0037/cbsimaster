<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require '../autoload.php';

use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Exception\ClientsNotFoundException;
use dml_read_exception;

require_once("$CFG->libdir/formslib.php");
include("{$MULTI->forms}/client_setting_form.php");
include($CFG->dirroot.'/Multitenant/helper.php');
$clientid = optional_param('id', '', PARAM_INT);
$PAGE->set_pagelayout('admin');
$syscontext = context_system::instance(); 
$PAGE->set_context($syscontext);
$PAGE->set_title('Live login reports');
$PAGE->set_heading('Live login reports');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/live_login_reports.php');
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('livereports'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Live login reports');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery-1.12.0.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery.dataTables.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.fixedColumns.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.html5.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.flash.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.print.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.buttons.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jszip.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/pdfmake.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/vfs_fonts.js'),true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery.dataTables.min.css'));
// Not support bootstrap3
//$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.bootstrap.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/styles.css'),true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.dataTables.min.css'));


echo $OUTPUT->header();
// not support bootstrap3
//echo '<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">';
echo '<style>
        .startup .alert{
                padding: 8px;
         }
        .startup .alert h4{
             font-size: 15px;
        }         
     </style>';

$cm = new ClientManager();
$alert = 'alert alert-success';
$msg = 'Client <b>%s</b> has been selected';

try {
    $lists = $cm->get_clients();
} catch(ClientsNotFoundException $e){
    $msg = 'No clients available';    
}
$optionlist = array();
$optionlist['none'] = '-- Select --';
if(!empty($lists)) {
    foreach ($lists as $list) {
        $optionlist[$list->get_id()] = $list->get_name();
    }
}
$clientname = '';
if (isset($optionlist[$clientid])) {
        $clientname = $optionlist[$clientid];
}
echo html_writer::tag('p', 'Client summary', array('class'=>'lead'));
echo html_writer::empty_tag('hr');

$table = new html_table();
$table->id = 'example2';
$table->head = array('S.L.','Client name', 'Online users');
$table->attributes = array('class' => 'table table-striped table-bordered table-hover');
$clientonlineusers = array();
$i = 1;
$activeclient = 0;
$inactiveclient = 0;
$activeusers = 0;
$inactiveusers = 0;
foreach($lists as $client) {
    if($client->get_status() == 1) {
        $activeclient += 1;
    } else {
        $inactiveclient +=1;
    }

    try {
        $clientdatabase = 'moodle_'.$client->get_sub_domain();
        $database = get_class($DB);
        $db = new $database;
        $db->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdatabase, $CFG->prefix, $CFG->dboptions);
        
        $activeusers += $db->count_records('user', array('deleted'=>0, 'confirmed' => 1 , 'suspended' => 0));
        $inactiveusers += $db->count_records('user', array('deleted'=>0, 'confirmed' => 1 , 'suspended' => 1));

        $timetoshowusers = 100; //Seconds default  
        $now = time();
        $timefrom = 100 * floor(($now - $timetoshowusers) / 100); // Round to nearest 100 seconds for better query cache
        $paramss['now'] = $now;
        $paramss['timefrom'] = $timefrom;
        $groupmembers = "";
        $groupselect = "";
        $userfields = 'id,firstname, lastname,lastip';
        $sql = "SELECT $userfields, MAX(u.lastaccess) AS lastaccess FROM {user} u $groupmembers 
                WHERE u.lastaccess > :timefrom  AND u.lastaccess <= :now  $groupselect 
                GROUP BY $userfields ORDER BY lastaccess DESC ";
        $loggedinusers = $db->get_records_sql($sql, $paramss);
        if(count($loggedinusers) > 0) {
            $clientonlineusers[] = array($i++,$client->get_name(), count($loggedinusers));
        }
    } catch(dml_connection_exception $e) {
        //$clientonlineusers[] = array($i++, $client->get_name(), '-');
    } catch(moodle_exception $e) {
        //$clientonlineusers[] = array($i++,$client->get_name(), '-');
    }
}
?>
<div class="startup" style="display:table;width:100%">
    <div class="span2">
        <div class="alert alert-warning">
            <center>
                <h4>Active clients</h4>
                <span class="label label-pill label-default"><?php echo $activeclient;?></span>
            </center>
        </div>
    </div>
    <div class="span2">
        <div class="alert alert-warning">
            <center>
            <h4>Inactive clients</h4>
            <span class="label label-pill label-default">
                <?php echo $inactiveclient;?>
            </span>
            </center>
       </div>
    </div>
    <div class="span2">
        <div class="alert alert-warning">
        <center>
            <h4>Total clients </h4>
            <span class="label label-pill label-default">
            <?php echo $activeclient+$inactiveclient;?>
            </span>
        </center>
        </div>
    </div>
    <div class="span2">
        <div class="alert alert-warning">
            <center>
            <h4>Active users </h4>
            <span class="label label-pill label-default">
                <?php echo $activeusers;?>
            </span>
            </center>
        </div>
    </div>
    <div class="span2">
        <div class="alert alert-warning">
            <center>
            <h4>Inactive users</h4>
            <span class="label label-pill label-default">
                <?php echo $inactiveusers;?>
            </span>
            </center>
        </div>
    </div>
    <div class="span2">
        <div class="alert alert-warning">
            <center>
            <h4>Total users </h4>
            <span class="label label-pill label-default">
                <?php echo $activeusers+$inactiveusers;?>
            </span>
           </center>
       </div>
    </div>
</div>
<?php

echo html_writer::tag('p', 'Live login report summary', array('class'=>'lead'));
echo html_writer::empty_tag('hr');

if(empty($clientonlineusers)) {
    echo html_writer::div('No users currently online in any clients', 'alert alert-warning');  
} else {
    $table->data = $clientonlineusers;
    echo html_writer::table($table);
}

echo html_writer::tag('p', 'Live login report details', array('class'=>'lead'));
echo html_writer::empty_tag('hr');

echo html_writer::tag('span', 'Select client', array('style'=>'font-weight:600;margin-right:8px'));
echo $OUTPUT->single_select($url, 'id', $optionlist, $clientid, null, 'rolesform');

if(!empty($clientid)) {
    $clientdb = 'moodle_'.$lists[$clientid]->get_sub_domain();
    try {
        $DB->dispose();
        // Connect client database if exists
        $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $clientdb, $CFG->prefix, $CFG->dboptions);
        } catch(moodle_exception $e) {
            // Connect master
            try {
                $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
            } catch(moodle_exception $e) {
                echo "$e";
            }
       echo html_writer::div('Service not available!!', 'alert alert-warning mrx');     
       echo $OUTPUT->footer();
       die();
    }
    try {
       $loggedinusers = loggedin_users();      
    } catch (dml_read_exception $e) {
       // don't show exception.
        echo html_writer::div('Please check whether moodle is properly installed or not.','alert alert-error mrx');
    }   
    if(!empty($loggedinusers)){
        $table = new html_table();
        $table->id = 'example';
        $table->head = array('S.L.','Client name', 'Name', 'Login');
        $table->attributes = array('class' => 'table table-striped table-bordered table-hover');
        $iplist = array();
        foreach($loggedinusers as $user){
            $iplist[] = array("query" => $user->lastip,"fields" => "city,country");
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://ip-api.com/batch");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($iplist)); 
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: text/plain')); 
        $loginaddress = json_decode(curl_exec ($ch), true);
        foreach($loggedinusers as $loginuser) {
            static $i = 0;
            $location = sprintf('User login with %s Ip-address',$loginuser->lastip);
            if(count($loginaddress[0]) > 0) {
                $location = sprintf('%s, %s with %s Ip address',$loginaddress[$i]['city'],$loginaddress[$i]['country'], $loginuser->lastip);
            }
            $fullname = $loginuser->firstname.' '.$loginuser->lastname;
            $table->data[] = array(
                                ++$i,
                                $clientname,
                                html_writer::img($CFG->wwwroot . '/user/pix.php?file=/' . $loginuser->id . '/f1.jpg', $fullname, array('height' => 32, 'class' => 'img-circle')).' &nbsp;'.
                                $fullname,
                                //$location
                               );
        }
        $msg = html_writer::tag('buttom', '<span aria-hidden="true">&times;</span>' ,array('class' => 'close', 'data-dismiss' => 'alert', 'aria-label' => 'Close'));
        $msg = 'You have selected <b>%s</b> client.'.$msg;
        echo html_writer::div(sprintf($msg, $optionlist[$clientid]), 'alert alert-success mrx');
        echo html_writer::table($table);
    } else {
        echo html_writer::div('Currently no user online.','alert alert-warning mrx');
    }
}

try {
     $DB->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, $CFG->dbname, $CFG->prefix, $CFG->dboptions);
} catch(moodle_exception $e) {
    echo $e->getMessage();
}
echo '<script type="text/javascript">
        $(document).ready(function() {
            $("#example").DataTable({
                dom: "Bfrtip",
                buttons: [
                            "copy", "csv", "excel", "pdf", "print"
                         ]
             });
            $("#example2").DataTable({
                dom: "Bfrtip",
                buttons: [
                   "copy", "csv", "excel", "pdf", "print"
                         ]
                });
        });
      </script>';

echo $OUTPUT->footer();
