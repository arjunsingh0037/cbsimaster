<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require '../autoload.php';

use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\ClientsNotFoundException;

require_once("$CFG->libdir/formslib.php");
include("{$MULTI->forms}/client_change_password_form.php");
include($CFG->dirroot . '/Multitenant/helper.php');
$clientid = optional_param('id', 0, PARAM_INT);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Client change password');
$PAGE->set_heading('Client change password');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/client_changepassword.php');
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('changepassword'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Client change password');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));
$cm = new ClientManager();
$alert = 'alert alert-success';
$msg = 'Client <b>%s</b> has been selected';
try {
    $lists = $cm->get_clients();
} catch (ClientsNotFoundException $e) {
    $msg = 'Clients not found';
}
$optionlist = array();
$optionlist['none'] = '-- Select --';
if (!empty($lists)) {
    foreach ($lists as $list) {
        $optionlist[$list->get_id()] = $list->get_name();
    }
}
$clientname = '';
$admins = array();
if (isset($optionlist[$clientid])) {
    $clientname = $optionlist[$clientid];
    $client = $lists[$clientid];
    $admins = $cm->get_client_admins($client);
}


$csetting = new client_change_password_form($url, array('id' => $clientid, 'admins'=> $admins));

if ($data = $csetting->get_data()) {
    // ceate instance of CLientManager 
    if ($cm->change_client_password($data ,true)) {
        $msg = 'Client <b>%s</b> setting has been updated.';
    } else {
        $alert = 'alert alert-warning';
        $msg = 'Client <b>%s</b> updated problem. please contact site administrator';
    }
}
$msg .= html_writer::tag('buttom', '<span aria-hidden="true">&times;</span>', array('class' => 'close', 'data-dismiss' => 'alert', 'aria-label' => 'Close'));

echo $OUTPUT->header();
echo html_writer::tag('span', 'Client  change password', array('class' => 'lead'));
echo html_writer::empty_tag('hr');
if ($csetting != null) {
    if (!empty($clientid)) {
        echo html_writer::div(sprintf($msg, $optionlist[$clientid]), $alert);
    }
    echo html_writer::start_div('', array('style' => 'margin-left: 14.5%;margin-bottom:8px'));
    echo html_writer::tag('span', 'Select client', array('style' => 'font-weight:600;padding-right:10px'));
    echo $OUTPUT->single_select($url, 'id', $optionlist, $clientid, null, 'rolesform');
    echo html_writer::end_div();
    $csetting->display();
} else {
    echo html_writer::div(sprintf($msg, $optionlist[$clientid]), $alert);
}
echo $OUTPUT->footer();
