<?php

require '../autoload.php';

use moodle_exception;
use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\ClientsNotFoundException;

$clientid = optional_param('id', '', PARAM_INT);
require_once($CFG->libdir . '/adminlib.php');
include($CFG->dirroot . '/Multitenant/helper.php');
require_once("$CFG->libdir/formslib.php");
include($CFG->dirroot . '/Multitenant/Forms/client_plugin_form.php');
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Client plugins');
$PAGE->set_heading('Client plugins');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/client_plugins.php');
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('plugins'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Client plugins');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));

$cm = new ClientManager();
$alert = 'alert alert-success';
$msg = 'Client <b>%s</b> has been selected';
try {
    $lists = $cm->get_clients();
} catch (ClientsNotFoundException $e) {
    $msg = 'Clients not found';
}
$client = null;
$optionlist = array();
$optionlist['none'] = '-- Select --';
    if (!empty($lists)) {
        foreach ($lists as $list) {
        $optionlist[$list->get_id()] = $list->get_name();
    }
}
$clientname = '';
if (isset($optionlist[$clientid])) {
    $clientname = $optionlist[$clientid];
}
$currentsettings = null;
$activedata = null;
if(!empty($clientid)) {
    $client = $lists[$clientid];
    try {
        $currentsettings = $cm->get_client_config_plugins($client);
        if(is_null($currentsettings)) {
            $msg = 'Error on reading data from client database!!';
            $alert = 'alert alert-warning';
        }
        $activedata = serialize($currentsettings);
    } catch(moodle_exception $e) {
        $msg = 'Error on reading data from client database !!';
        $alert = 'alert alert-error';
    }
}
$form = new client_plugin_form($url, array('id' => $clientid, 'clientname' => $clientname, 'tmpdata' => $activedata));
$form->set_data($currentsettings);
if($postdata = $form->get_data()) {
    if($cm->set_client_config_plugins($postdata)) {
        $msg = "Client $clientname settings has been updated.";
        $alert = 'alert alert-success';
    }
}
echo $OUTPUT->header();
echo html_writer::tag('span', 'Client settings', array('class' => 'lead'));
echo html_writer::empty_tag('hr');
echo html_writer::tag('p', 'If you check the option then these modules will be shown in client administration', array('class'=>'alert alert-warning'));

if ($form != null) {
    if (!empty($clientid)) {
        echo html_writer::div(sprintf($msg, $optionlist[$clientid]), $alert);
    }
    echo html_writer::start_div('', array('style' => 'margin-left:18.5%;margin-bottom:8px'));
    echo html_writer::tag('span', 'Select client', array('style' => 'font-weight:600;padding-right:10px'));
    echo $OUTPUT->single_select($url, 'id', $optionlist, $clientid, null, 'rolesform');
    echo html_writer::end_div();
    $form->display();
} else {
    echo html_writer::div(sprintf($msg, $optionlist[$clientid]), $alert);
}
echo $OUTPUT->footer();
