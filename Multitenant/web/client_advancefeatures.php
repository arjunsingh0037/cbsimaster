<?php

require '../autoload.php';

use moodle_exception;
use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\ClientsNotFoundException;

$clientid = optional_param('id', '', PARAM_INT);
require_once($CFG->libdir . '/adminlib.php');
include($CFG->dirroot . '/Multitenant/helper.php');
require_once("$CFG->libdir/formslib.php");
include($CFG->dirroot . '/Multitenant/Forms/client_advance_features_form.php');
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Client  advance features');
$PAGE->set_heading('Client site administration menu');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/client_advancefeatures.php');
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('advancefeatures'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Client site administration menu');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));

$cm = new ClientManager();
$alert = 'alert alert-success';
$msg = 'Client <b>%s</b> has been selected';
try {
    $lists = $cm->get_clients();
} catch (ClientsNotFoundException $e) {
    $msg = 'Clients not found';
}
$client = null;
$optionlist = array();
$optionlist['none'] = '-- Select --';
    if (!empty($lists)) {
        foreach ($lists as $list) {
        $optionlist[$list->get_id()] = $list->get_name();
    }
}
$currentsettings = null;
/*
|---------------------------
| Advance feature form
|---------------------------
*/
$form = new client_advance_features_form($url, array('id' => $clientid));
$form->set_data($cm->get_advance_features($clientid));

if($postdata = $form->get_data()) {
    unset($postdata->submitbutton);
    if($cm->set_advance_features($postdata)) {
        $alert = 'alert alert-success';
        $msg = 'Client <b>%s</b> settings has been updated succssfully';        
    } else {
        $alert = 'alert alert-warning';
        $msg = 'Client <b>%s</b> settings update problem!!';
    }
}

echo $OUTPUT->header();
echo html_writer::tag('span', 'Client site administration menu', array('class' => 'lead'));
echo html_writer::empty_tag('hr');

echo html_writer::tag('p', 'If you check the option then the menu\'s will be shown in client administration', array('class'=>'alert alert-warning'));

if ($form != null) {
    if (!empty($clientid)) {
        echo html_writer::div(sprintf($msg, $optionlist[$clientid]), $alert);
    }
    echo html_writer::start_div('', array('style' => 'margin-left:18.5%;margin-bottom:8px'));
    echo html_writer::tag('span', 'Select client', array('style' => 'font-weight:600;padding-right:10px'));
    echo $OUTPUT->single_select($url, 'id', $optionlist, $clientid, null, 'rolesform');
    echo html_writer::end_div();
    $form->display();
} else {
    echo html_writer::div(sprintf($msg, $optionlist[$clientid]), $alert);
}

echo $OUTPUT->footer();
