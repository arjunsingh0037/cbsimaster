<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require '../autoload.php';

use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;

require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/installlib.php");
include_once("{$MULTI->forms}/addclient_form.php");
include($CFG->dirroot . '/Multitenant/helper.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title('Add Client');
$PAGE->set_heading('Add Client');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));
$PAGE->set_url(new moodle_url($CFG->wwwroot . '/Multitenant/web/create_client.php'));
$regions = $PAGE->blocks->get_default_region();
$bc = new block_contents();
$PAGE->blocks->add_fake_block(navigation_menu('createclient'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();

$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Add client');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/style.css'));
echo $OUTPUT->header();

$addclientform = new addclient_form();
$msg = '';
if ($data = $addclientform->get_data()) {
    $starttime = time();
    // ceate instance of ClientManager 
    $cm = new ClientManager();
    $client = new Client();
    if(!isset($data->availablefromenabled)) {
        $data->end_date = 0;
    }
    $client->set($data);
    $client->set_status(1);
    if ($cm->add_client($client)) {
        $msg .= html_writer::div(sprintf(
                                        'Your <b>%s</b> client folder has been created, please <b><a href="%s" target="_blank">Click here</a></b> to installation', 
                                        $data->name, 
                                        $data->site_url), 
                                        'alert alert-success'
                                );
        $table = new html_table();
        $table->attributes = array('class' => 'table table-striped');
        $maxdatastorage = $data->max_datastorage;
        if($maxdatastorage == 0) {
            $maxdatastorage = 'Unlimited';
        }
        $maxdbstorage = $data->max_dbstorage;
        if($maxdbstorage == 0) {
            $maxdbstorage = 'Unlimited';
        }
        $maxusers = $data->max_users;
        if($maxusers == 0) {
            $maxusers = 'Unlimited';
        }
        $maxcourses = $data->max_courses;
        if($maxcourses == 0) {
            $maxcourses = 'Unlimited';
        }
        $enddate = 'Unlimited';
        if($client->get_end_date() != 0)
        {
            $enddate =  userdate($client->get_end_date(), '%d %B %Y');
        }
        $table->data = array (
            array('Name', $client->get_name()),
            array('Domain name', $client->get_sub_domain()),
            array('Site url', html_writer::link(new moodle_url($client->get_site_url()), $client->get_site_url())),
            array('Start date', userdate($client->get_start_date(), '%d %B %Y')),
            array('End date', $enddate),
            array('Max users', $maxusers),
            array('Max courses', $maxcourses),
            array('Max data storage',$maxdatastorage.' Gigabytes'),
            array('Max database storage', $maxdbstorage.' Gigabytes'),
        );
        $msg .= html_writer::div(html_writer::table($table), 'row-fulid');
        $msg .= html_writer::link($CFG->wwwroot . '/Multitenant/web/clients.php', 'View clients', array('class' => 'btn btn-primary'));
        $addclientform = null;
    } else {
        $msg .= html_writer::div(sprintf('Something problem client <b>%s</b> creation try later.', $data->name), 'alert alert-success');
    }
}
if (!is_null($addclientform)) {
    $addclientform->display();
}
if (!empty($msg)) {
    echo $msg;
}
echo '<script type="text/javascript">
        document.getElementById("id_sub_domain").addEventListener("change", function() {
            var subdomain =  document.getElementById("id_sub_domain").value;
            var base_url = window.location.origin;
            var url = base_url.replace("mle", subdomain); 
            document.getElementById("id_site_url").value = url;
        });
      </script>';
echo $OUTPUT->footer();
