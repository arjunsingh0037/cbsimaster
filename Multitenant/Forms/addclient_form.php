<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class addclient_form extends moodleform {

    //Add elements to form
    public function definition() {
        global $CFG;
        $mform = $this->_form;

        $mform->addElement('header', 'general', 'Client details');

        $mform->addElement('text', 'name', 'Client name', 'maxlength="100" size="25"');
        $mform->setType('name', PARAM_TEXT);        
        $mform->addRule('name', 'Missing Client Name', 'required', null, 'client');

        $mform->addElement('text', 'pocname','Client poc name' , 'maxlength="100" size="25"');
        $mform->setType('pocname', PARAM_RAW_TRIMMED);
        $mform->addRule('pocname', 'Enter poc name', 'required', null, 'client');

        $mform->addElement('text', 'pocphone','Client poc phone number' , 'maxlength="10" size="25"');
        $mform->setType('pocphone', PARAM_RAW_TRIMMED);
        $mform->addRule('pocphone', 'Enter poc phone number', 'required', null, 'client');
        $mform->addRule('pocphone', 'Enter valid phone number', 'numeric', null, 'client');

        $mform->addElement('text', 'email', 'Client poc email address', 'maxlength="100" size="25"');
        $mform->setType('email', PARAM_RAW_TRIMMED);
        $mform->addRule('email', null, 'email', null, 'client');
        $mform->addRule('email', get_string('missingemail'), 'required', null, 'client');


        $mform->addElement('header', 'general', 'Client configuration');    
        
        $readonly ='';
        if(isset($this->_customdata['id'])) {
            $readonly = 'readonly ="readonly"';
        }
        $mform->addElement('text', 'sub_domain', ' sub domain name', $readonly);
        $mform->setType('sub_domain', PARAM_RAW);
        $mform->addRule('sub_domain', 'Missing sub domain name', 'required', null, 'client');
        
        $mform->addElement('text', 'site_url', ' site URL', $readonly);
        $mform->setType('site_url', PARAM_URL);
        $mform->addRule('site_url', 'missing site url', 'required', null, 'server');
        $mform->setDefault('site_url', @$_SERVER['REQUEST_SCHEME'] . '://' . @$_SERVER['SERVER_NAME']);

        $mform->addElement('date_selector', 'start_date', 'Start date');
        $mform->setType('start_date', PARAM_NOTAGS);
        $mform->addRule('start_date', null, 'required', null, 'client');

        $availablefromgroup=array();
        $availablefromgroup[] =& $mform->createElement('date_selector', 'end_date', 'End date');
        $availablefromgroup[] =& $mform->createElement('checkbox', 'availablefromenabled','','Time bound');
        $mform->addGroup($availablefromgroup, 'availablefromgroup', 'End date', ' ', false);
        $mform->disabledIf('availablefromgroup', 'availablefromenabled');

        $mform->addElement('text', 'max_courses', 'Max courses', 'minlength="1"');
        $mform->addRule('max_courses', null, 'numeric', null, 'client');
        $mform->addRule('max_courses', null, 'required', null, 'client');
        $mform->setType('max_courses', PARAM_INT);
        $mform->setDefault('max_courses', 0);

        $mform->addElement('text', 'max_users', 'Max users', 'minlength="1"');
        $mform->addRule('max_users', null, 'numeric', null, 'client');
        $mform->addRule('max_users', null, 'required', null, 'client');
        $mform->setType('max_users', PARAM_INT);
        $mform->setDefault('max_users', 0);

        if (isset($this->_customdata['id'])) {
            $mform->addElement('hidden', 'id', $this->_customdata['id']);
            $mform->setType('id', PARAM_RAW);
        }
        
        $mform->addElement('header', 'general', 'Client stroage');
        $mform->addElement('text', 'max_datastorage', 'Max disk storage in Gigabytes');
        $mform->setType('max_datastorage', PARAM_INT);
        $mform->setDefault('max_datastorage', 0);
        $mform->addRule('max_datastorage', null, 'required', null, 'client');
        $mform->addElement('text', 'max_dbstorage', 'Max database storage in Gigabytes');
        $mform->setType('max_dbstorage', PARAM_INT);
        $mform->setDefault('max_dbstorage', 0);
        $mform->addRule('max_dbstorage', null, 'required', null, 'client');

        if(empty($this->_customdata['id'])){
            $this->add_action_buttons(false, 'Create client');
        } else {
            $this->add_action_buttons(false, 'Update client');
        }
    }

    function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
        
        if(!strpos($data['site_url'], $data['sub_domain'])) {
            $errors['site_url'] = 'Sub domain is not present in site url.';
        }
        $domain = explode('.', $data['site_url']);
        if(!isset($domain[1])) {
            $errors['site_url'] = 'Sub domain not valid url.';
        }
        
        if (!isset($data['id'])) {
            \Multitenant\Core\Utils\DbManager::master_table();
            if ($DB->record_exists('multitenant_master', array('sub_domain' => $data['sub_domain']))) {
                $errors['sub_domain'] = 'Sub domain already exists';
            }
        }
        if(isset($data['availablefromenabled']))
        {
            if($data['end_date'] < strtotime('+15 days', $data['start_date']) )
            {
                $errors['availablefromgroup'] = 'End date must be greater then 15 days';
            }
        }
        return $errors;
    }

}
