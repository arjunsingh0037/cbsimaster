<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class client_change_password_form extends moodleform {

    //Add elements to form
    public function definition() {
        global $CFG;
        $mform = $this->_form;
        $mform->addElement('Hidden', 'id', $this->_customdata['id']);
        $mform->setType('id', PARAM_NOTAGS);
        $options = array();
        foreach($this->_customdata['admins'] as $admin) {
            $options[$admin->id] = $admin->firstname.' '.$admin->lastname;
        }
        $mform->addElement('select', 'userid', 'Select admin', $options);
        $mform->setType('userid', PARAM_RAW);
        $mform->addRule('userid', 'Select admin', 'required'); 
        
        $mform->addElement('password', 'password', 'New password');
        $mform->setType('password', PARAM_RAW);
        $mform->addRule('password', 'New password can\'t empty.', 'required');

        $mform->addElement('password', 'repassword', 'Confirm password');
        $mform->setType('repassword', PARAM_RAW);
        $mform->addRule('repassword', 'Confirm password can\'t empty.', 'required');
        
        $this->add_action_buttons();
    }

    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        if ($data['id'] == 0) {
            $errors['password'] = 'Please select any client !!';
        }
        if ($data['password'] != $data['repassword']) {
            $errors['repassword'] = 'Password mismatch !!';
        }
        return $errors;
    }
}
