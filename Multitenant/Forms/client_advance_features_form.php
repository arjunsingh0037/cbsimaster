<?php

class client_advance_features_form extends moodleform {

    public function definition() {
        global $CFG,$DB, $PAGE, $OUTPUT;
        $mform = $this->_form;
        $pluginman = core_plugin_manager::instance();
        $plugininfo = $pluginman->get_plugins();
        
        $mform->addElement('hidden', 'id', $this->_customdata['id']);
        $mform->setType('id', PARAM_TEXT);

        $advanceconfig = array(
                                'plugins' => 'Plugins',
                                'development'=> 'Development',
                                'assignmentupgradehelper' => 'Assignment upgrade helper',
                                'userpermission' => 'User permission',
                                'coursebackup' => 'Course backup',
                                'advancedfeatures' => 'Advanced features',
                                'security' => 'Security',
                                'appearance' => 'Appearance',
                                'server' => 'Server',
                                'questionengineupgradehelper' => 'Question engine upgrade helper'
                            );    
        foreach ($advanceconfig as $type =>$feature) {
            $mform->addElement('advcheckbox', $type, $feature, '',array('group'=>0), array(0, 1));
            $mform->setType($type, PARAM_INT);
        }
       $this->add_action_buttons(); 
    }
}
