<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class client_setting_form extends moodleform {

    //Add elements to form
    public function definition() {
        global $CFG;
        $mform = $this->_form;
        $themes = core_component::get_plugin_list('theme');
        $options = array();
        foreach ($themes as $key => $useless) {
            $options[$key] = $key;
        }
        $mform->addElement('Hidden', 'id', $this->_customdata['id']);
        
        $mform->setType('id', PARAM_NOTAGS);
        $mform->addElement('select', 'theme', 'Select theme', $options);
        
        $mform->addElement('checkbox', 'passwordpolicy', 'Enable / Disable password policy');
        $mform->addElement('checkbox', 'changeownpassword', 'Change own password');
        
        $mform->addElement('checkbox', 'maintenance_enabled', 'Maintenance mode');
        $mform->addElement('textarea', 'maintenance_message', 'Maintenance mode message', 'wrap="virtual" rows="3" cols="35"');
        $mform->setDefault('maintenance_message',"Your site have been currently in maintenance mode");
        
        
        $mform->addElement('text', 'limitconcurrentlogins', 'Limit concurrent logins');
        $mform->addRule('limitconcurrentlogins', null, 'required', null, 'client');
        $mform->setType('limitconcurrentlogins', PARAM_RAW);
        $mform->setDefault('limitconcurrentlogins',0);


        $mform->addElement('text', 'client', 'Selected client', 'readonly="readonly"');
        $mform->setType('client', PARAM_NOTAGS);
        $mform->setDefault('client', $this->_customdata['clientname']);
        $mform->disable_form_change_checker();

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        if (empty($data['client'])) {
            $errors['client'] = 'Client id can\'t be empty';
        }
        return $errors;
    }

}
