<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * Helper functions
 */

/**
 * 
 * @param type $key
 * @param type $value
 * @param type $extra
 * @return type
 */
function lang($key, $value = '', $extra = '') {
    $key = trim($key);
    $string = array();
    $string['clientmgnr'] = 'Clien Manager';
    $string['createclient'] = 'Add clients';
    $string['clientsettings'] = 'Client settings';
    $string['cliennts'] = 'Clients';
    $string['livereports'] = 'Live login reports';

    $string['coursemngr'] = 'Course Manager';
    $string['createdcourse'] = 'Allocate Courses';
    $string['coursesettings'] = 'Course Settings';
    $string['courses'] = 'Courses';
    $string['coursereports'] = 'Course Reports';

    $string['addclient'] = 'Add client';
    $string['clnname'] = 'Enter client name';
    $string['clnnamemissing'] = 'Missing client name';
    $string['clnemail'] = 'Enter client email';
    $string['clnemailmissing'] = 'Missing client email';
    $string['subdomain'] = 'Enter sub domain name';
    $string['subdomainmissing'] = 'Missing sub domain name';
    $string['siteurl'] = 'Enter site URL';
    $string['siteurlmissing'] = 'Missing site URL';
    $string['startdate'] = 'Start date';
    $string['enddate'] = 'End date';
    $string['maxcourses'] = 'Start date';
    $string['dbconn'] = 'trying to connect {$a} database failed.';
    $string['themechange'] = 'theme {$b} has been changed for the {$a} client';
    $string['maintenancemessage'] = 'maintenance message : {$b} has been changed for the {$a} client';
    $string['deactivemessage'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['passwordpolicy'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['maintenanaceenabled'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['limitconcurrentlogins'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['changeownpassword'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['cacheclear'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['rolecapabilitiesinsert'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['rolecapabilitiesupdate'] = 'deactive message :" {$b} "has been changed for the {$a} client';
    $string['dirwriteerror'] = 'Unable to create directry the {$a} client';
    $string['xmlwriteerror'] = 'failed to write xml for {$a} client';
    $string['clientenvsetup'] = 'Client {$a} enviroment setup successfully';

    if (isset($string[$key])) {
        $str = '';
        if (!empty($extra)) {
            $str = str_replace('{$b}', $extra, $string[$key]);
        } else {
            $str = $string[$key];
        }
        return str_replace('{$a}', $value, $str);
    }
}

/**
 * 
 * @global type $CFG
 * @param type $page
 * @return \block_contents
 */
function navigation_menu($page) {
    global $CFG;
    $client = array(
        'activeinactive',
        'dashboard',
        'createclient',
        'clientsettings',
        'clients',
        'changepassword',
        'livereports',
        'plugins',
        'advancefeatures',
        'currentsettings'
    );
    $course = array('courseallocation', 'coursesettings', 'courses', 'coursereports');
    $clientflag = false;
    if (in_array($page, $client)) {
        $clientflag = true;
    }
    $courseflag = false;
    if (in_array($page, $course)) {
        $courseflag = true;
    }
    $bc = new block_contents();
    $bc->title = 'Multitenant';
    $web = $CFG->wwwroot . '/Multitenant/web/';
    $content = '<ul class="collapsibleList" type="none" style="margin: 0 0 10px 0px;">
                 <li>
                 <label for="mylist-node1"><img src="pix/' . ($clientflag ? 'expanded' : 'collapsed') . '.svg"> Client manager</label>
                 <input type="checkbox" id="mylist-node1" ' . ($clientflag ? 'checked="checked"' : '') . ' />
                 <ul type="square">
                     <li><a ' . ($page == "createclient" ? 'class="active"' : '') . ' href="' . $web . 'create_client.php' . '">Add client</b></li>
                     <li><a ' . ($page == "clientsettings" ? 'class="active"' : '') . ' href="' . $web . 'client_settings.php' . '">Clients settings</a></li>
                     <li><a ' . ($page == "clients" ? 'class="active"' : '') . ' href="' . $web . 'clients.php' . '">Clients</a></li>
                     <li><a ' . ($page == "activeinactive" ? 'class="active"' : '') . ' href="' . $web . 'client_activeinactive.php' . '">Client active/inactive</a></li>
                     <li><a ' . ($page == "changepassword" ? 'class="active"' : '') . ' href="' . $web . 'client_changepassword.php' . '">Client change password</a></li>
                     <li><a ' . ($page == "plugins" ? 'class="active"' : '') . ' href="' . $web . 'client_plugins.php' . '">Client plugins</a></li>
                     <li><a ' . ($page == "advancefeatures" ? 'class="active"' : '') . ' href="' . $web . 'client_advancefeatures.php' . '">Client site administration menu</a></li>
                     <li><a ' . ($page == "currentsettings" ? 'class="active"' : '') . ' href="' . $web . 'client_currentsettings.php' . '">Client current settings</a></li>
                     <li><a ' . ($page == "livereports" ? 'class="active"' : '') . ' href="' . $web . 'live_login_reports.php' . '">Live login reports</a></li>
                     <li><a href="' . $CFG->wwwroot . '/blocks/configurable_reports/managereport.php?courseid=1' . '" target="_blank">Configurable Reports</a></li>
                  </ul>
                  </li>
                  <li>
                    <label for="mylist-node2"><img src="pix/' . ($courseflag ? 'expanded' : 'collapsed') . '.svg"> Course manager</label>
                    <input type="checkbox" id="mylist-node2" ' . ($courseflag ? 'checked="checked"' : '') . ' />
                    <ul type="square">
                        <li><a ' . ($page == "courseallocation" ? 'class="active"' : '') . ' href="' . $web . 'course_allocation.php' . '">Course allocation</b></li>
                    </ul>
                  </li>
                </ul>';
    $bc->content = $content;
    $bc->collapsible = 1;
    $bc->dockable = true;
    return $bc;
}

/**
 * 
 * @global type $DB
 * @return type
 */
function loggedin_users() {
    global $DB;
    $timetoshowusers = 100; //Seconds default  
    $now = time();
    $timefrom = 100 * floor(($now - $timetoshowusers) / 100); // Round to nearest 100 seconds for better query cache

    $paramss['now'] = $now;
    $paramss['timefrom'] = $timefrom;

    $groupmembers = "";
    $groupselect = "";
    $userfields = 'id,firstname, lastname,lastip';
    $sql = "SELECT 
    $userfields, MAX(u.lastaccess) AS lastaccess FROM {user} u $groupmembers 
     WHERE u.lastaccess > :timefrom  AND u.lastaccess <= :now  $groupselect 
     GROUP BY $userfields ORDER BY lastaccess DESC ";
    $loggedinusers = $DB->get_records_sql($sql, $paramss, 0, 50);

    return $loggedinusers;
}

/**
 * 
 * @param type $bytes
 * @param type $precision
 * @return type
 */
function size_format($bytes, $precision = 2) {

    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        return round($bytes, $precision) . ' B';
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        return round($bytes / $kilobyte, $precision) . ' KB';
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        return round($bytes / $megabyte, $precision) . ' MB';
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        return round($bytes / $gigabyte, $precision) . ' GB';
    } elseif ($bytes >= $terabyte) {
        return round($bytes / $terabyte, $precision) . ' TB';
    } else {
        return round($bytes, $precision) . ' B';
    }
}

function category_tree($parent = 0, $optional = true) {
    global $DB;
    $data = array();
    $resultset = $DB->get_records('course_categories', array('parent' => (int) $parent));
    if ($resultset) {
        foreach ($resultset as $id => $row) {
            if ($optional || $id != 1) {
                $data[$id] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'courses' => get_courses($row->id, '', 'c.id,c.fullname'),
                    'flag' => category_tree($row->id)
                );
            }
        }
    }
    return $data;
}
