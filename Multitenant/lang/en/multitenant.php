<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['clientmgnr'] = 'Clien Manager';
$string['createclient'] = 'Add clients';
$string['clientsettings'] = 'Client settings';
$string['cliennts'] = 'Clients';
$string['livereports'] = 'Live login reports';

$string['coursemngr'] = 'Course Manager';
$string['createdcourse'] = 'Allocate Courses';
$string['coursesettings'] = 'Course Settings';
$string['courses'] = 'Courses';
$string['coursereports'] = 'Course Reports';

$string['addclient'] = 'Add client';
$string['clnname'] = 'Enter client name';
$string['clnnamemissing'] = 'Missing client name';
$string['clnemail'] = 'Enter client email';
$string['clnemailmissing'] = 'Missing client email';
$string['subdomain'] = 'Enter sub domain name';
$string['subdomainmissing'] = 'Missing sub domain name';
$string['siteurl'] = 'Enter site URL';
$string['siteurlmissing'] = 'Missing site URL';
$string['startdate'] = 'Start date';
$string['enddate'] = 'End date';
$string['maxcourses'] = 'Start date';
$string['dbconn'] = 'trying to connect {$a} database failed.';
$string['themechange'] = 'theme {$b} has been changed for the {$a} client';
$string['themechange'] = 'theme {$b} has been changed for the {$a} client';
$string['maintenancemessage'] = 'maintenance message : {$b} has been changed for the {$a} client';
$string['deactivemessage'] = 'deactive message :" {$b} "has been changed for the {$a} client';
$string['passwordpolicy'] = 'deactive message :" {$b} "has been changed for the {$a} client';
$string['maintenanaceenabled'] = 'deactive message :" {$b} "has been changed for the {$a} client';
$string['limitconcurrentlogins'] = 'deactive message :" {$b} "has been changed for the {$a} client';
$string['changeownpassword'] = 'deactive message :" {$b} "has been changed for the {$a} client';
$string['cacheclear'] = 'deactive message :" {$b} "has been changed for the {$a} client';
$string['rolecapabilitiesinsert'] = 'deactive message :" {$b} "has been changed for the {$a} client';

