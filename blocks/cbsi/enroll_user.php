<?php
require_once('../../config.php');
require_once($CFG->dirroot. "/enrol/manual/externallib.php");

/*
    /block/cbsi/users.php passes in array of user ids to enroll
*/
//$users             = array($_POST['users']); //debugging using REST tool
$users             = $_POST['users'];
$courseid          = $_POST['courseid'];

// $users    = array('149'); //debug
// $courseid = 549;          //debug
$failed_enrollment = array();

/**
 * @param $userid
 * @param $courseid
 * @return void
 */
function enroll_user($userid, $courseid) {
    global $failed_enrollment;
    $user = new stdClass();
    $user->id = $userid;

    $course = new stdClass();
    $course->id = $courseid;

    $enrollments = array(
      array(
        'roleid'    => 5,
        'userid'    => $user->id,
        'courseid'  => $course->id,
        'timestart' => null,
        'timeend'   => null,
        'suspend'   => null
        )
    );

    $contextid = context_course::instance($courseid);

    if(has_capability('enrol/manual:enrol', $contextid)) {
        try {
            enrol_manual_external::enrol_users($enrollments);
        } catch (Exception $e) {
            array_push($failed_enrollment, $userid);
            //echo $e->getMessage();
            //die(__FILE__.":".__LINE__);
        }
    }
}

foreach($users as $userid) {
    //psuedo code
    enroll_user($userid, $courseid);
}

//set response type to JSON for easy jQuery parsing
header('Content-Type: application/json');

if(count($failed_enrollment) === 0) {
    echo json_encode($failed_enrollment);
} else {
    //send back list of failed registrations
    $html = "The following user IDs could not be registered for this course. Please inform the site creator.\n";
    foreach($failed_enrollment as $f) {
        $html .= "$f\n";
    }

    echo json_encode($html);
}
