<?php require_once('nav.php');?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <?php include_once('head.php');?>
    <div id="container" class="wizard">
    <?php echo $navTop;?>
    <div id="page1" class="active">
        <?php include('page1.php');?>
    </div>

    <div id="page2" class="inactive">
        <?php include('page2.php');?>
    </div>

    <div id="page3" class="inactive">
        <?php include('page3.php');?>
    </div>

    <div id="page4" class="inactive">
        <?php include('page4.php');?>
    </div>

    <div id="page5" class="inactive">
        <?php include('page5.php');?>
    </div>

    <?php //echo $navBottom;?>

    </div>

    <script>
        $(document).ready(function() {
            var current_page = 1;
            var page_max     = 5;
            var course       = [];
            var category     = null;
            var course_name  = null;
            var language     = null;
            var description  = null;
            var startdate    = null;
            var starttime    = null;
            var enddate      = null;
            var endtime      = null;
            var home         = "http://<?php echo $_SERVER['HTTP_HOST']?>";
            /*
             * the following variables are set by data returned by ajax calls
             * they will return errors if not accessed via set and get methods
             * methods after that call.
             */
            var course_url   = null;
            var course_id    = null;
            var bbb_url      = null; //url for big blue button activity setup

            function set_current_page(id) {

                if(id <= 0) {
                    current_page = 1;
                    return;
                }

                if(id > page_max) {
                    current_page = page_max;
                    return;
                }

                return current_page = id;
            }

            function get_current_page() {
                return current_page;
            }

            function get_category() {
                return category;
            }

            function get_course_name() {
                return $('#course_name').val();
            }

            function get_language() {
                return $('#language').val();
            }

            function get_description() {
                return $('#description').val();
            }

            function get_startdate() {
                return $('#class_begin_datetxt').val();
            }

            function get_starttime() {
                return $('#class_begin_timetxt').val();
            }

            function get_enddate() {
                return $('#class_end_datetxt').val();
            }

            function get_endtime() {
                return $('#class_end_timetxt').val();
            }

            function create_course() {
                course.push(get_course_name());
                course.push(get_description());
                course.push(get_category());
                course.push(get_language());
                course.push(get_startdate());
                course.push(get_starttime());
                course.push(get_enddate());
                course.push(get_endtime());
                //console.log(course);

                //send course via ajax for actual creation
                /*
                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {course : course},
                    dataType: "json",
                    success: function(data) {
                        //clear selected users list
                        //console.log("ajax reeturn" + data);
                        course_url = "/course/view.php?id=" + data.course_id;
                        bbb_url = "/course/modedit.php?add=bigbluebuttonbn&type=&course=" + data.course_id + "&section=0&return=0&sr=";
                    },
                    error: function(data) {
                        console.log(data);
                        //alert("There was a problem. Please notify the site creator.");
                    }
                });

                course = [];
                */
            }

            function pager() {

                for(i = 0; i < page_max;i++) {
                    if( $('.nav > ul > li > input').eq(i).val() <= get_current_page()) {
                        $('.nav > ul > li > input').eq(i).parent().addClass("done");
                    } else {
                        $('.nav > ul > li > input').eq(i).parent().removeClass("done");
                    }

                    // if( $('.bottomNav > .nav > ul > li > input').eq(i).val() <= get_current_page()) {
                    //     $('bottomNav > .nav > ul > li > input').eq(i).parent().addClass("done");
                    // } else {
                    //     $('bottomNav > .nav > ul > li > input').eq(i).parent().removeClass("done");
                    // }
                }
            }

            function set_course_id(id) {
                course_id = id;
            }

            function get_course_id() {
                return course_id;
            }

            function set_course_url() {
                course_url = "/course/view.php?id=" + get_course_id();
            }

            function get_course_url() {
                return course_url;
            }

            function set_bbb_url() {
                bbb_url = "/course/modedit.php?add=bigbluebuttonbn&type=&course=" + get_course_id() + "&section=0&return=0&sr=";
            }

            function get_bbb_url() {
                return bbb_url;
            }

            //set deafault page
            set_current_page(current_page);

            $("#what_to_do").click(function() {
                $("#page1").removeClass("active");
                $("#page1").addClass("inactive");
                $("#page2").removeClass("inactive");
                $("#page2").addClass("active");
                set_current_page(2);
                pager();
            });

            $("#meeting").click(function() {
                category = "Meeting";
                $("#page2").removeClass("active");
                $("#page2").addClass("inactive");
                $("#page3").removeClass("inactive");
                $("#page3").addClass("active");

                set_current_page(3);
                pager();
            });

            $("#training").click(function() {
                category = "Training";
                $("#page2").removeClass("active");
                $("#page2").addClass("inactive");
                $("#page3").removeClass("inactive");
                $("#page3").addClass("active");

                set_current_page(3);
                pager();
            });

            $('#class_begin_timetxt').timepicker({ 'timeFormat': 'H:i:s' });

            // Date Functionality
            $( "#datepicker" ).datepicker({
                showOn: "both",
                buttonImage: "http://cbsi-connect.net/theme/msu/pix/bluecalendar.png",
                buttonImageOnly: true,
                dateFormat: "yy-mm-dd",
                minDate: 0
              });

            $('#set_date_yes').click(function() {
                $('#course-dates').toggle();
            })

            $('#set_date_no').click(function() {
                $('#datepicker').val('');
                $('#class_begin_timetxt').val('');
                $('#class_begin_date').hide();

                $("#page4").removeClass("active");
                $("#page4").addClass("inactive");
                $("#page5").removeClass("inactive");
                $("#page5").addClass("active");

                set_current_page(4);
                pager();
            });

            $('#save_home').click(function() {
                //send course data via ajax for course creation
                //redirect to homepage
                create_course();

                //send course data via ajax for actual creation
                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {course : course},
                    dataType: "json",
                    success: function(data) {
                        //clear selected users list
                        // console.log("ajax return:");
                        // console.log(data);
                        set_course_id(data.course_id);
                        set_course_url(data.course_id);
                        set_bbb_url(data.course_id);

                        course = [];
                        window.location.replace("/");
                    },
                    error: function(data) {
                        //console.log(data);
                        //alert("There was a problem. Please notify the site creator.");
                    }
                });
            });

            $('#delete_home').click(function() {
                //clear out course values
                course = [];
                        //redirect home
                window.location.replace("/");
            });

            $('#save_start').click(function() {
                //send course data via ajax for course creation
                create_course();

                //send course data via ajax for actual creation
                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {course : course},
                    dataType: "json",
                    success: function(data) {
                        //clear selected users list
                        // console.log("ajax return:");
                        // console.log(data);
                        set_course_id(data.course_id);
                        set_course_url(data.course_id);
                        set_bbb_url(data.course_id);

                        course = [];

                        if(category == "Meeting") {
                            window.location.replace(get_bbb_url());
                        }

                        if(category == "Training") {
                            window.location.replace(get_course_url());
                        }
                    },
                    error: function(data) {
                        //console.log(data);
                        //alert("There was a problem. Please notify the site creator.");
                    }
                });


            });

            $('#next').click(function(){
                //calculate next page number
                var next_page = get_current_page() + 1;

                if(next_page <= page_max) {
                    $('#page' + get_current_page()).removeClass('active');
                    $('#page' + get_current_page()).addClass('inactive');
                    $('#page' + next_page).removeClass('inactive');
                    $('#page' + next_page).addClass('active');

                    set_current_page(next_page);

                    if(get_current_page() == page_max) {
                        $("#next").removeClass('wizardButton');
                        $("#next").addClass('wizardButtonDisabled');
                    }

                    if(next_page == 2) {
                        $("#previous").removeClass('wizardButtonDisabled');
                        $("#previous").addClass('wizardButton');
                    }

                } else {
                    set_current_page(page_max);
                }

                pager();
            });

            $('#previous').click(function(){
                //calculate next page number
                var previous_page = get_current_page() - 1;
                if(previous_page >= 1) {
                    $('#page' + get_current_page()).removeClass('active').addClass('inactive');
                    $('#page' + previous_page).removeClass('inactive');
                    $('#page' + previous_page).addClass('active');
                    set_current_page(previous_page);
                }

                if(previous_page == 1) {
                    $("#previous").removeClass('wizardButton');
                    $("#previous").addClass('wizardButtonDisabled');
                }

                if(get_current_page() < page_max) {
                    $("#next").removeClass('wizardButtonDisabled');
                    $("#next").addClass('wizardButton');
                }

                pager();
            });

            $('.exitWizard').click(function() {
                window.location.replace("/");
            });
        });
    </script>
    </body>
</html>
