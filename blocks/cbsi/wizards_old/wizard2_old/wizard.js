function ajaxRequest(){
  var activexmodes=["Msxml2.XMLHTTP", "Microsoft.XMLHTTP"];

  if (window.ActiveXObject)
  {
    for (var i=0; i<activexmodes.length; i++){
      try
      {
      return new ActiveXObject(activexmodes[i]);
      } catch(e)
      {
       alert("JS: There is a problem with the WS.");
      }
    }
  } else if (window.XMLHttpRequest)
  {
    return new XMLHttpRequest();
  } else {
    alert("JS: There is a problem with the WS.");
    return false;
  }
}

function ajaxget(userid,subcat,filter,divid,pagename,myurl){
    var myresponse=new ajaxRequest();

    myresponse.onreadystatechange=function(){
     if (myresponse.readyState==4){
      if (myresponse.status==200 || window.location.href.indexOf("http")==-1){
             var pre_content=JSON.parse(myresponse.responseText);
             var content="";
             for(var x=0; x<pre_content.length;x++){
                content += '<input type="radio" onclick="document.getElementById(\'changeCourseID\').value=this.value;this.form.submit();" name="selecting_course_from_category" value="'+pre_content[x].id+'">'+pre_content[x].fullname;
             }
            document.getElementById(divid).innerHTML = content;
      }else{
       alert("JS: There is a problem with the WS.");
      }
     }
    }
              myresponse.open("GET", myurl+"?id="+userid+"&filter="+filter+"&subcat="+subcat, true);
              myresponse.send(null);


}




$(document).ready(function() {

  $('#class_begin_timetxt').timepicker({ 'timeFormat': 'H:i:s' });
  $('#class_end_timetxt').timepicker({ 'timeFormat': 'H:i:s' });

  // Date Functionality
  $( "#class_begin_datetxt" ).datepicker({
        showOn: "both",
        buttonImage: "http://cbsi-connect.net/theme/msu/pix/bluecalendar.png",
        buttonImageOnly: true,
        dateFormat: "yy-mm-dd",
        minDate: 0
      });

  $( "#class_end_datetxt" ).datepicker({
        showOn: "both",
        buttonImage: "/theme/msu/pix/bluecalendar.png",
        buttonImageOnly: true,
        dateFormat: "yy-mm-dd",
        minDate: 0
      });

  // Send browser back to home page
  $('p.home').click(function() {
  window.top.location.href = "http://cbsi-connect.net/";

    });

  // Send browser back to home page
  $('.exitWizard').click(function() {
  window.top.location.href = "http://cbsi-connect.net/";
  });


  // Add border and over flow to selected academies on click
  $('.showHiddenCountries').click(function() {
  $('#cohort_selection').css({"border": "2px solid #4f617a", "overflow-y": "scroll"}).show();
    });

  // Date field pop up
   $('.datesYes').click(function(){
  $('.dateChoice').show();

   });





  // Disable all numbers
  $('.nav ul li input').prop('disabled', true);


  // Disable all but pageone
  if($('form').attr('id') === 'pageone') {
  $('.nav ul li:nth-child(1)').addClass('done');
  $('.nav ul li:nth-child(1) input').prop('disabled', false);

  // Disable all but pageone and pagetwo
  }else if ($('form').attr('id') === 'pagetwo')  {
  $('.nav ul li:nth-child(1), .nav ul li:nth-child(2)').addClass('done');
  $('.nav ul li:nth-child(1) input, .nav ul li:nth-child(2) input').prop('disabled', false);

  // Enable pageone, pagetwo and pagethree
  }else if ($('form').attr('id') === 'pagethree')  {
  $('.nav ul li:nth-child(1), .nav ul li:nth-child(2), .nav ul li:nth-child(3)').addClass('done');
  $('.nav ul li:nth-child(1) input, .nav ul li:nth-child(2) input, .nav ul li:nth-child(3) input').prop('disabled', false);

  // Enable pageone, pagetwo, pagethree, and pagefour
  }else if ($('form').attr('id') === 'pagefour')  {

  // Clear step 4 fields
  $('.courseName').attr('value', '');
  $('textarea').empty();
  $('select[name="course_language"]').prop('selectedIndex', 0);
  $('select[name="course_sections"]').prop('selectedIndex', 0);

  $('.nav ul li:nth-child(1),  .nav ul li:nth-child(2), .nav ul li:nth-child(3),.nav ul li:nth-child(4)').addClass('done');
  $('.nav ul li:nth-child(1) input, .nav ul li:nth-child(2) input, .nav ul li:nth-child(3) input, .nav ul li:nth-child(4) input').prop('disabled', false);



  // Form Validation
  $("#pagefour input[name='next']").click(function() {

   $('.errors, .errors1, .errorsLan').remove();

   if($('.courseName').val() === '') {
      $('.courseName').after('<span class="errors">Please add a course name!</span>');
    errors = true;
      return false;
   }

    if($('select[name="course_language"] option:Selected').text() === "Select") {
        $('.languageName').after('<span class="errorsLan">Please select an option!</span>');
    errors = true;
      return false;
   }

    if($('textarea').val() === '') {
        $('textarea').after('<span class="errors1">Please add a course description!</span>');
    errors = true;
      return false;
   }


     if($('select[name="course_sections"] option:Selected').text() === "Select") {
        $('.topicNumber').after('<span class="errorsLan">Please select an option!</span>');
    errors = true;
      return false;
   }

    return true;




   });






  // Enable  pageone, pagetwo, pagethree, pagefour, and page six
  }else if ($('form').attr('id') === 'pagesix')  {
  $('.nav ul li').addClass('done');
  $('.nav ul li input').prop('disabled', false);


  // Enable  all pages
  }else if ($('form').attr('id') === 'pageseven')  {
  $('.nav ul li').addClass('done');
  $('.nav ul li input').prop('disabled', false);
  }


});









