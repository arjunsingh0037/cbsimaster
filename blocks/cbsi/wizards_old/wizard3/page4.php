<h2>Set the start date of the course?</h2>
<?php /*
<div class="wizardChoices">
    <div class="wizardChoice">
        <p>No:<br>Decide Later</p>
    </div><!-- end wizardChoice -->
    <div class="wizardChoice">
        <p class="datesYes" id="set_date_yes">Yes:<br> Set Date</p>
    </div><!-- end wizardChoice -->
    <div class="wizardChoice">
        <div class="formWrap" id="class_begin_date" style="display:none;">
            <tr>
                <td align="right" valign="top"><b>When do you want this course to begin?</b></td>
                <td valign="top" align="left">  <input id="datepicker" name="class_begin_datetxt" type="text" /></td>
            </tr>

            <img src="/theme/msu/pix/ClockGraphic.png" width="39" height="39" class="clock" />

            <tr>
                <td align="right" valign="top"><b> Choose a time:</b></td>
                <td valign="top" align="left">  <input id="class_begin_timetxt" name="class_begin_timetxt" type="text" /></td>
            </tr>
        </div><!-- end formWrap -->
    </div><!-- end wizardChoices -->
</div><!-- end wizardChoices -->
<div style="clear:both"></div>
*/
?>
<div class="wizardChoices">
    <div class="wizardChoice">
        <p>No:<br>Decide Later</p>
    </div><!-- end wizardChoice -->
    <div class="wizardChoice">
        <p class="datesYes" id="set_date_yes">Yes:<br> Set Date</p>
    </div><!-- end wizardChoice -->
    <div style="clear:both"></div>
    <div id="course-dates" style="display:none">
        <form id="class-begin" class="wizardChoice">
            <label for="class_begin_datetxt"><b>When do you want this course to begin?</b></label>
            <input id="class_begin_datetxt" name="class_begin_datetxt" type="text" />

            <label for="class_begin_timetxt"><b> Choose a start time:</b></label>
            <input id="class_begin_timetxt" name="class_begin_timetxt" type="text" />
            <img src="/theme/msu/pix/ClockGraphic.png" width="39" height="39" class="clock"/>
        </form>
        <br/><br/>
        <div style="clear:both"></div>
        <form id="class-end" class="wizardChoice">
            <label for="class_end_datetxt"><b>When do you want this course to end?</b></label>
            <input id="class_end_datetxt" name="class_end_datetxt" type="text"/>

            <label for="class_end_timetxt"><b> Choose an end time:</b></label>
            <input id="class_end_timetxt" name="class_end_timetxt" type="text" />
            <img src="/theme/msu/pix/ClockGraphic.png" width="39" height="39" class="clock" />
        </form>
    </div><!-- end course dates -->
</div>

