<?php
require_once('../../../config.php');
global $CFG,$USER,$PAGE;
require_once($CFG->dirroot.'/blocks/cbsi/lib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot. '/enrol/locallib.php');

require_login();

$course_title = $_POST['course'][0];
$description  = $_POST['course'][1];
$category     = $_POST['course'][2];
$language     = $_POST['course'][3];
$startdate    = $_POST['course'][4];
$starttime    = $_POST['course'][5];

$date     = explode('-', $startdate);
$time     = explode(":", $starttime);
$hour     = $time[0];
$minute   = $time[1];
$second   = $time[2];
$month    = $date[1];
$day      = $date[2];
$year     = $date[0];
$datetime = mktime($hour, $minute, $second, $month, $day, $year);
$data     = new stdClass();

if($category == "Meeting") {
    $data->category = get_meeting_id();
}

if($category == "Training") {
    $data->category = get_training_id();
}

$data->fullname   = $course_title;
$data->shortname  = substr($course_title,0,3). rand(5,15);
$data->summary    = $description;
$data->lang       = $language;
$data->format= "topics"; //default of topics
//if category is meeting, set course format to singleactivity
// if($category == "Meeting")
// {
//     $data->format = "singleactivity";
// } else {
//     $data->format= "topics"; //default of topics
// }

$data->visible    = 1;
$data->sortorder  = 0;
$data->idnumber   = "";
$data->showgrades = 1;
$data->newsitems  = 0;
$data->startdate  = $datetime;
$data->marker     = 0;
$data->maxbytes   = 0;

try {
    $course = create_course($data);
} catch (Exception $e) {
    echo $e->getMessage();
}

try {
    //enroll course creator into course
    $context   = context_course::instance($course->id);
    $manager   = new course_enrolment_manager($PAGE, $course);
    $instances = $manager->get_enrolment_instances();
    $today     = date('U');
    $role_id   = 9; //content contributor

    //find the manual one
    foreach ($instances as $instance) {
        if ($instance->enrol == 'manual') {
            break;
        }
    }

    $plugins = $manager->get_enrolment_plugins();
    $plugin  = $plugins['manual'];
    $plugin->enrol_user($instance, $USER->id, $role_id, $today, 0);

    //return course id for processing on return page
    //set response type to JSON for easy jQuery parsing
    header('Content-Type: application/json');
    echo json_encode(array('course_id' => $course->id));
} catch(Exception $e) {
    header('Content-Type: application/json');
    echo json_encode(array("error" => $e->getMessage()));
}

