<?php
require_once('../../config.php');
require_once('lib.php');
global $DB, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('open-courses', 'theme_msu'));
$PAGE->set_heading(get_string('open-courses', 'theme_msu'));
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/open_courses.php');
$PAGE->navbar->add(get_string('open-courses', 'theme_msu'), new moodle_url($CFG->wwwroot . '/blocks/cbsi/open_courses.php'));

echo $OUTPUT->header();
require_login();

$html = '';
//get shared courses, where 4 is the categoryid for shared courses
$courses = get_shared_courses(4);

$html .= '
<div>
        <h2>'.get_string('open-courses', 'theme_msu').'</h2>
        <p>'. get_string('open-courses-header', 'theme_msu').'</p>
        <!--
        <div class="line-right ax_horizontal_line"></div>
        <div id="">
        <button class="showMore">'.get_string('show-more', 'theme_msu').'</button>
          <ul>
             <li><a href="#tabs-1">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
             <li><a href="#tabs-2">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
             <li><a href="#tabs-3">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
          </ul>
          -->';

$html .= '
      <div id="tabs-1" class="extra">
        <ul>';

            foreach($courses as $m)
            {
                if($m->visible = 1)
                {
                  $a = get_nstudents($m->id);
                  $s = get_nseats($m->id);
                  $so = $s - $a;

                  $html .= '
                    <li>
                    <div class="tabInfo">
                    <img src="'. get_course_image_url($m->id) .'" class="first-img" />
                    <div class="tabContent">
                    <a href="/course/view.php?id=' . $m->id . '"><h2>' . $m->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . '<!--&nbsp;at ' . gmdate('H:i', $m->startdate) . 'hrs--></span></div><!--end tabContent --><br />
                    <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>   </div><!--end tabInfo -->
                    <div class="tabAction">
                    <div class="arrow">
                    <!-- <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />--></div><!--end gear-->
                    <div class="buttons tabButton">
                    <a href="http://cbsi-connect.net/course/view.php?id=' . $m->id .'"><button>Start/Join</button></a>';

                    if (has_capability('moodle/course:update', context_course::instance($m->id)))
                    {
                      $html .= '<a href="/course/view.php?id=' . $m->id . '&sesskey=' . $USER->sesskey . '&edit=on"><button>Manage</button></a>';
                      $html .= '<a href="/course/edit.php?id=' . $m->id . '"><button>Edit</button></a>';
                    }

                  $html .=  '</div><!--end buttons--></div><!--end tabAction -->
                    </li>
                  ';
                }
            }

            $html .= '
         </ul>
      </div><!-- end tabs-1 -->';

echo $html;
