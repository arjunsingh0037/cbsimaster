<h2><?php echo get_string('final_page_header', 'block_cbsi'); ?></h2>
<div class="wizardLeft">
    <span class="or"><?php echo get_string('or', 'block_cbsi'); ?></span>
    <div id="save_home" >
        <p><?php echo get_string('save_go_home', 'block_cbsi'); ?></p>
    </div><!-- end wizardChoice -->
    <div id="delete_home">
        <p><?php echo get_string('delete_go_home', 'block_cbsi'); ?></p>
    </div><!-- end wizardChoice -->
</div><!-- end wizardLeft -->

<div id="save_start" class="wizardRight">
    <div class="wizardChoice">
        <p><?php echo get_string('save_start', 'block_cbsi'); ?></p>
    </div><!-- end wizardChoice -->
</div><!-- end wizardright -->

<div style="clear:both"></div>
