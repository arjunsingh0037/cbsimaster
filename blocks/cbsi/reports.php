 <?php
require_once('../../config.php');
require_once('report_functions.php');

global $DB, $USER, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
//$PAGE->set_title(get_string('sidebar-manage-courses', 'theme_msu'));
$PAGE->set_title('CBSI Reports');
//$PAGE->set_heading(get_string('sidebar-manage-courses', 'theme_msu'));
$PAGE->set_heading("CBSI Reports");
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/reports.php');
$PAGE->navbar->add(get_string('sidebar-manage-courses', 'theme_msu'), new moodle_url($CFG->wwwroot . '/blocks/cbsi/manage_content.php'));

require_once("{$CFG->dirroot}/blocks/cbsi/lib.php");
echo $OUTPUT->header();
require_login();

//echo totalOfficersTrainedByGender();
//echo siteAdminsTrainedByGender();
//echo instructorsTrainedByGender();
//echo print_r(getCoursesByInstitution());
echo getCoursesByInstitution()
