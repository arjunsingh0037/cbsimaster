<?php

function xmldb_block_cbsi_upgrade($oldversion = 0) {
    global $DB;
    $dbman = $DB->get_manager();

    $result = true;

    /// Add a new column newcol to the mdl_myqtype_options
    if ($oldversion < 2014102800) {

        // Define table block_cbsi to be created.
        $table = new xmldb_table('block_cbsi');

        // Adding fields to table block_cbsi.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '9', null, XMLDB_NOTNULL, null, null);
        $table->add_field('course_creator', XMLDB_TYPE_INTEGER, '9', null, XMLDB_NOTNULL, null, null);
        $table->add_field('start_datetime', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('end_datetime', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table block_cbsi.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for block_cbsi.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Cbsi savepoint reached.
        upgrade_block_savepoint(true, 2014102800, 'cbsi');
    }

    if ($oldversion < 2015010500) {

        // Define field default_shortname to be added to block_cbsi.
        $table = new xmldb_table('block_cbsi');
        $field = new xmldb_field('default_shortname', XMLDB_TYPE_TEXT, null, null, null, null, null, 'end_datetime');

        // Conditionally launch add field default_shortname.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Cbsi savepoint reached.
        upgrade_block_savepoint(true, 2015010500, 'cbsi');
    }

    if ($oldversion < 2015020200) {

        // Define field categoryid to be added to block_cbsi.
        $table = new xmldb_table('block_cbsi');
        $field = new xmldb_field('categoryid', XMLDB_TYPE_INTEGER, '4', null, null, null, null, 'default_shortname');

        // Conditionally launch add field categoryid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Cbsi savepoint reached.
        upgrade_block_savepoint(true, 2015020200, 'cbsi');
    }

    if ($oldversion < 2015080302) {

        // Define table block_cbsi to be created.
        $table = new xmldb_table('block_cbsi');
        $field = new xmldb_field('parent_category', XMLDB_TYPE_INTEGER, '3', null, null, null, null);

        // Conditionally launch create table for block_cbsi.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Cbsi savepoint reached.
        upgrade_block_savepoint(true, 2015080302, 'cbsi');
    }

    return $result;
}
