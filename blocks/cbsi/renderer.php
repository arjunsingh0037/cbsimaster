<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * course_overview block rendrer
 *
 * @package    block_course_overview
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

/**
 * Course_overview block rendrer
 *
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('lib.php');

class block_cbsi_renderer extends plugin_renderer_base {

  /**
   * Meetings Page
   * @return html
   */
  public function meetings() {
    $html  = '';
    $meetings = get_courses(get_meeting_id());

    $html .= '

          <div>
            <h2>' . get_string('tab-meetings', 'theme_msu') . '</h2>
            <div class="line-right ax_horizontal_line"></div>';
/*
            <div id="tabs">
              <ul>
                 <li><a href="#tabs-1">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-2">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-3">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
              </ul>';
*/
    $html .= '
          <div id="tabs-1" class="extra">
            <ul>';

    foreach($meetings as $m)
    {

      $a = get_nstudents($m->id);
      $s = get_nseats($m->id);
      $so = $s - $a;

      $html .= '
              <li>
                <div class="tabInfo">
                  <img src="' . get_course_image_url($m->id). '" class="first-img" />
                  <div class="tabContent">
                    <a href="/course/view.php?id=' . $m->id .'"><h2>' . $m->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . ' at ' . gmdate('H:i', $m->startdate) . 'hrs</span>
                  </div><!--end tabContent --><br />
                  <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
                </div><!--end tabInfo -->
              </li>
            ';
    }

    $html .= '
          </div><!-- end tabs -->
        <div>
              ';

    $html .= '<script>
      $(document).ready(function() {

      var currentPage =  $("form").attr("id");

        var newHeight = $("div.left-column").outerHeight() + 40;
        var unit = "px";
        var cssHeight = newHeight + unit;
        $("div.right-column, iframe").css("min-height", cssHeight);


       if($( "iframe" ).contents().find( "pagelayout-frontpage")) {


      }else {

        alert("didnt work");
      }

      $( "#tabs").tabs();
      $("#tabs1").tabs({active: 10});
      $("#tabs2").tabs({active: 10});

      $("ul").each(function() {

      if($(this).html() === "") {
      $(this).parent().parent().addClass("nothingThere");
      $(".nothingThere").tabs({active: 2});
      //$(".nothingThere ul li").find(".ui-tabs-active").removeClass("ui-tabs-active");
      //$(".nothingThere ul li").find(".ui-state-active").removeClass("ui-state-active");
      //$(".nothingThere ul li:last-child").addClass("ui-tabs-active").addClass("ui-state-active");

      }





      });




         $(".extra ul li").click(function() {

      if ($(this).hasClass("activeCol")) {

          $(".activeCol").find(".gear").show();
          $(".activeCol").find(".arrow").show();
          $(".activeCol").find(".buttons").hide();
          $(".activeCol").find(".attendees").hide();
          $(".activeCol").removeClass("activeCol")

      }else {
          $(".activeCol").find(".gear").show();
          $(".activeCol").find(".arrow").show();
          $(".activeCol").find(".buttons").hide();
          $(".activeCol").find(".attendees").hide();
          $(".activeCol").removeClass("activeCol")

          $(this).addClass("activeCol");
          $(".activeCol").find(".gear").hide();
          $(".activeCol").find(".arrow").hide();
          $(".activeCol").find(".buttons").fadeIn(700);
           $(".activeCol").find(".attendees").fadeIn(700).css("display", "block");
       }
          });



      $(".span").hide();
      $("span").on("click",  function(event) {

       if($(this).hasClass("current")){

       }else {


          var newCurrent =  $(this).attr("id")


        if (newCurrent === "english") {
        $(".labelUsernameEnglish").html("<label for="username">Username</label>");
        $(".labelPasswordEnglish").html("<label for="password">Password</label>")
       $(".forgetpass").html("<a href="forgot_password.php">Forgotten your username or password?</a>")
        $(".spanishLan").removeClass("spanishLan");
         $(".theLogin").html(" <input type="submit" id="loginbtn" value="Login &raquo;" />");

       $(".eng").show();
       $(".span").hide();
         $(".spanish").hide();
        $(".english").show();


        }else {

         $(".labelUsernameEnglish").html("<label for="username">Usuario</label>");
           $(".labelPasswordEnglish").html("<label for="password">Contraseña</label>");
           $(".forgetpass").html("<a href="forgot_password.php">¿Olvidó su usuario o contraseña?</a>");
           $(".languages").addClass("spanishLan");
           $(".theLogin").html(" <input type="submit" id="loginbtn" value="Entrar &raquo;" />");
           $(".span").show();
            $(".eng").hide();
        $(".spanish").show();
        $(".english").hide();


           }
         }
      });

      $(".showMore").click(function() {
      $(this).closest("div").find(".extra ul").children().removeClass("hidden");
      });




      //if ($(".ui-tabs .ui-tabs-nav li:last-child").hasClass("ui-state-active")) {
      //$(".showMore").hide();

      //} else {

      //}

      });
    </script>';
    return $html;
  }

public function trainings()
  {
    $html  = '';
    $trainings = get_courses(get_training_id());


    $html .= '

          <div>
            <h2>' . get_string('tab-trainings', 'theme_msu') . '</h2>
            <div class="line-right ax_horizontal_line"></div>';
/*
            <div id="tabs">
              <ul>
                 <li><a href="#tabs-1">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-2">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-3">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
              </ul>';
*/
    $html .= '
          <div id="tabs-1" class="extra">
            <ul>';

    foreach($trainings as $m)
    {
      $a = get_nstudents($m->id);
      $s = get_nseats($m->id);
      $so = $s - $a;

      $html .= '
              <li>
                <div class="tabInfo">
                  <img src="' . get_course_image_url($m->id). '" class="first-img" />
                  <div class="tabContent">
                    <a href="/course/view.php?id=' . $m->id .'"><h2>' . $m->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . ' at ' . gmdate('H:i', $m->startdate) . 'hrs</span>
                  </div><!--end tabContent --><br />
                  <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
                </div><!--end tabInfo -->
              </li>
            ';
    }

    $html .= '
          </div><!-- end tabs -->
        <div>
              ';

    $html .= '<script>
      $(document).ready(function() {

      var currentPage =  $("form").attr("id");

        var newHeight = $("div.left-column").outerHeight() + 40;
        var unit = "px";
        var cssHeight = newHeight + unit;
        $("div.right-column, iframe").css("min-height", cssHeight);


       if($( "iframe" ).contents().find( "pagelayout-frontpage")) {


      }else {

        alert("didnt work");
      }

      $( "#tabs").tabs();
      $("#tabs1").tabs({active: 10});
      $("#tabs2").tabs({active: 10});

      $("ul").each(function() {

      if($(this).html() === "") {
      $(this).parent().parent().addClass("nothingThere");
      $(".nothingThere").tabs({active: 2});
      //$(".nothingThere ul li").find(".ui-tabs-active").removeClass("ui-tabs-active");
      //$(".nothingThere ul li").find(".ui-state-active").removeClass("ui-state-active");
      //$(".nothingThere ul li:last-child").addClass("ui-tabs-active").addClass("ui-state-active");

      }





      });




         $(".extra ul li").click(function() {

      if ($(this).hasClass("activeCol")) {

          $(".activeCol").find(".gear").show();
          $(".activeCol").find(".arrow").show();
          $(".activeCol").find(".buttons").hide();
          $(".activeCol").find(".attendees").hide();
          $(".activeCol").removeClass("activeCol")

      }else {
          $(".activeCol").find(".gear").show();
          $(".activeCol").find(".arrow").show();
          $(".activeCol").find(".buttons").hide();
          $(".activeCol").find(".attendees").hide();
          $(".activeCol").removeClass("activeCol")

          $(this).addClass("activeCol");
          $(".activeCol").find(".gear").hide();
          $(".activeCol").find(".arrow").hide();
          $(".activeCol").find(".buttons").fadeIn(700);
           $(".activeCol").find(".attendees").fadeIn(700).css("display", "block");
       }
          });



      $(".span").hide();
      $("span").on("click",  function(event) {

       if($(this).hasClass("current")){

       }else {


          var newCurrent =  $(this).attr("id")


        if (newCurrent === "english") {
        $(".labelUsernameEnglish").html("<label for="username">Username</label>");
        $(".labelPasswordEnglish").html("<label for="password">Password</label>")
       $(".forgetpass").html("<a href="forgot_password.php">Forgotten your username or password?</a>")
        $(".spanishLan").removeClass("spanishLan");
         $(".theLogin").html(" <input type="submit" id="loginbtn" value="Login &raquo;" />");

       $(".eng").show();
       $(".span").hide();
         $(".spanish").hide();
        $(".english").show();


        }else {

         $(".labelUsernameEnglish").html("<label for="username">Usuario</label>");
           $(".labelPasswordEnglish").html("<label for="password">Contraseña</label>");
           $(".forgetpass").html("<a href="forgot_password.php">¿Olvidó su usuario o contraseña?</a>");
           $(".languages").addClass("spanishLan");
           $(".theLogin").html(" <input type="submit" id="loginbtn" value="Entrar &raquo;" />");
           $(".span").show();
            $(".eng").hide();
        $(".spanish").show();
        $(".english").hide();


           }
         }
      });

      $(".showMore").click(function() {
      $(this).closest("div").find(".extra ul").children().removeClass("hidden");
      });




      //if ($(".ui-tabs .ui-tabs-nav li:last-child").hasClass("ui-state-active")) {
      //$(".showMore").hide();

      //} else {

      //}

      });
    </script>';
    return $html;
  }

  public function meetings_old()
  {
    $hmtl = '';

    $html .= tabs_begin()
      .meetings()
      .trainings()
      .tabs_end()
      .scripts();

    return $html;
  }

  public function get_r($renderer_name)
  {
    global $DB, $USER;
    $html = '';

    $result = $DB->get_records('cbsi_renderer',array('name' => $renderer_name));
    foreach($result as $r)
    {
      if($r->name = $renderer_name)
      {
        $body = explode('.', $r->body);
        foreach($body as $b)
        {
          /*
          if(strpos($b , '$') != false)
          {
            $c = strstr($b, '$');           //get parameter of function
            $c = preg_replace ('/\)$/', '' , $c);   //strip closing parenthesis
            $html .= call_user_func($b, get_course_info($user->id));    //call function and append data
          }

          else
          {
            $html .= call_user_func($b);
          }
          */
          $html .= call_user_func($b);
        }

        return $html;
      }
    }
  }


public function home_2015_04_15() {
  global $USER,$CFG,$DB;

  /*
   * Note: shared courses are courses, as are trainings, so shared courses on trainings tab
   */

  $trainings = get_trainings();
  //start debug
  // echo "</br>" . (int)date('U') . "</br>";
  // foreach($trainings as $t) {
  //   echo "</br>" . (int)$t->startdate . ": " . $t->fullname . "</br>";
  //   //print_r($t);
  // }
  // die(__FILE__.":".__LINE__);
  //end debug
  $meetings  = get_meetings();
  $shared    = get_shared_courses();

  $html = '
    <!-- front page / home -->
    <script>
      $(function() {
        $( "#tabs, #tabs1, #tabs2, #tabs3, #tabs4, #tabs5" ).tabs();
      });
    </script>';

  $html .= slideshow().tabs_begin(get_string('section-upcoming', 'theme_msu'));
  $html .= '
    <div id="tabs-1" class="extra">
      <ul>';
        foreach($meetings as $m)
        {
          // print_r($m);
          // die(__FILE__.":".__LINE__);
          $a = get_nstudents($m->courseid);
          $s = get_nseats($m->courseid);
          $so = $s - $a;

          /*
           * Make sure startdate is set otherwise an error will be generated and break site
           */
          if(!isset($m->startdate))
          {
            $m->startdate = 0;
          }
      //if((int)$m->startdate >= (int)date('U')) {
        $html .= '
          <li>
            <div class="tabInfo">
              <img src="' . get_course_image_url($m->courseid) . '" class="first-img" />
              <div class="tabContent">
                <a href="/course/view.php?id=' . $m->courseid . '"><h2>' . $m->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . '&nbsp;at ' . gmdate('H:i', $m->startdate) . 'hrs</span>
              </div><!--end tabContent --><br />
              <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
            </div><!--end tabInfo -->
              <div class="tabAction">
                <div class="arrow">
                  <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                </div><!--end gear-->
                <div class="buttons tabButton">
                  <a href="/course/view.php?id=' . $m->courseid . '"><button>Start/Join</button></a>';
                  if (has_capability('moodle/course:update', context_course::instance($m->courseid)))
                  {
                    $html .= '<a href="/course/view.php?id=' . $m->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                    $html .= '<a href="/course/edit.php?id=' . $m->courseid . '"><button>Edit</button></a>';
                  }


        $html .=  '
              </div><!--end buttons-->
            </div><!--end tabAction -->
          </li>';
                  //}
      }

  $html .= '
            </ul>
          </div><!-- end tabs-1 -->';

  $html .= '
    <div id="tabs-2" class="extra">
      <ul>';

        foreach($trainings as $t) {
          $a = get_nstudents($t->courseid);
          $s = get_nseats($t->courseid);
          $so = $s - $a;

          // if(!isset($t->startdate))
          // {
          //   $t->startdate = 0;
          // }

          /*
           * if course startdate is prior today do not show it, it will be shown in completed section.
           */
          //if((int)$t->startdate >= (int)date('U')) {
            $html .= '
              <li>
                <div class="tabInfo">
                  <img src="' . get_course_image_url($t->courseid) . '" class="first-img" />
                  <div class="tabContent">
                    <a href="/course/view.php?id=' . $t->courseid . '"><h2>' . $t->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $t->startdate) . '&nbsp;at ' . gmdate('H:i', $t->startdate) . 'hrs</span>
                  </div><!--end tabContent --><br />
                  <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
                </div><!--end tabInfo -->
                  <div class="tabAction">
                    <div class="arrow">
                      <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                    </div><!--end gear-->
                    <div class="buttons tabButton">
                      <a href="/course/view.php?id=' . $t->courseid . '"><button>Start/Join</button></a>';
                      if (has_capability('moodle/course:update', context_course::instance($t->courseid)))
                      {
                        $html .= '<a href="/course/view.php?id=' . $t->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                        $html .= '<a href="/course/edit.php?id=' . $t->courseid . '"><button>Edit</button></a>';
                      }

            $html .=  '</div><!--end buttons-->
                </div><!--end tabAction -->
              </li>
            ';
          //}
        }

    $html .= '
              </ul>
            </div><!-- end tabs-2 -->';

    $html .= tabs_end();

    $html .= '

          <div>
            <h2>' . get_string('section-completed', 'theme_msu') . '</h2>
            <div class="line-right ax_horizontal_line"></div>
            <div id="tabs">
            <button class="showMore">'. get_string('show-more', 'theme_msu').'</button>
              <ul>
                 <li><a href="#tabs-4">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-5">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-6">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
              </ul>';


    $html .= '
      <div id="tabs-4" class="extra">
        <ul>';

    //$html .= completed_courses();
    foreach($meetings as $m) {
      $a = get_nstudents($m->courseid);
      $s = get_nseats($m->courseid);
      $so = $s - $a;

      if(!isset($m->startdate))
      {
        $m->startdate = 0;
      }

      /*
       * Completed meeting section
       * if meeting startdate is prior to today, put it here
       */
      if((int)$m->startdate < (int)date('U')) {
        $html .= '
          <li>
            <div class="tabInfo">
              <img src="' . get_course_image_url($m->courseid) . '" class="first-img" />
              <div class="tabContent">
                <a href="/course/view.php?id=' . $m->courseid . '"><h2>' . $m->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . '&nbsp;at ' . gmdate('H:i', $m->startdate) . 'hrs</span>
              </div><!--end tabContent --><br />
              <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
            </div><!--end tabInfo -->
              <div class="tabAction">
                <div class="arrow">
                  <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                </div><!--end gear-->
                <div class="buttons tabButton">
                  <a href="/course/view.php?id=' . $m->courseid . '"><button>Start/Join</button></a>';
                  if (has_capability('moodle/course:update', context_course::instance($m->courseid)))
                  {
                    $html .= '<a href="/course/view.php?id=' . $m->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                    $html .= '<a href="/course/edit.php?id=' . $m->courseid . '"><button>Edit</button></a>';
                  }

        $html .=  '</div><!--end buttons-->
            </div><!--end tabAction -->
          </li>
        ';
      }
    }

    $html .= '</ul></div><!-- tabs-4 -->';

    $html .= '
      <div id="tabs-5" class="extra">
        <ul>';

    //$html .= completed_courses();
    foreach($trainings as $t) {
      $a = get_nstudents($t->courseid);
      $s = get_nseats($t->courseid);
      $so = $s - $a;

      if(!isset($t->startdate))
      {
        $t->startdate = 0;
      }

      /*
       * Completed training section
       * if training is complete put it here
       * at this time completion is determined by whether the course start date has past
       */
      if((int)$t->startdate < (int)date('U')) {
        $html .= '
          <li>
            <div class="tabInfo">
              <img src="' . get_course_image_url($t->courseid) . '" class="first-img" />
              <div class="tabContent">
                <a href="/course/view.php?id=' . $t->courseid . '"><h2>' . $t->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $t->startdate) . '&nbsp;at ' . gmdate('H:i', $t->startdate) . 'hrs</span>
              </div><!--end tabContent --><br />
              <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
            </div><!--end tabInfo -->
              <div class="tabAction">
                <div class="arrow">
                  <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                </div><!--end gear-->
                <div class="buttons tabButton">
                  <a href="/course/view.php?id=' . $t->courseid . '"><button>Start/Join</button></a>';
                  if (has_capability('moodle/course:update', context_course::instance($t->courseid)))
                  {
                    $html .= '<a href="/course/view.php?id=' . $t->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                    $html .= '<a href="/course/edit.php?id=' . $t->courseid . '"><button>Edit</button></a>';
                  }

        $html .=  '</div><!--end buttons-->
            </div><!--end tabAction -->
          </li>
        ';
      }
    }

    $html .= '</ul></div><!-- tabs-5 -->';

    $html .= tabs_end()
      .scripts();

  return $html;
}

  public function profile()
  {
      global $USER,$CFG,$DB;

      $user = $USER;
      $course_info = get_course_info($user->id);

      $html = '
      <script>
        $(function() {
        $( "#tabs, #tabs1, #tabs2" ).tabs();

        });
      </script>';

      $html .= user_details($user)
          .tabs_begin(get_string('section-enrollment_list', 'theme_msu'))
          .meetings()
          .trainings()
          .tabs_end();

      $html .= '

          <div>
            <h2>' . get_string('section-completed', 'theme_msu') . '</h2>
            <div class="line-right ax_horizontal_line"></div>
            <div id="tabs">
            <button class="showMore">'. get_string('show-more', 'theme_msu').'</button>
              <ul>
                 <li><a href="#tabs-4">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-5">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-6">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
              </ul>';



    $html .= completed_courses();

    $html .= '</ul></div>';
    $html .= '</ul></div>';
    $html .= tabs_end()
        .badges()
        .scripts();

      return $html;
  }

  public function teacher_panel()
  {
      global $USER,$CFG,$DB;

      $user = $USER;
      $course_info = get_course_info($user->id);

      $hmtl = '
      <script>
        $(function() {
          $( "#tabs, #tabs1" ).tabs();
        });
      </script>';

      $html .= slideshow($course_info)
          .user_details($user)
          .teacher_info()
          .tabs_begin()
          .meetings($course_info)
          .trainings($course_info)
          .completed_courses($course_info)
          .tabs_end()
          .scripts();

      return $html;
  }

  public function user_training()
  {
   $html = tabs_begin()
          .meetings()
          .trainings()
          .tabs_end()
          .scripts();

    return $html;
  }

  public function home() {
      global $USER,$CFG,$DB;

      /*
       * Note: shared courses are courses, as are trainings,
       * so shared courses on trainings tab.
       */

      $trainings = get_trainings();
      $meetings  = get_meetings();
      $shared    = get_shared_courses_new();
      $trainings = $trainings + $shared;

      // //start debug
      // if(is_user_siteadmin()) {
      //   //echo "</br>" . (int)date('U') . "</br>";
      //   foreach($trainings as $t) {
      //     //echo "</br>" . (int)$t->startdate . ": " . $t->fullname . "</br>";
      //     echo $t->fullname . "<br/>";
      //   }
      //   die(__FILE__.":".__LINE__);
      // }
      // //end debug

      $html = '
        <!-- front page / home -->
        <script>
          $(function() {
            $( "#tabs, #tabs1, #tabs2, #tabs3, #tabs4, #tabs5" ).tabs();
          });
        </script>';

      $html .= slideshow_responsiveslides();
      $html .= tabs_begin(get_string('section-upcoming', 'theme_msu'));
      $html .= '
        <div id="tabs-1" class="extra">
          <ul>';
            foreach($meetings as $m)
            {
              $enddate = get_course_enddate($m->courseid);

              // print_r($enddate);
              // die(__FILE__.":".__LINE__);
              $a  = get_nstudents($m->courseid);
              $s  = get_nseats($m->courseid);
              $so = $s - $a;

              /*
               * Make sure startdate is set otherwise an error will be generated and break site
               */
              if(!isset($m->startdate))
              {
                $m->startdate = 1451638800;
              }
          if((int)$enddate >= (int)date('U') + 86400) {
            $html .= '
              <li>
                <div class="tabInfo">
                  <img src="' . get_course_image_url($m->courseid) . '" class="first-img" />
                  <div class="tabContent">
                    <a href="/course/view.php?id=' . $m->courseid . '"><h2>' . $m->fullname . '</h2></a><!--' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . '&nbsp;at ' . gmdate('H:i', $m->startdate) . 'hrs</span>-->
                  </div><!--end tabContent --><br />
                  <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
                </div><!--end tabInfo -->
                  <div class="tabAction">
                    <div class="arrow">
                      <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                    </div><!--end gear-->
                    <div class="buttons tabButton">
                      <a href="/course/view.php?id=' . $m->courseid . '"><button>Start/Join</button></a>';
                      if (has_capability('moodle/course:update', context_course::instance($m->courseid)))
                      {
                        $html .= '<a href="/course/view.php?id=' . $m->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                        $html .= '<a href="/course/edit.php?id=' . $m->courseid . '"><button>Edit</button></a>';
                      }


            $html .=  '
                  </div><!--end buttons-->
                </div><!--end tabAction -->
              </li>';
                      }
          }

      $html .= '
                </ul>
              </div><!-- end tabs-1 -->';

      $html .= '
        <div id="tabs-2" class="extra">
          <ul>';

            foreach($trainings as $t) {

              $enddate = get_course_enddate($t->courseid);

              if(is_user_siteadmin()) {
                // $today = ((int)date('U') - 86400 );
                // echo "Today :" . date('Y-m-d', $today) . "<br/>";
                // echo "End Date: " . date('Y-m-d', $enddate) . "<br/>";
                //echo "<!-- id: " . $t->courseid . " enddate: " . date('Y-m-d', $enddate) . " --><br/>";
                //die(__FILE__.":".__LINE__);
              }

              $a  = get_nstudents($t->courseid);
              $s  = get_nseats($t->courseid);
              $so = $s - $a;

              // if(!isset($t->startdate))
              // {
              //   $t->startdate = 0;
              // }

              /*
               * if course startdate is prior today do not show it, it will be shown in completed section.
               */
              //display if course start date is greater than or equal to today.
              if((int)$enddate >= ( (int)date('U') - 86400 )) {
                $html .= '
                  <li>
                    <div class="tabInfo">
                      <img src="' . get_course_image_url($t->courseid) . '" class="first-img" />
                      <div class="tabContent">
                        <a href="/course/view.php?id=' . $t->courseid . '"><h2>' . $t->fullname . '</h2></a><!--' . '<span class="course-date time">' . gmdate('m.d.y', $t->startdate) . '&nbsp;at ' . gmdate('H:i', $t->startdate) . 'hrs</span>-->
                      </div><!--end tabContent --><br />
                      <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
                    </div><!--end tabInfo -->
                      <div class="tabAction">
                        <div class="arrow">
                          <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                        </div><!--end gear-->
                        <div class="buttons tabButton">
                          <a href="/course/view.php?id=' . $t->courseid . '"><button>Start/Join</button></a>';
                          if (has_capability('moodle/course:update', context_course::instance($t->courseid)))
                          {
                            $html .= '<a href="/course/view.php?id=' . $t->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                            $html .= '<a href="/course/edit.php?id=' . $t->courseid . '"><button>Edit</button></a>';
                          }

                $html .=  '</div><!--end buttons-->
                    </div><!--end tabAction -->
                  </li>';
              }
            }

        $html .= '
                  </ul>
                </div><!-- end tabs-2 -->';

        $html .= tabs_end();

        $html .= '

              <div>
                <h2>' . get_string('section-completed', 'theme_msu') . '</h2>
                <div class="line-right ax_horizontal_line"></div>
                <div id="tabs">
                <button class="showMore">'. get_string('show-more', 'theme_msu').'</button>
                  <ul>
                     <li><a href="#tabs-4">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
                     <li><a href="#tabs-5">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
                     <li><a href="#tabs-6">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
                  </ul>';


        $html .= '
          <div id="tabs-4" class="extra">
            <ul>';

        //$html .= completed_courses();
        foreach($meetings as $m) {
          $enddate = get_course_enddate($m->courseid);
          $a  = get_nstudents($m->courseid);
          $s  = get_nseats($m->courseid);
          $so = $s - $a;

          if(!isset($m->startdate))
          {
            $m->startdate = 1451638800;
          }

          /*
           * Completed meeting section
           * if meeting startdate is prior to today, put it here
           */
          if((int)$enddate < (int)date('U')) {
            $html .= '
              <li>
                <div class="tabInfo">
                  <img src="' . get_course_image_url($m->courseid) . '" class="first-img" />
                  <div class="tabContent">
                    <a href="/course/view.php?id=' . $m->courseid . '"><h2>' . $m->fullname . '</h2></a>' . '<!--<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . '&nbsp;at ' . gmdate('H:i', $m->startdate) . 'hrs</span>-->
                  </div><!--end tabContent --><br />
                  <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
                </div><!--end tabInfo -->
                  <div class="tabAction">
                    <div class="arrow">
                      <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                    </div><!--end gear-->
                    <div class="buttons tabButton">
                      <a href="/course/view.php?id=' . $m->courseid . '"><button>Start/Join</button></a>';
                      if (has_capability('moodle/course:update', context_course::instance($m->courseid)))
                      {
                        $html .= '<a href="/course/view.php?id=' . $m->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                        $html .= '<a href="/course/edit.php?id=' . $m->courseid . '"><button>Edit</button></a>';
                      }

            $html .=  '</div><!--end buttons-->
                </div><!--end tabAction -->
              </li>
            ';
          }
        }

        $html .= '</ul></div><!-- tabs-4 -->';

        $html .= '
          <div id="tabs-5" class="extra">
            <ul>';

        //$html .= completed_courses();
        foreach($trainings as $t) {
          $enddate = get_course_enddate($t->courseid);
          $a  = get_nstudents($t->courseid);
          $s  = get_nseats($t->courseid);
          $so = $s - $a;

          if(!isset($t->startdate))
          {
            $t->startdate = 1451638800;
          }

          /*
           * Completed training section
           * if training is complete put it here
           * at this time completion is determined by whether the course start date has past
           */
          if((int)$enddate < (int)date('U')) {
            $html .= '
              <li>
                <div class="tabInfo">
                  <img src="' . get_course_image_url($t->courseid) . '" class="first-img" />
                  <div class="tabContent">
                    <a href="/course/view.php?id=' . $t->courseid . '"><h2>' . $t->fullname . '</h2></a>' . '<!--<span class="course-date time">' . gmdate('m.d.y', $t->startdate) . '&nbsp;at ' . gmdate('H:i', $t->startdate) . 'hrs</span>-->
                  </div><!--end tabContent --><br />
                  <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
                </div><!--end tabInfo -->
                  <div class="tabAction">
                    <div class="arrow">
                      <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                    </div><!--end gear-->
                    <div class="buttons tabButton">
                      <a href="/course/view.php?id=' . $t->courseid . '"><button>Start/Join</button></a>';
                      if (has_capability('moodle/course:update', context_course::instance($t->courseid)))
                      {
                        $html .= '<a href="/course/view.php?id=' . $t->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
                        $html .= '<a href="/course/edit.php?id=' . $t->courseid . '"><button>Edit</button></a>';
                      }

            $html .=  '</div><!--end buttons-->
                </div><!--end tabAction -->
              </li>
            ';
          }
        }

        $html .= '</ul></div><!-- tabs-5 -->';

        $html .= tabs_end();
        $html .= scripts();
        $html .= "<br/>";
        $html .= user_details();
        $html .= badges();
        $html .= "
          <script>
            $(document).ready(function(){
              $('.userprofile').click(function() {
                $('.profile-top').toggle();
                $('.editProfile').toggle();
              });

              $('#badge-header').click(function() {
                $('#badges-container').toggle();
              });
            });
          </script>";
        $html .= "
          <style>
            .badge-header {
              font-weight: 600;
              font-size: 23px;
              color: #446694;
              padding-bottom: 5px;
              font-size: 33px !important;
            }

            .cbsi-badge {
              clear: left;
              margin: 5px 5px 0 10px;
            }

            .cbsi-badge img {
              float: left;
              margin-bottom: 5px;
              margin-right: 5px;
            }

            .cbsi-badge-caption p {
              margin: 0 0;
              font-size: 14px;
              color: #20324c;
            }

            .profile-top {
              margin: 0px 0px 25px 0px;
              overflow: hidden;
              border-top: 1px dotted #293b53;
              padding-bottom: 20px;
              background: #fff;
              border-bottom: 1px solid #293b53;
              background: url(http://cbsi-connect.net/theme/msu/pix/stripebg.png) !important;
            }

            #badges-container {
              background: url(http://cbsi-connect.net/theme/msu/pix/stripebg.png) !important;
              height: 35%;
              padding-top: 10px;
            }

            #badges-container ul {
              /*background: url(http://cbsi-connect.net/theme/msu/pix/stripebg.png) !important;*/
            }

            .profileTitle h2 {}
          }

          </style>";

      return $html;
  }

  // public function test() {

  // }

  public function test() {

  }


  /**
   * @param course
   * -Course object returned from Moodle after a course is created
   * -Takes backup of folder resource and adds it to newly created course
   */
  public function test_restore_folder_activity() {
    if( has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
        global $CFG,$DB;
        //$course = get_course(908);

        //restore course from template
        $destdir = $CFG->dataroot.'/temp/backup/'.md5($course->shortname);
        $zipfile = $CFG->dataroot.'/course_templates/folder.mbz';

        if(file_exists($zipfile))
        {
            define('CLI_SCRIPT', true);
            require_once($CFG->dirroot.'/backup/util/includes/restore_includes.php');
            $packer = new zip_packer();
            $packer->extract_to_pathname($zipfile, $destdir);

            // Transaction
            $transaction = $DB->start_delegated_transaction();
            // Restore backup into course
            $controller = new restore_controller(
              md5($course->shortname),
              $course->id,
              backup::INTERACTIVE_NO,
              backup::MODE_SAMESITE,
              2,
              backup::TARGET_CURRENT_ADDING
            );

            $controller->execute_precheck();
            $controller->execute_plan();
            // Commit
            $transaction->allow_commit();
            define('CLI_SCRIPT', false);

            $mods = unserialize($course->modinfo);
            //here you can edit and resource or activity

            rebuild_course_cache($course->id, true);
        }
     } else {
        return "Need to be admin.";
     }
  }

  public function test_change_permissions() {
    global $CFG,$DB,$USER;
    $DB->set_debug(true);
    $html = "";
    /* role_assign($roleid, $userid, $contextid, $component = '', $itemid = 0, $timemodified = '')   X-Ref */

    //Get the user ids of all content contributors assigned at the system level - works
    //$sql_users = "SELECT userid FROM mdl_role_assignments WHERE contextid = 1 AND roleid = 9";
    $sql_users = "SELECT userid FROM mdl_role_assignments WHERE contextid = 1 AND roleid = 9";
    $users     = $DB->get_records_sql($sql_users);


    //get the context id for the user's parent category (academy)
    foreach($users as $user) {

      $u = core_user::get_user($user->userid);



      $cohort_id = get_cohort_id($u);
      print_r($cohort_id);
      die(__FILE__.":".__LINE__);


      $sql = 'SELECT contextid FROM {cohort} WHERE id = "'. $cohort_id . '"';
      $category = $DB->get_record_sql($sql);

      $categorycontext = context_coursecat::instance($category['id']);
      //role_assign(9, $user->id, get_parent_id());
      $html .= "User: {$user->id}, Category: {$category['id']}";
    }

    return $html;
  }

  public function test_ldap() {
    /*
    global $USER;
    $user = $USER;
    // config
    //$ldapserver = 'ldaps://cbsi-ad.cbsi-connect.net/';
    $ldapserver = 'ldaps://172.31.34.65';
    $ldapuser   = 'moodleadmin';
    $ldappass   = 'Hello123!';
    //$ldaptree   = "OU=Academies,DC=cbsi-AD,DC=cbsi-connect,DC=net";
    $ldaptree   = "ou=academies,dc=cbsi-ad,dc=cbsi-connect,dc=net";
    $ldapport   = 636;

    // connect
    $ldapconn = ldap_connect($ldapserver, $ldapport) or die("Could not connect to LDAP server.");
    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

    if($ldapconn) {
      // binding to ldap server
      $ldapbind = ldap_bind($ldapconn, $ldapuser, $ldappass);

      //$ldapbind = ldap_bind($ldapconn, $ldapuser, $ldappass) or die ("Error trying to bind: ".ldap_error($ldapconn));
      // verify binding
      if ($ldapbind) {
        //debug
        //echo "LDAP bind successful...<br /><br />";

        $result = ldap_search($ldapconn,$ldaptree, "(sAMAccountName=".$USER->username.")") or die ("Error in search query: ".ldap_error($ldapconn));
        $data = ldap_get_entries($ldapconn, $result);
        // print_r($data);
        // die(__FILE__.":".__LINE__);
        //print_r($data);die(__FILE__);
        if(!empty($data)) {
          return "Successfully connected to Active Directory";
        } else {
          //debug
          echo "LDAP bind failed: See your Moodle administrator";
          // print_r($data);
          // die(__FILE__.":".__LINE__);
        }
      }
    }
    */
    $html = '
      <h3>How to connect to site admin tool</h3>
      <ol>
        <li>Right-click <a href="/blocks/cbsi/cbsi.rdp">this link</a></li>
        <li>Select <strong>Save link as...</strong> to your desktop</li>
        <li>Go to your desktop and double-click on the cbsi.rdp icon. <img src="/blocks/cbsi/img/rdp_icon.png"/></li>
        <li>Enter your credentials</li>
        <li>blah blah blah</li>
      </ol>';

    return $html;
  }

  public function course_creation_wizard()
  {
    $html = '<script src="/theme/msu/javascript/custom.js"></script>';
    $html .= '<iframe src="http://cbsi-connect.net/blocks/cbsi/wizard/index.php"></iframe>';
    return $html;
  }

  public function test_wizard()
  {
    $html = '<script src="/theme/msu/javascript/custom.js"></script>';
    //$html .= '<iframe src="/local/metrostarwizard/index.php"></iframe>';
    $html .= '<iframe src="http://cbsi-connect.net/blocks/cbsi/wizard3/index.php"></iframe>';
    return $html;
  }

  public function enroll_users_block() {
    global $DB;
    //get cohort list as an array of cohort objects
    $academies = $DB->get_records_sql('SELECT id,name,idnumber from mdl_cohort order by name');

    //reset indexes for array for easier iteration
    $academies = array_values($academies);

    // print_r($academies);
    // die();

    //get all users in an academy (cohort)
    $sql_users = "SELECT *
        FROM {cohort_members}
        INNER JOIN {user}
        ON {cohort_members}.userid = {user}.id
        WHERE cohortid = ? ORDER BY lastname ASC";

    $filter = '
        <div class="container">
            <div id="filters">
                <span>Filter Users By Academy</span>
                <ul id="academy-names">';
                    foreach($academies as $a) {
                        $filter .= '
                            <li>
                                <input type="checkbox" value="' . $a->name . '" class="categoryFilter" name="categoryFilter">
                                <label for="">' . $a->name. ' </label>
                            </li>';
                    }

        $filter .= '
                </ul>
            </div>
            <div id="selected-users">
                <span>Selected Users</span><span style="float:right"><button id="enroll_users">Enroll Users</button></span>
                <ul id="user-names">
                </ul>
            </div><!-- end selected-users -->
            <button id="clear_users" style="float:right; margin-bottom: 10px;">Clear Users</button>
        </div><!-- end container -->';


    $table = '
        <table id="users" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Academy</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>';

    foreach($academies as $a) {
        $users = $DB->get_records_sql($sql_users, array($a->id));

        foreach($users as $user) {
            $table .= "
                <tr id='$user->id'>
                    <td>$a->name</td>
                    <td>$user->firstname</td>
                    <td>$user->lastname</td>
                    <td>$user->email</td>
                </tr>";
        }
    }

    $table .= "</tbody></table>";

    $html = $filter . $table;

    return $html;
  }

  public function enroll_users_iframe()
  {
    global $DB;

    //get course id
    //check if current user is course creator
    //if course creator, show add users
    //if not course creator, display unauthorized message


    $html = '<iframe src="/blocks/cbsi/users.php"></iframe>';
    //$html .= '<iframe src="http://cbsi-connect.net/blocks/cbsi/wizard/index.php"></iframe>';
    return $html;

  }

  public function helpdesk()
  {
    $html = '<iframe src="/helpdesk/index.php"></iframe>';
    return $html;
  }

  public function shared_courses()
  {
    global $USER;

    $html = '';
    //get shared courses, where 4 is the categoryid for shared courses
    $courses = get_shared_courses(4);

    $html .= '
    <div>
            <h2>Open Courses</h2>
            <p>Open Courses are available to all CBSI-Connect users.  Site Administrators in each academy can place a course in the Open Courses category to share content with all other participating academies.</p>
            <!--
            <div class="line-right ax_horizontal_line"></div>
            <div id="">
            <button class="showMore">'. get_string('show-more', 'theme_msu').'</button>
              <ul>
                 <li><a href="#tabs-1">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-2">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-3">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
              </ul>
              -->';

    $html .= '
          <div id="tabs-1" class="extra">
            <ul>';

                foreach($courses as $m)
                {
                      $a = get_nstudents($m->id);
                      $s = get_nseats($m->id);
                      $so = $s - $a;

                      $html .= '
                        <li>
                        <div class="tabInfo">
                        <img src="'. get_course_image_url($m->id) .'" class="first-img" />
                        <div class="tabContent">
                        <a href="/course/view.php?id=' . $m->id . '"><h2>' . $m->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . '&nbsp;at ' . gmdate('H:i', $m->startdate) . 'hrs</span></div><!--end tabContent --><br />
                        <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>   </div><!--end tabInfo -->
                        <div class="tabAction">
                        <div class="arrow">
                        <!-- <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />--></div><!--end gear-->
                        <div class="buttons tabButton">
                        <a href="http://cbsi-connect.net/course/view.php?id=' . $m->id .'"><button>Start/Join</button></a>';

                        if (has_capability('moodle/course:update', context_course::instance($m->id)))
                        {
                          $html .= '<a href="/course/view.php?id=' . $m->id . '&sesskey=' . $USER->sesskey . '&edit=on"><button>Manage</button></a>';
                          $html .= '<a href="/course/edit.php?id=' . $m->id . '"><button>Edit</button></a>';
                        }

    $html .=            '</div><!--end buttons--></div><!--end tabAction -->
                        </li>
                      ';
                }

                $html .= '
             </ul>
          </div><!-- end tabs-1 -->';

      return $html;
  }
}

