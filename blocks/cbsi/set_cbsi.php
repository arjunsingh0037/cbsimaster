<?php
require_once('../../config.php');

global $DB, $PAGE,$CFG;
require_once(__DIR__.'/set_cbsi_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title("Set | CBSI");
$PAGE->set_heading("CBSI Course Settings");
$PAGE->set_url($CFG->wwwroot . '/blocks/cbsi/set_cbsi.php');
$PAGE->navbar->add(get_string('sidebar-manage-courses', 'theme_msu'), new moodle_url($CFG->wwwroot . '/blocks/cbsi/manage_content.php'))  ;

$courseid = required_param('courseid', PARAM_INT); // course id
$sql        = "SELECT * FROM mdl_block_cbsi WHERE courseid = {$courseid}";
$record     = $DB->get_record_sql($sql);
// print_r($data);
// die();

echo $OUTPUT->header();
require_login();

//Instantiate simplehtml_form
$mform = new set_cbsi_form(null, (array)$record);

//Form processing and displaying is done here
if ($mform->is_cancelled()) {
    //Handle form cancel operation, if cancel button is present on form
    redirect($PAGE->url);
} else if ($fromform = $mform->get_data()) {
    //In this case you process validated data. $mform->get_data() returns data posted in form.
    // print_r($fromform);
    // die();
    $record->end_datetime = $fromform->end_datetime;
    $DB->update_record('block_cbsi', $record);
    redirect($PAGE->url . "?courseid=" . $courseid);
} else {
  // this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
  // or on the first display of the form.

  //Set default data (if any)

  $mform->set_data($record);
  //displays the form
  $mform->display();
}
