<?php

/**
 * @param $user id
 * -id of user to use to query the database
 * @return $course_info
 * -array of user and associated course information
 */
function get_training_info($user_id) {
  global $DB;

  // join to get relevant user course data
    $course_info = $DB->get_records_sql('
        SELECT
         {course_completions}.id,
         {course_completions}.userid,
         {course_completions}.course,
         {course_completions}.timeenrolled,
         {course_completions}.startdateed,
         {course_completions}.timecompleted,
         {user_enrolments}.status,
         {user_enrolments}.startdate,
         {user_enrolments}.timeend,
         {course}.category,
         {course}.fullname,
         {course}.shortname,
         {course}.summary
        FROM {course_completions}
         INNER JOIN {course} ON {course_completions}.course = {course}.id
         INNER JOIN {user_enrolments} ON {course_completions}.userid = {user_enrolments}.userid
        WHERE {course_completions}.userid = ?',
        array($user_id)
    );
    return $course_info;
 }

 /**
 * @param $user id
 * -id of user to use to query the database
 * @return $course_info
 * -array of user and associated course information
 */
function get_meeting_info($user_id) {
  global $DB;

  // join to get relevant user course data
    $course_info = $DB->get_records_sql('
        SELECT
         {course_completions}.id,
         {course_completions}.userid,
         {course_completions}.course,
         {course_completions}.timeenrolled,
         {course_completions}.startdateed,
         {course_completions}.timecompleted,
         {user_enrolments}.status,
         {user_enrolments}.startdate,
         {user_enrolments}.timeend,
         {course}.category,
         {course}.fullname,
         {course}.shortname,
         {course}.summary
        FROM {course_completions}
         INNER JOIN {course} ON {course_completions}.course = {course}.id
         INNER JOIN {user_enrolments} ON {course_completions}.userid = {user_enrolments}.userid
        WHERE {course_completions}.userid = ?',
        array($user_id)
    );

    return $course_info;
 }

/**
 * @param $user id
 * -id of user to use to query the database
 * @return $course_info
 * -array of user and associated course information
 */
function get_course_info($user_id) {
	global $DB;

  // join to get relevant user course data
    $course_info = $DB->get_records_sql('
        SELECT
         {course_completions}.id,
         {course_completions}.userid,
         {course_completions}.course,
         {course_completions}.timeenrolled,
         {course_completions}.timestarted,
         {course_completions}.timecompleted,
         {user_enrolments}.status,
         {user_enrolments}.timestart,
         {user_enrolments}.timeend,
         {course}.category,
         {course}.fullname,
         {course}.shortname,
         {course}.summary
        FROM {course_completions}
         INNER JOIN {course} ON {course_completions}.course = {course}.id
         INNER JOIN {user_enrolments} ON {course_completions}.userid = {user_enrolments}.userid
        WHERE {course_completions}.userid = ?',
        array($user_id)
    );

    return $course_info;
 }

/**
 * @param $user id id of user to use to query the database
 * @return $course_info array of user and associated course information
 */
function get_completed_courses() {
  global $DB,$USER;

  // join to get relevant user course data
    $course_info = $DB->get_records_sql('
        SELECT
         {course_completions}.id,
         {course_completions}.userid,
         {course_completions}.course,
         {course_completions}.timeenrolled,
         {course_completions}.timestarted,
         {course_completions}.timecompleted,
         {user_enrolments}.status,
         {user_enrolments}.startdate,
         {user_enrolments}.timeend,
         {course}.category,
         {course}.fullname,
         {course}.shortname,
         {course}.summary
        FROM {course_completions}
         INNER JOIN {course} ON {course_completions}.course = {course}.id
         INNER JOIN {user_enrolments} ON {course_completions}.userid = {user_enrolments}.userid
        WHERE {course_completions}.userid = ?',
        array($USER->id)
    );

    return $course_info;
 }

/**
 * @param $user_id id of user
 * @return string link to user picture
 */
 function get_user_picture()
 {
 	global $USER, $PAGE;
	$user_picture = new user_picture($USER);
	$src = $user_picture->get_url($PAGE);

	//return $OUTPUT->user_picture($userpic, array('class'=> 'circular', 'size'=>100));
	return $src;
 }

/**
 * Render slideshow
 * @return string html
 */
 function slideshow()
{
  global $DB;
  $shared_courses_sql = "SELECT *
    FROM mdl_course
    WHERE category = 4
    ORDER BY startdate
    LIMIT 4";

  $shared_courses = $DB->get_records_sql($shared_courses_sql);
  //$shared_courses = $DB->execute($sql,null);

  //get shared courses, where 4 is the categoryid for shared courses
  //$courses = get_shared_courses(4);

  //$courses = get_courses_limit(4);
  $slide_number = 0;
  $class = "";
  $slideshow = '';

  // if( has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
  //   // echo "\n{$courses[0]->id}\n";
  //   for($i = 0;$i < 4;$i++) {
  //     echo "\n{$courses[$i]->id}\n";
  //   }
  // }

  $slideshow .= '<!-- slideshow() -->
    <div id="s6">';

              foreach($shared_courses as $c)
              {
                $slideshow .= '
                  <div class="slides">
                    <div class="slide-caption">
                        <a href="/course/view.php?id=' . $c->id . '"><h2>' . $c->fullname . '</h2></a>
                        <p>' . $c->summary . '</p>
                    </div>
                    <div class="slide-image">
                        <a href="/course/view.php?id=' . $c->id . '"><img src="' . get_course_image_url($c->id) . '" width="240" height="180"></a>
                    </div>
                  </div>';
              }

  $slideshow .= '
    </div>
    <div style="clear:both"></div>
    <div class="controlWrap">
      <span class="slideshow-view-all"><a href="/?name=open_courses">Show all available courses</a></span>
      <div style="margin-top: -25px" id="control"></div>
    </div>';

  //$slideshow .= '<script src="http://cbsi-connect.net/theme/msu/javascript/jquery.cycle.all.js"></script>';
  //$slideshow .= '<script src="http://cbsi-connect.net/theme/msu/javascript/jquery.cycle2.min.js"></script>';


  return $slideshow;
}

/**
 * Render slideshow based on responsive slides
 * @return string html
 */
function slideshow_responsiveslides() {
  global $DB;

  $sql     = "SELECT id, fullname, summary FROM {course} WHERE category = 4 ORDER BY startdate LIMIT 4";
  $courses = $DB->get_records_sql($sql);

  $html    = '<script src="/blocks/cbsi/jquery/responsiveslides.min.js"></script>';
  $html .= "
    <style>
      .slides {
        width: 720px;
        margin: 0 auto;
        height: 200px;
        border-radius: 8px;
        -webkit-border-radius: 8px;
        background: #fff !important;
      }

      #control .active-control {
          color: white;
          background-color: #4f617a;
      }
    </style>";

  $html .= '
    <script>
      $(function() {

        function get_page() {
          var p = $(".rslides1_on").attr("id");

          switch(p) {
            case "rslides1_s0":
              page = 1;
              break;
            case "rslides1_s1":
              page = 2;
              break;
            case "rslides1_s2":
              page = 3;
              break;
            case "rslides1_s3":
              page = 4;
              break;
            default:
              page = 1;
          }

          return page;
        }

        function set_page(page) {
          $("#slide-" + page).addClass("active-control");
        }


        $("#slide-4").on("click", function() {
          $(get_page()).removeClass("rslides1_on");
          $(".rslides1_s3").addClass("rslides1_on");
        });

        $(".rslides").responsiveSlides({
            auto: true,             // Boolean: Animate automatically, true or false
            speed: 500,            // Integer: Speed of the transition, in milliseconds
            timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
            pager: true,           // Boolean: Show pager, true or false
            nav: false,             // Boolean: Show navigation, true or false
            random: false,          // Boolean: Randomize the order of the slides, true or false
            pause: false,           // Boolean: Pause on hover, true or false
            pauseControls: true,    // Boolean: Pause when hovering controls, true or false
            prevText: "Previous",   // String: Text for the "previous" button
            nextText: "Next",       // String: Text for the "next" button
            maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
            navContainer: "",       // Selector: Where controls should be appended to, default is after the "ul"
            manualControls: "#pager",     // Selector: Declare custom pager navigation
            namespace: "rslides",   // String: Change the default namespace used
            before: function(){
              var page = get_page();

              $("#slide-" + page).removeClass("active-control");
            },   // Function: Before callback
            after: function(){
              var page = get_page();

              $("#slide-" + page).addClass("active-control");
            }     // Function: After callback
        });
      });
    </script>';

  $html .= '
        <ul class="rslides">';
        foreach($courses as $c) {
          $html .= '
            <div class="slides">
              <div class="slide-caption">
                  <a href="/course/view.php?id=' . $c->id . '"><h2>' . $c->fullname . '</h2></a>
                  <p>' . $c->summary . '</p>
              </div>
              <div class="slide-image">
                  <a href="/course/view.php?id=' . $c->id . '"><img src="' . get_course_image_url($c->id) . '" width="240" height="180"></a>
              </div>
            </div>';
        }

  $html .= '
       </ul>
       <div style="clear:both"></div>
       <div id="pager" class="controlWrap">
        <span style="
        display: inline; padding-top: 7px; margin-left: 10px; color: #4f617a;position: relative;top: 6px"><a style="color: #4f617a;" href="/blocks/cbsi/open_courses.php">'. get_string("show-all-courses", "theme_msu").'</a></span>
        <div style="margin-top: -25px;position: relative;top: 5px" id="control">';

        for($i = 0; $i <= count($courses) - 1;$i++) {
          $index = $i + 1;
          $class = "";
          if($index == 1) { $class = 'class="active-control"';}
          $html .= '<a ' . $class . ' id="slide-' . $index . '" href="#">'. $index .'</a>';
        }
  $html .= '
        </div>
      </div>';

  return $html;
}

/**
 * Render slideshow basedon YUI3
 * @deprecated Yahoo stopped development on YUI3
 * @return string html
 */
 function slideshow_yui($course_info = null)
 {

    $html = '';
    $courses = get_shared_courses(4);
    $slide_number = 1;

    //set active slide
    $html .= '
    <div id="slideshow">
        <ul class="slides">';

        foreach($courses as $c)
        {
          $html .= '<li ';

          if($slide_number == 1)
          {
            $html .= 'class="active"';
          }

          $html .= '>
              <div class="slide-caption">
                <a href="/course/view.php?id=' . $c->id . '">' . $c->fullname . '</a>'
                . $c->summary . '
                <!--<button class="btn btn-primary">Read More</button>-->
              </div><!-- end slide-caption -->

              <div class="slide-image">
                <a href="/course/view.php?id=' . $c->id . '"><img src="' . get_course_image_url($c->id) .'"></a>
              </div><!-- end slide-image -->
            </li>';
            $slide_number++;
        }

    $html .= '</ul>
        <ul class="controls">';

      $slide_number = 1;
      foreach($courses as $k => $v)
      {
         $html .= '<li ';

         if($slide_number == 1)
          {
            $html .= 'class="active"';
          }

          $html .= '><a href="#slide-' . $slide_number . '">' . $slide_number . '</a></li>';
         $slide_number++;
      }

    $html .= '
        </ul>
    </div><!-- end slideshow -->';

    return $html;
 }

/**
 * Render slideshow basedon YUI3
 * @deprecated Yahoo stopped development on YUI3
 * @return string html
 */
function slideshow_yui_new() {
  // ini_set ('display_errors', 'on');
  // ini_set ('log_errors', 'on');
  // ini_set ('display_startup_errors', 'on');
  // ini_set ('error_reporting', E_ALL);
  global $DB;

  $shared_courses_sql = "SELECT *
    FROM mdl_course
    WHERE category = 4
    ORDER BY startdate
    LIMIT 4";

  $shared_courses = $DB->get_records_sql($shared_courses_sql);
  $shared_courses = array_values($shared_courses);
  // print_r($shared_courses);
  // die(__FILE__.":".__LINE__);

  $slideshow = '';
  $slideshow .= '
    <div id="slideshow" class="slides">
      <ul class="slides">';

        $i = 1;

        foreach($shared_courses as $s) {

          $class = "";

          if($s->id == $shared_courses[0]->id) {
            $class = 'active';
          }

          $image = get_course_image_url($s->id);

          $slideshow .= "
            <li class=\"slide-image $class\">
              <img src=\"$image\" width=\"240\" height=\"180\" \>
            </li>";
          $i++;
        }

        $i = 1;
        $slideshow .= '
        <div style="clear:both"></div>
        <ul class="controls controlWrap">';
          foreach($shared_courses as $s) {

            if($s->id == $shared_courses[0]->id) {
              $class = 'active';
            }

            $slideshow .= '<li class="'.$class.'"><a href="#slide-'.$i.'">'. $i .'</a></li>';
            $i++;
          }
        $slideshow .= '</ul><div class="paused">paused...</div>';

  $slideshow .= "
      </ul><!-- slides -->
    </div><!-- slideshow -->";

  $slideshow .= "
    <script>
      YUI().use('node', 'paginator',  'event-hover', 'gallery-timer', function (Y) {
          var slideshow= Y.one('#slideshow'),
              slides = slideshow.all('.slides li'),
              controls = slideshow.all('.controls li'),
              selectedClass = 'active',
              pg = new Y.Paginator({
                  itemsPerPage: 1,
                  totalItems: slides.size()
              });


          pg.after('pageChange', function (e) {
              var page = e.newVal;

              // decrement page since nodeLists are 0 based and
              // paginator is 1 based
              page--;

              // clear anything active
              slides.removeClass(selectedClass);
              controls.removeClass(selectedClass);

              // make the new item active
              slides.item(page).addClass(selectedClass);
              controls.item(page).addClass(selectedClass);
          });



          // when we click the control links we want to go to that slide
          slideshow.delegate('click', function (e) {
              e.preventDefault();
              var control = e.currentTarget;

              // if it's already active, do nothing
              if (control.ancestor('li').hasClass(selectedClass)) {
                  return;
              }

              pg.set('page', parseInt(control.getHTML(), 10));
          }, '.controls a');



          // create a timer to go to the next slide automatically
          // we use timer to obtain a pause/resume relationship
          autoPlay = new Y.Timer({
              length: 2500,
              repeatCount: 0});

          autoPlay.after('timer', function (e) {
              if (pg.hasNextPage()) {
                  pg.nextPage();
              } else {
                  pg.set('page', 1);
              }
          });

          // start the autoPlay timer
          autoPlay.start();




          // we want to pause when we mouse over the slide show
          // and resume when we mouse out
          slideshow.on('hover', function (e) {
              autoPlay.pause()
          }, function (e) {
              autoPlay.resume()
          });


      });
    </script>";

    return $slideshow;
}

/**
 * Renders a list of the current users meetings
 * @return string html
 */
function meetings()
{
    global $USER;
    $meetings = get_courses(get_meeting_id());
    $html = '';

    $html .= '
          <div id="tabs-1" class="extra">
            <ul>';

                foreach($meetings as $m)
                {
                      $a = get_nstudents($m->id);
                      $s = get_nseats($m->id);
                      $so = $s - $a;

                      $html .= '
                        <li>
                        <div class="tabInfo">
                        <img src="'. get_course_image_url($m->id) .'" class="first-img" />
                        <div class="tabContent">
                        <h2>' . $m->fullname . '</h2>' . '<span class="course-date time">' . gmdate('m.d.y', $m->startdate) . '&nbsp;at ' . gmdate('H:i', $m->startdate) . 'hrs</span></div><!--end tabContent --><br />
                        <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>   </div><!--end tabInfo -->
                        <div class="tabAction">
                        <div class="arrow">
                        <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                        <div class="buttons tabButton">
                        <a href="http://cbsi-connect.net/course/view.php?id=' . $m->id .'"><button>Start/Join</button></a>';

                        if (has_capability('moodle/course:update', context_course::instance($m->id)))
                        {
                          $html .= '<a href="/course/view.php?id=' . $m->id . '&sesskey=' . $USER->sesskey . '&edit=on"><button>Manage</button></a>';
                          $html .= '<a href="/course/edit.php?id=' . $m->id . '"><button>Edit</button></a>';
                        }

    $html .=            '</div><!--end buttons--></div><!--end tabAction -->
                        </li>
                      ';
                }

                $html .= '
             </ul>
          </div><!-- end tabs-1 -->';

    return $html;
}

/**
 * Renders a list of the current users trainings
 * @return string html
 */
function trainings()
{
    global $USER;
    $html = '';
    $trainings = get_courses(get_training_id());

    $html .= '
        <div id="tabs-2" class="extra">
          <ul>';

              foreach($trainings as $t)
              {
                    $a = get_nstudents($t->id);
                    $s = get_nseats($t->id);
                    $so = $s - $a;

                    $html .= '
                      <li>
                      <div class="tabInfo">
                      <img src="'.get_course_image_url($t->id).'" class="first-img" />
                      <div class="tabContent">
                      <h2>' . $t->fullname . '</h2>' . '<span class="course-date time">' . gmdate('m.d.y', $t->startdate) . ' at ' . gmdate('H:i', $t->startdate) . 'hrs</span></div><!--end tabContent --><br />
                      <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>   </div><!--end tabInfo -->
                      <div class="tabAction">
                      <div class="arrow">
                      <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                      <div class="buttons tabButton">
                      <a href="/course/view.php?id=' . $t->id.'"><button>Start/Join</button></a>';

                      if (has_capability('moodle/course:update', context_course::instance($t->id)))
                        {
                          $html .= '<a href="/course/view.php?id=' . $t->id . '&sesskey=' . $USER->sesskey . '&edit=on"><button>Manage</button></a>';
                          $html .= '<a href="/course/edit.php?id=' . $t->id . '"><button>Edit</button></a>';
                        }

    $html .=          '</div><!--end buttons--></div><!--end tabAction -->
                      </li>
                    ';
              }
              $html .= '
           </ul>
        </div><!-- end tabs-2 -->';

        return $html;
}

/**
 * Renders scripts needed for styling and javascript functionality
 * @return string html
 */
function scripts()
{
    $html = '
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
      <script src="http://cbsi-connect.net/theme/msu/javascript/custom.js"></script>
      <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css" type="text/css" media="all" />

      <!-- Latest compiled and minified CSS -->
			<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> commnted Mihir metro30 1st april -->

			<!-- Optional theme -->
			<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css"> commnted Mihir metro30 1st april -->

			<!-- Latest compiled and minified JavaScript -->
			<!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> -->
      <script src="/blocks/cbsi/js/cbsi.js"></script>';

    return $html;
}

/**
 * Renders and basic user profile for the current user
 * @return string html
 */
function user_details()
{
  global $USER;
  $user = $USER;
  $html = '';
    $html .= '
      <div id="userprofile" class="userprofile">
        <div class="profileTitle">
          <h2>' . get_string('sidebar-profile', 'theme_msu') . '</h2>
          <div class="editProfile"><a href="/user/editadvanced.php?id=' . $user->id . '"><span class="profile-edit">' . get_string("button-edit_profile", "theme_msu") . '</span></a> </div>
        </div>
        <div class="line-right ax_horizontal_line"></div>
      <div class="profile-top">
        <a href="#"><img src="' . get_user_picture() . '" width="100" height="100" class="defaultuserpic circular"></a>
        <div class="profile-main-info">
          <div class="profile-fullname">' . $user->firstname . '&nbsp;' . $user->lastname . '</div>
          <div class="profile-title"><!-- Company Title (not defined in profile) --></div>
          <div class="profile-joindate">Joined <span class="profile-joindate">' . format_time(time() - $user->firstaccess) . '</span> ago</div>
          <div class="profile-lastlogin">Last Login: <span class="profile-lastlogin">' . gmdate("M d, Y", $user->lastaccess) . '</span></div>
        </div><!-- end profile-main-info-->
      </div><!-- end profile-top -->
    </div><!-- end userprofile -->';

  return $html;
}

/**
 * @deprecated Due to time constraints, I don't think this panel was ever used
 * @return [type] [description]
 */
 function quick_panel() {

  $html = '  <div class="quick_panel">
       <h2>Teachers Panel: Quick Info & Tasks</h2>
       <ul>
       <li>
       <div class="quickInfo meeting">
       <h3>' . get_string('sidebar-meetings', 'theme_msu') . '</h3>
       <p>' . get_string('button-logout', 'theme_msu') . ' ' . get_string('sidebar-meetings', 'theme_msu') . ': 1</p>
       <p>Total Attendees: 32</p>
       </div>
        <button class="profile-edit">New Meeting</button>
       </li>
       <li>
       <div class="quickInfo training">
       <h3>' . get_string('tab-trainings', 'theme_msu') . '</h3>
       <p>' . get_string('button-logout', 'theme_msu') . ' ' . get_string('sidebar-meetings', 'theme_msu') . ': 1</p>
       <p>Total Attendees: 32</p>
       <p>Students Awaiting Your Approval: 3</p>
       </div>
        <button class="profile-edit">New Training</button>
        </li>
       </ul>
       </div>
          <div style="clear:both"></div>';
    return $html;
}

/**
 * Render the current user's upcoming meetings and trainings
 * @deprecated This functionality is built into the stand-alone homepage
 * @param  object $course_info
 * @return string html
 */
function upcoming()
{
    $meetings = get_courses(get_meeting_id());
    $trainings = get_courses(get_training_id());

   $html = '<!-- upcoming() -->';

          foreach($meetings as $meeting)
              {
                $html .= '
                  <li>
                  <div class="tabInfo">
                  <img src="'.get_course_image_url($m->id).'" class="first-img" />
                  <div class="tabContent">
                  <h2>' . $meeting->shortname . '</h2>' . '<span class="course-date time">' . gmdate('m.d.y', $meeting->startdate) . '&nbsp;at&nbsp; ' . gmdate('G:i', $meeting->startdate) . ' </span></div><!--end tabContent --><br />
                  <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
                  <div class="tabAction">
                  <div class="arrow">
                  <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                  <div class="buttons tabButton">
                  <button>Start/Join</button>
                  <button>Manage</button>
                  <button>Edit</button>
                  </div><!--end buttons--></div><!--end tabAction -->
                  </li>
                  </ul>

               ';
              }


        foreach($trainings as $training)
        {
          $image_name = strtolower($training->shortname);
          $image_name = preg_replace('@[ ]@', '_', $image_name);

          $image_url = "$CFG->wwwroot/pluginfile.php/305/course/overviewfiles/$image_name";
          $html .= '
            <li>
            <div class="tabInfo">
            <img src="' . $image_url .'" class="first-img" />
            <div class="tabContent">
            <h2>' . $training->shortname . '</h2>' . '<span class="course-date time">' . gmdate('m.d.y', $training->startdate) . '&nbsp;at&nbsp; ' . gmdate('G:i', $training->startdate) . ' </span></div><!--end tabContent --><br />
            <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
            <div class="tabAction">
            <div class="arrow">
            <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
            <div class="buttons tabButton">
            <button>Start/Join</button>
            <button>Manage</button>
            <button>Edit</button>
            </div><!--end buttons--></div><!--end tabAction -->
            </li>
            </ul>
         ';
        }


    return $html;
}

/**
 * Render the current user's trainings
 * @deprecated This functionality is built into the stand-alone homepage
 * @param  object $course_info
 * @return  string html
 */
function training($course_info)
{
   $html = '

        <div class="training">
          <h2>' . get_string('tab-trainings', 'theme_msu') . '</h2><span class="upcomingTrainings">32 Training Courses Avaiable </span>
          <div class="line-right ax_horizontal_line"></div>
          <div id="tabs1">
          <button class="showMore">'. get_string('show-more', 'theme_msu').'</button>
            <ul>
               <li><a href="#tabs1-1">In Progress</a></li>
               <li><a href="#tabs1-2">' . get_string('button-logout', 'theme_msu') . '</a></li>
               <li><a href="#tabs1-3">View All</a></li>
              <li><a href="#tabs1-4">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
            </ul>
            <div id="tabs1-1" class="extra">
          <ul>';

          foreach($course_info as $ci)
              {
                  if($ci->timecompleted == "")
                  {
                      $html .= '<li>
                                <div class="tabInfo">
                                <img src="'.get_course_image_url($ci->id).'" class="first-img" />
                                <div class="tabContent">
                                <a href="/course/view.php?id=' . $ci->id . '<h2>' . $ci->shortname . '</h2></a>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . '&nbsp;at&nbsp; ' .gmdate('G:i', $ci->startdate) . ' </span></div><!--end tabContent --><br />
                                <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
                                <div class="tabAction">
                                <div class="arrow">
                                <img src="http://iomad.metrostarsystems.com/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                                <div class="buttons tabButton">
                                <a href="/course/view.php?id=' . $ci->id . '<button>Start/Join</button></a>
                                <button>Manage</button>
                                <button>Edit</button>
                                </div><!--end buttons--></div><!--end tabAction -->
                                </li>

                             ';
                  }
              }

              $html .= '
           </ul>
        </div><!-- end tabs-1 -->
        <div id="tabs1-2" class="extra">
         <ul>';

         foreach($course_info as $ci)
              {
                  if($ci->timecompleted == "")
                  {
                      $html .= '<li>
                                <div class="tabInfo">
                                <img src="'.get_course_image_url($ci->id).'" class="first-img" />
                                <div class="tabContent">
                                <a href="/course/view.php?id=' . $ci->id .'"><h2>' . $ci->shortname . '</h2></a>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . '&nbsp;at&nbsp; ' .gmdate('G:i', $ci->startdate) . ' </span></div><!--end tabContent --><br />
                                <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
                                <div class="tabAction">
                                <div class="arrow">
                                <img src="http://iomad.metrostarsystems.com/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                                <div class="buttons tabButton">
                                <a href="/course/view.php?id=' . $ci->id . '<button>Start/Join</button></a>
                                <button>Manage</button>
                                <button>Edit</button>
                                </div><!--end buttons--></div><!--end tabAction -->
                                </li>

                             ';
                  }
              }

               $html .= '
           </ul>
        </div><!-- end tabs1-2 -->
        <div id="tabs1-3" class="extra">
         <ul>';

          foreach($course_info as $ci)
              {
                  if($ci->timecompleted == "")
                  {
                      $html .= '<li>
                                <div class="tabInfo">
                                <img src="'.get_course_image_url($ci->id).'" class="first-img" />
                                <div class="tabContent">
                                <h2>' . $ci->shortname . '</h2>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . '&nbsp;at&nbsp; ' .gmdate('G:i', $ci->startdate) . ' </span></div><!--end tabContent --><br />
                                <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
                                <div class="tabAction">
                                <div class="arrow">
                                <img src="http://iomad.metrostarsystems.com/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                                <div class="buttons tabButton">
                                <button>Start/Join</button>
                                <button>Manage</button>
                                <button>Edit</button>
                                </div><!--end buttons--></div><!--end tabAction -->
                                </li>

                             ';
                  }
              }

               $html .= '
           </ul>
        </div><!-- end tabs1-3 -->
        </div></div>';


    return $html;
}


/**
 * Render the current user's meeting
 * @deprecated This functionality is built into the stand-alone homepage
 * @param  object $course_info
 * @return [type]              [description]
 */
function your_meetings($course_info)
{
   $html = '

        <div class="yourMeeting">
          <h2>Your ' . get_string('sidebar-meetings', 'theme_msu') . '</h2><span class="yourupcomingMeetings">42 ' . get_string('sidebar-meetings', 'theme_msu') . '</span>
          <div class="line-right ax_horizontal_line"></div>
          <div id="tabs2">
          <button class="showMore">'. get_string('show-more', 'theme_msu').'</button>
            <ul>
               <li><a href="#tabs1-1">In Progress</a></li>
               <li><a href="#tabs1-2">' . get_string('section-upcoming', 'theme_msu') . '</a></li>
               <li><a href="#tabs1-3">View All</a></li>
              <li><a href="#tabs1-4">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
            </ul>
            <div id="tabs1-1" class="extra">
          <ul>';

          foreach($course_info as $ci)
              {
                  if($ci->timecompleted == "")
                  {
                      $html .= '<li>
                                <div class="tabInfo">
                                <img src="'.get_course_image_url($ci->id).'" class="first-img" />
                                <div class="tabContent">
                                <h2>' . $ci->shortname . '</h2>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . '&nbsp;at&nbsp; ' .gmdate('G:i', $ci->startdate) . ' </span></div><!--end tabContent --><br />
                                <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
                                <div class="tabAction">
                                <div class="arrow">
                                <img src="http://iomad.metrostarsystems.com/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                                <div class="buttons tabButton">
                                <button>Start/Join</button>
                                <button>Manage</button>
                                <button>Edit</button>
                                </div><!--end buttons--></div><!--end tabAction -->
                                </li>

                             ';
                  }
              }

              $html .= '
           </ul>
        </div><!-- end tabs-1 -->
        <div id="tabs1-2" class="extra">
         <ul>';

         foreach($course_info as $ci)
              {
                  if($ci->timecompleted == "")
                  {
                      $html .= '<li>
                                <div class="tabInfo">
                                <img src="'.get_course_image_url($co->id).'" class="first-img" />
                                <div class="tabContent">
                                <h2>' . $ci->shortname . '</h2>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . '&nbsp;at&nbsp; ' .gmdate('G:i', $ci->startdate) . ' </span></div><!--end tabContent --><br />
                                <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
                                <div class="tabAction">
                                <div class="arrow">
                                <img src="http://iomad.metrostarsystems.com/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                                <div class="buttons tabButton">
                                <button>Start/Join</button>
                                <button>Manage</button>
                                <button>Edit</button>
                                </div><!--end buttons--></div><!--end tabAction -->
                                </li>

                             ';
                  }
              }

               $html .= '
           </ul>
        </div><!-- end tabs1-2 -->
        <div id="tabs1-3" class="extra">
         <ul>';

          foreach($course_info as $ci)
              {
                  if($ci->timecompleted == "")
                  {
                      $html .= '<li>
                                <div class="tabInfo">
                                <img src="'.get_course_image_url($ci->id).'" class="first-img" />
                                <div class="tabContent">
                                <h2>' . $ci->shortname . '</h2>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . '&nbsp;at&nbsp; ' .gmdate('G:i', $ci->startdate) . ' </span></div><!--end tabContent --><br />
                                <span class="attendees"> Attendees: 42 | Seats Open: 42  </span>   </div><!--end tabInfo -->
                                <div class="tabAction">
                                <div class="arrow">
                                <img src="http://iomad.metrostarsystems.com/theme/msu/pix/btn_row_arrow.png" width="30" height="30" /></div><!--end gear-->
                                <div class="buttons tabButton">
                                <button>Start/Join</button>
                                <button>Manage</button>
                                <button>Edit</button>
                                </div><!--end buttons--></div><!--end tabAction -->
                                </li>

                             ';
                  }
              }

               $html .= '
           </ul>
        </div><!-- end tabs1-3 -->
        </div></div>';


    return $html;
}


  /**
   * Render starting divs for tabs
   *
   * @return string html
   */
  function tabs_begin($header = '')
  {
      $html = '

          <div>
            <h2>' . $header . '</h2>
            <div class="line-right ax_horizontal_line"></div>
            <div id="tabs">
            <button class="showMore">'. get_string('show-more', 'theme_msu').'</button>
              <ul>
                 <li><a href="#tabs-1">' . get_string('tab-meetings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-2">' . get_string('tab-trainings', 'theme_msu') . '</a></li>
                 <li><a href="#tabs-3">' . get_string('tab-foldup', 'theme_msu') . '</a></li>
              </ul>';

      return $html;
  }


  /**
   * Render ending divs for tabs
   *
   * @return string html
   */
  function tabs_end()
  {
      $html = '
          </div><!-- end tabs -->
        <div>
              ';

      return $html;
  }

  /**
   * Render a list of completed courses
   *
   * @return string html
   */
  function completed_courses()
  {
    global $USER,$CFG,$DB;
    $trainings = get_courses(get_training_id());
    $meetings = get_courses(get_meeting_id());

    $html = '<div id="tabs-4" class="extra">
              <ul>';

      foreach($trainings as $ci)
      {
        //print_r($ci);
        //die();
        $a = get_nstudents($ci->id);
        $s = get_nseats($ci->id);
        $so = $s - $a;
        if(is_course_complete($ci->id) == true) {
          $html .= '
            <li>
              <div class="tabInfo">
                <img src="' . get_course_image_url($ci->id). '" class="first-img" />
                <div class="tabContent">
                  <a href="/course/view.php?id=' . $ci->id .'"><h2>' . $ci->fullname . '</h2></a>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . ' at ' .gmdate('Hi', $ci->startdate) . ' </span>
                </div><!--end tabContent --><br />
                <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
              </div><!--end tabInfo -->
              <div class="tabAction">
                <div class="arrow">
                  <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
                </div><!--end gear-->
                <div class="buttons tabButton">
                  <a href="/course/view.php?id=' . $ci->id .'"><button>Start/Join</button></a>
                  <!--<button>Manage</button>-->
                  <!--<button>Edit</button>-->
                </div><!--end buttons-->
              </div><!--end tabAction -->
            </li>
          ';
        }
      }

    $html .= "</div><!-- end tabs-4 -->";

    $html .= '<div id="tabs-5" class="extra">
              <ul>';

      foreach($meetings as $ci)
      {
          if(is_course_complete($ci->id) == true)
          {
            $html .= '
              <li>
                <div class="tabInfo">
                  <img src="' . get_course_image_url($ci->id ). '" class="first-img" />
                  <div class="tabContent">
                    <a href="/course/view.php?id=' . $ci->id .'"><h2>' . $ci->fullname . '</h2></a>' . '<span class="course-date time">' .gmdate('m.d.y', $ci->startdate) . ' at ' .gmdate('Hi', $ci->startdate) . ' </span>
                  </div><!--end tabContent --><br />
                </div><!--end tabInfo -->
                <div class="tabAction">
                  <div class="arrow">
                    <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30">
                  </div><!--end gear-->
                  <div class="buttons tabButton">
                    <a href="/course/view.php?id=' . $ci->id .'"><button>Start/Join</button></a>
                    <!--<button>Manage</button>-->
                    <!--<button>Edit</button>-->
                  </div><!--end buttons-->
                </div><!--end tabAction -->
              </li>
            ';
          }
      }

    $html .= "</ul></div><!-- end tabs-5 -->";
    return $html;
  }

/**
 * Generate a link to an image
 *
 * @param $course_id course id to retreive context
 * @return string URL to image file
 */
function get_course_image_url($course_id) {
    //global $id; // the course_module id
    global $DB;
    $coursecontext = context_course::instance($course_id);
    $image = $DB->get_record_sql(
      "SELECT filename
       FROM mdl_files
       WHERE contextid = $coursecontext->id
       AND component = 'course'
       AND filearea = 'overviewfiles'
       AND filename != '.'");

    //return print_r($image);
    if($image == null)
    {
      return "/theme/msu/pix/sideIcon.png";
    } else {
      return moodle_url::make_pluginfile_url(
          $coursecontext->id,
          "course",
          "overviewfiles",
          null,
          "/",
          $image->filename,
          false);
    }
}

/**
 * Gets a custom icon to represent courses on course pages.
 *
 * @param  int $course_module_id
 * @return string path to icon
 */
function get_course_image($course_module_id) {
  return "/theme/msu/pix/sideIcon.png";
}

/**
 * Function to get the number of students in a given course.
 *
 * @param $courseid int Unique course ID.
 * @return int The number of students in the course.
 */
function get_nstudents($courseid) {
    global $DB;

    $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
    $context = get_context_instance(CONTEXT_COURSE, $course->id, MUST_EXIST);
    require_capability('moodle/course:viewparticipants', $context);
    $role = $DB->get_record('role', array('shortname'=>'systemuser'), '*', MUST_EXIST);
    //$role = $DB->get_record('role', array('shortname'=>'manager'), '*', MUST_EXIST); //metrochange2016 Mihir

    $contextlist = get_related_contexts_string($context);
	

    list($esql, $params) = get_enrolled_sql($context, NULL, NULL, true);

    $joins = array("FROM {user} u");
    $wheres = array();

    $select = "SELECT u.id, u.username, u.firstname, u.lastname,
                      u.email, u.city, u.country, u.picture,
                      u.lang, u.timezone, u.maildisplay, u.imagealt,
                      COALESCE(ul.timeaccess, 0) AS lastaccess";
    $joins[] = "JOIN ($esql) e ON e.id = u.id"; // course enrolled users only

    // not everybody accessed course yet
    $joins[] = "LEFT JOIN {user_lastaccess} ul ON (ul.userid = u.id AND ul.courseid = :courseid)";

    $params['courseid'] = $course->id;

    // performance hacks - we preload user contexts together with accounts
    list($ccselect, $ccjoin) = context_instance_preload_sql('u.id', CONTEXT_USER, 'ctx');
    $select .= $ccselect;
    $joins[] = $ccjoin;

    // limit list to users with some role only
    if ($role) {
        $wheres[] = "u.id IN (SELECT userid FROM {role_assignments} WHERE roleid = :roleid AND contextid $contextlist)";
        $params['roleid'] = $role->id;
    }

    $from = implode("\n", $joins);
    if ($wheres) {
        $where = "WHERE " . implode(" AND ", $wheres);
    } else {
        $where = "";
    }

    $totalcount = $DB->count_records_sql("SELECT COUNT(u.id) $from $where", $params);
    return $totalcount;
}

/**
 * Function to get the number of seats in a given course.
 * Numbers of seats can only be assigned to self enroll section of course
 * The customint3 field of the enrol table stores the number of seats
 *
 * @param $courseid int Unique course ID.
 * @return int The number of seats in the course.
 */
function get_nseats($courseid)
{
  global $DB;
  $s = $DB->get_record('enrol', array('courseid' => $courseid, 'enrol' => 'self'));

  if(isset($s->customint3) && $s->customint3 > 0)
  {
    return $s->customint3;
  } else {
    return 25;
  }
}

/**
 * Get the category id of an academy
 * Remember that an acadmey is really just a top level category
 *
 * @return int category id
 */
function get_academy_id()
{
  global $DB,$USER;
  $result = $DB->get_record('course_categories', array('idnumber' => $USER->institution));
  return $result->id;
}

/**
 * Returns user assigned teacher role in course. Assuming one teacher per course.
 *
 * @param $courseid
 * @return string
 */
function get_teacher_id($courseid)
{
  global $DB, $USER;

  /* WHERE {role}.name = 'teacher' AND {context}.id = 5"; */
  $sql = "SELECT {course}.id, {course}.fullname, {user}.id, {user}.firstname, {user}.lastname, {role}.name

  FROM {course}
  JOIN {context}  ON {course}.id = {context}.instanceid
  JOIN {role_assignments} ON {role_assignments}.contextid = {context}.id
  JOIN {user} ON {user}.id = {role_assignments}.userid
  JOIN {role} ON {role}.id = {role_assignments}.roleid

  WHERE {course}.id = $courseid AND {user}.id = $USER->id";

  $result = $DB->get_records_sql($sql);

  $teacher_id = $result[$courseid];

  return $teacher_id;
}

/**
 * Get the teacher assigned to a course
 *
 * @param  int $courseid [description]
 * @return string teacher name
 */
function get_teacher($courseid)
{
  global $DB, $USER;

  $teacher = '';

  /* WHERE {role}.name = 'teacher' AND {context}.id = 5"; */
  $sql = "SELECT {course}.id, {course}.fullname, {user}.id, {user}.firstname, {user}.lastname, {role}.name

  FROM {course}
  JOIN {context}  ON {course}.id = {context}.instanceid
  JOIN {role_assignments} ON {role_assignments}.contextid = {context}.id
  JOIN {user} ON {user}.id = {role_assignments}.userid
  JOIN {role} ON {role}.id = {role_assignments}.roleid

  WHERE {course}.id = " . $courseid . " AND {role}.id = 12";

  $result = $DB->get_records_sql($sql);

  if($result == null)
  {
     return get_string('instructor_not_yet_assigned', 'block_cbsi');
  }

  else
  {
    foreach($result as $r)
    {
      $teacher = $r->firstname . ' ' . $r->lastname;
    }
  }

  return $teacher;
}


/**
 * Output text for syntax to add user to Active Directory via the command line
 * for mutliple users user
 *
 * @param string nme of organizational unit of student or teacher
 * @return string command
 */
  function create_users($type)
  {
    global $academies;

    foreach($academies as $academy_id => $academy_name)
    {
      for($i = 1; $i <= 20; $i++)
      {
        echo "dsadd user \"CN=$type" . $i . "_" . strtolower($academy_id) . ",OU=$type,OU=$academy_name,OU=Academies,DC=cbsi-ad,DC=cbsi-connect,DC=net\" -pwd Passw0rd -company \"$academy_id\" -samid $type" . $i . "_" . strtolower($academy_id) . "<br>";
      }
    }
  }

/**
 * Output text for syntax to add user to Active Directory via the command line
 * for a single user
 *
 * @param string nme of organizational unit of student or teacher
 * @return string command
 */
function create_user($type)
{
  global $academies;

  foreach($academies as $academy_id => $academy_name)
  {
    if($academy_id == $_POST['academy'])
    {
      echo "dsadd user \"CN=". $_POST['username'] . ",OU=$type,OU=$academy_name,OU=Academies,DC=cbsi-ad,DC=cbsi-connect,DC=net\" -pwd " . $_POST['password'] . " -company \"$academy_id\" -samid " . $_POST['username'] . " -email " . $_POST['email'] . "<br>";
    }
  }
}

/**
 * Get all of the enrolled students in the course
 *
 * @param  int $course_id
 * @return array user objects
 */
function get_enrolled_students($course_id)
{
  global $DB;

  $sql = "SELECT c.id AS id, c.fullname, u.username, u.firstname, u.lastname, u.email, u.id, u.institution
    FROM mdl_role_assignments ra, mdl_user u, mdl_course c, mdl_context cxt
    WHERE ra.userid = u.id
    AND ra.contextid = cxt.id
    AND cxt.contextlevel = 50
    AND cxt.instanceid = c.id
    AND c.id = $course_id";
    //AND (roleid = 5)";

  $students = $DB->get_records_sql($sql);
  return $students;
}

/**
 * Get student enrolled in a course with the role of instructor
 * @return string email address
 */
function get_enrolled_teachers($course_id)
{
  global $DB;

  $sql = "SELECT c.id AS id, c.fullname, u.username, u.firstname, u.lastname, u.email
    FROM mdl_role_assignments ra, mdl_user u, mdl_course c, mdl_context cxt
    WHERE ra.userid = u.id
    AND ra.contextid = cxt.id
    AND cxt.contextlevel =50
    AND cxt.instanceid = c.id
    AND c.id = $course_id
    AND (roleid = 12)";

  $teachers = $DB->get_records_sql($sql);
  return $teachers[$course_id]->email;
}

/**
 * Get a list of all cohorts
 *
 * @return array of cohort objects
 */
function get_cohorts()
{
  global $DB;

  $sql = "SELECT id,idnumber,name FROM mdl_cohort";

  $cohorts = $DB->get_records_sql($sql);

  return $cohorts;
}

/**
 * Get the members of all cohorts
 *
 * @return array of user objects
 */
function get_cohort_members()
{
  global $DB;

  $sql = "SELECT cohortid,userid FROM mdl_cohort_members";

  $cohort_members = $DB->get_records_sql($sql);

  return $cohort_members;
}

/**
 * Get the cohort idnumber for the current user's cohort
 *
 * @return [type] [description]
 */
function get_cohort_idnumber()
{
  global $DB,$USER;

  $sql = "
    SELECT mdl_cohort.id, mdl_cohort.idnumber, mdl_cohort_members.cohortid, mdl_cohort_members.userid
    FROM mdl_cohort
    JOIN mdl_cohort_members ON mdl_cohort_members.cohortid = mdl_cohort.id
    WHERE mdl_cohort_members.userid = " . $USER->id;

  $membership = $DB->get_record_sql($sql);
  $cid = $membership->idnumber;

  return $cid;
}

/**
 * Get the cohort id of the current user
 *
 * @param  [type] $user [description]
 * @return [type]       [description]
 */
function get_cohort_id($user)
{
  global $DB;
  $i = $user->institution;

  $sql = "SELECT id from {cohort} WHERE idnumber = " . $i;
  $cohort = $DB->get_record_sql($sql);
  $cid = $cohort->id;

  return $cid;
}

/**
 * Get category id for parent category (aka: Academy)
 *
 * @param string academy code (MSS,DNCD, RGPFSSU, etc.)
 * @return int category id
 */
function get_parent_id($institution = null)
{
    global $DB, $USER;

    if($institution == null) {
     $institution = $USER->institution ;
    }

    $sql = <<<SQL
      SELECT id FROM mdl_course_categories WHERE idnumber = "{$institution}"
SQL;
    $parent_id = $DB->get_record_sql($sql);
    $pid = $parent_id->id;

    return $pid;
}

/**
 * Get parent id category of a course
 *
 * @param  int $category_id
 * @return int category id
 */
function get_parent_idnumber($category_id)
{
    global $DB;
    $sql = <<<SQL
      SELECT idnumber FROM mdl_course_categories WHERE id = {$category_id}
SQL;
    $p = $DB->get_record_sql($sql);
    $pid = $p->idnumber;

    return $pid;
}

/**
 * Get the training id for the current user's academy
 *
 * @return int category id
 */
function get_training_id()
{
    global $DB;

    $parent_id = get_parent_id();

    $sql = <<<SQL
      SELECT id FROM mdl_course_categories
      WHERE parent = $parent_id
      AND name = "Training"
SQL;

    $training_id = $DB->get_record_sql($sql);
    $tid = $training_id->id;

    return $tid;
}

/**
 * Get the meeting id for the current user's academy
 *
 * @return int category id
 */
function get_meeting_id()
{
    global $DB;

    $parent_id = get_parent_id();
    $sql = <<<SQL
      SELECT id FROM mdl_course_categories
      WHERE parent = $parent_id
      AND name = "Meeting"
SQL;
    $meeting_id = $DB->get_record_sql($sql);
    $mid = $meeting_id->id;

    return $mid;
}

/**
 * Get all of the courses the current user is enrolled in
 *
 * @return array course objects
 */
function enrolled_courses()
{
    global $DB,$USER;

    $sql = '
        SELECT * FROM {user_enrolments}
        INNER JOIN {enrol} ON {user_enrolments}.enrolid = {enrol}.id
        INNER JOIN {course} ON {enrol}.courseid = {enrol}.courseid
        WHERE userid = ' . $USER->id;

    $enrolled = $DB->get_records_sql($sql);

    return $enrolled;
}

/**
 * Check if a course is complete
 *
 * @param  int  $course_id
 * @return boolean
 */
function is_course_complete($course_id)
{
  global $DB,$USER;

  $complete = $DB->get_records_sql("SELECT * FROM {course_completions} WHERE userid = " . $USER->id . " AND course = " . $course_id . " AND timecompleted IS NOT NULL");

  if($complete != null)
  {
    return true;
  } else {
    return false;
  }
}

/**
 * Retreive courses with a parent of Sharted category
 *
 * @return [type] [description]
 */
function get_shared_courses()
{
  global $DB;
  return $courses = get_courses(4);
}

/**
 * Get element with data-id value same as user's academy id
 * Then hide all other line items
 * Which effectively hides all other academy functions
 *
 * @return string html
 */
function restrict_category_listing_js() {
  $html = '';

  if(!has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
    $html .= '
      <script>
        $(document).ready(function() {
          var uri = window.location.pathname;

          function hide_categories() {
            $("li[data-id=' . get_academy_id() . ']").siblings().hide();
          };

          function remove_create_new_course_link() {
            $("a:contains(\'Create new course\')").hide();
          };

          function add_view() {
            $(".categoryname").each(function() {

            if(!$(this).hasClass(\'amendedPath\')) {

              var category = $(this).attr("href");
              $(this).attr("href", category + \'&view=courses\').addClass(\'amendedPath\');
            }
            });
          };

          //hide front page editor from non admins
          $("p:contains(\'Front page\')").hide();

          if(uri == "/course/management.php" ) {
            hide_categories();
            add_view();
          }

          //wait for category tree to expand then append links
          $("a[data-action=\'expand\']").click(function() {
            setTimeout(function() { add_view() },750);
            remove_create_new_course_link();
          });
        });

      </script>
    ';
  }

  return $html;
}

/**
 * Redirect the user to a category listing page matching their academy
 * Prevents user from seeing other academies courses
 * @return
 */
function restrict_category_listing()
{
  global $USER;


  //check if admin
  if(!has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM)))
  {
    //if not admin
    //check that user belongs to academy
    if(isset($_GET['categoryid']))
    {
      $academy = get_academy_id();
      $parent  = get_parent_idnumber($_GET['categoryid']);
      //check if user->institution mataches categories parent idnumber
      if($USER->institution == $parent)
      {
        if(get_parent_idnumber($academy) != get_parent_idnumber($_GET['categoryid']))
        {
          header('Location: http://cbsi-connect.net/course/management.php?categoryid=' . $academy);
        }
      }
    }
  }
}

/**
 * Renders a user's badges
 *
 * @return string html
 */
function badges() {
  global $CFG,$USER,$DB;

  require_once($CFG->dirroot . '/lib/badgeslib.php');

  $html = '
    <h2 id="badge-header">'. get_string('badges','theme_msu').'</h2>
      <div id="badges-container">';

      $badges = badges_get_user_badges($USER->id);

      foreach($badges as $u) {
        $contextid_sql = "SELECT contextid FROM {files} WHERE itemid = " . $u->id;
        $contextid = $DB->get_record_sql($contextid_sql);

        $html .= '
          <div class="cbsi-badge">

              <img src="/pluginfile.php/'. $contextid->contextid.'/badges/badgeimage/' . $u->id . '/f1">

            ';

      $html .= '
          <div class="cbsi-badge-caption">
            <ul style="list-style: none;">
              <li><p><a href="/badges/overview.php?id='. $u->id.'">' . $u->name . '</a></p></li>
              <li>' . date('m.d.Y', $u->dateissued) . '</li>
            </ul>
          </div><!-- end cbsi-badge-caption-->
          <div class="profile-title"></div>';


      $html .= '
        </div><!--end cbsi-badge -->
    ';
  }

    $html .= '
          </div><!-- end badges-container -->';

  return $html;
}

function remove_create_new_course_link() {
  $html = '';

  if(!has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
    $html = '
      <script>
        $(document).ready(function() {
          if(window.location.pathname == "/course/management.php") {
            $("a:contains(\'Create new course\')").hide();
          }
        });
      </script>
    ';
  }

  return $html;
}

/**
 * Creates and adds a variable/object dump to a log file for debugging
 *
 * @param $name of variable testing
 * @param $bug value of variable testing
 * @param $line of code test called from
 */
function debug($name, $bug, $filename = '', $line = '') {
  global $CFG;

  $log = __FILE__ . "\\";

  if(is_array($bug)) {
    $bug = print_r($bug);
  }

  if(is_object($bug)) {
    $bug = var_dump($bug);
  }

  $log .= "\nLine: $line\n";
  $log .= "$name: \n $bug\n";

  file_put_contents($CFG->dataroot."/$name_" . 'debug.log', $log, FILE_APPEND | LOCK_EX);
}

/**
 * Get course start time (unix timestamp) from mdl_block_cbsi table
 *
 * @param $courseid
 * @return int
 */
function get_course_start_time() {
  global $DB;

  $html = '';
  $sql = "SELECT course_start_time FROM {blocks_cbsi_course} WHERE course_id = " . $courseid;
  $time = $DB->get_record_sql($sql);
  return $html;
}

/**
 * Add a course to a cohort and sync it with the existing members so they are
 * all enrolled in that course
 * @deprecated business decision to not globally enroll all members of a cohort
 * into a course
 * @param  int $courseid [description]
 * @param  int $cohortid [description]
 * @return
 */
function cohort_add_course($courseid, $cohortid) {
    global $DB;

    // Get the cohort enrol plugin
    $enrol = enrol_get_plugin('cohort');

    // Get the course record.
    $course = $DB->get_record('course', array('id' => $courseid));

    // Add a cohort instance to the course.
    $instance = array();
    $instance['name'] = '';
    $instance['status'] = ENROL_INSTANCE_ENABLED; // Enable it.
    $instance['customint1'] = $cohortid; // Used to store the cohort id.
    $instance['roleid'] = $enrol->get_config('roleid'); // Default role for cohort enrol which is usually student.
    $instance['customint2'] = 0; // Optional group id.
    $enrol->add_instance($course, $instance);

    // Sync the existing cohort members.
    $trace = new null_progress_trace();
    enrol_cohort_sync($trace, $course->id);
    $trace->finished();
}

/*
 * Return array of meeting objects
 */
function get_meetings() {
  global $DB,$USER;

  $sql = <<<SQL
    SELECT *
    FROM mdl_course
    INNER JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
    INNER JOIN mdl_enrol ON mdl_course.id = mdl_enrol.courseid
    INNER JOIN mdl_user_enrolments ON mdl_enrol.id = mdl_user_enrolments.enrolid
    WHERE mdl_course_categories.name = "Meeting"
    AND mdl_user_enrolments.userid = {$USER->id}
    AND mdl_course.visible = 1
    ORDER BY mdl_course.timecreated DESC
SQL;

    try {
      $meetings = $DB->get_records_sql($sql);
      return $meetings;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
}

/*
 * Return array of meeting objects
 */
function get_shared_courses_new() {
  global $DB,$USER;

  $sql = <<<SQL
    SELECT *
    FROM mdl_course
    INNER JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
    INNER JOIN mdl_enrol ON mdl_course.id = mdl_enrol.courseid
    INNER JOIN mdl_user_enrolments ON mdl_enrol.id = mdl_user_enrolments.enrolid
    WHERE mdl_course_categories.name = "Shared"
    AND mdl_user_enrolments.userid = {$USER->id}
    AND mdl_course.visible = 1
    ORDER BY mdl_course.timecreated DESC
SQL;

    try {
      $meetings = $DB->get_records_sql($sql);
      return $meetings;
    } catch (Exception $e) {
      echo $e->getMessage();
    }
}

/**
 * Retreive all of the trainings the current user is/has enrolled in, ever.
 *
 * @return array of course objects
 */
function get_trainings() {
  global $DB,$USER;

  $sql = <<<SQL
    SELECT *
    FROM mdl_course
    INNER JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
    INNER JOIN mdl_enrol ON mdl_course.id = mdl_enrol.courseid
    INNER JOIN mdl_user_enrolments ON mdl_enrol.id = mdl_user_enrolments.enrolid
    WHERE (
      mdl_course_categories.name = "Training"
      OR mdl_course_categories.name = "Shared"
    )
    AND mdl_user_enrolments.userid = {$USER->id}
    AND mdl_course.visible = 1
    ORDER BY mdl_course.timecreated DESC
SQL;

  $trainings = $DB->get_records_sql($sql);

  return $trainings;
}

/**
 * Retreive all of the trainings and shared courses the current user is enrolled
 * in that started prior to the current date.
 * @return array of course objects
 */
function get_past_trainings() {
  global $DB,$USER;

  $sql = '
    SELECT *
    FROM mdl_course
    JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
    JOIN mdl_enrol ON mdl_course.id = mdl_enrol.courseid
    JOIN mdl_user_enrolments ON mdl_enrol.id = mdl_user_enrolments.enrolid
    WHERE (mdl_course_categories.name = "Training" OR mdl_course_categories.name = "Shared") AND mdl_user_enrolments.userid = ' . $USER->id . ' AND startdate < UNIX_TIMESTAMP() ORDER BY startdate DESC';

  $trainings = $DB->get_records_sql($sql);

  return $trainings;
}

/*
 * Return array of training objects
 */
function get_future_trainings() {
  global $DB,$USER;

  $sql = '
    SELECT *
    FROM mdl_course
    JOIN mdl_course_categories ON mdl_course.category = mdl_course_categories.id
    JOIN mdl_enrol ON mdl_course.id = mdl_enrol.courseid
    JOIN mdl_user_enrolments ON mdl_enrol.id = mdl_user_enrolments.enrolid
    WHERE (mdl_course_categories.name = "Training" OR mdl_course_categories.name = "Shared") AND mdl_user_enrolments.userid = ' . $USER->id . ' AND startdate >= UNIX_TIMESTAMP() ORDER BY startdate DESC';

  $trainings = $DB->get_records_sql($sql);

  return $trainings;
}

function render_completed_trainings() {
  $training = get_past_trainings();
  $html = '
    <div id="completed">
        <ul>
            <li><a href="#meetings">Meetings</a></li>
            <li><a href="#trainings">Trainings</a></li>
        </ul>
        <div>
            <div id="meetings">
                <p>meetings content</p>
            </div>
            <div id="trainings">
                <p>trainings content</p>
            </div>
        </div>
    </div>';

  $html .= '
    <script type="text/javascript">
      YUI().use(\'tabview\', function(Y) {
          var tabview = new Y.TabView({srcNode:\'#completed\'});
          tabview.render();
      });
    </script>
    <style>
      .yui3-skin-sam .yui3-tab-label {
        background: none;
        border: solid #a3a3a3;
        border-width: 1px 1px 0 1px;
        color: #000;
        cursor: pointer;
        font-size: 85%;
        padding: .3em .75em;
        text-decoration: none;
      }

      .yui3-skin-sam .yui3-tabview-panel {
        border: none;
        padding: .25em .5em;
        background: none;
      }

      .yui3-skin-sam .yui3-tab-selected .yui3-tab-label, .yui3-skin-sam .yui3-tab-selected .yui3-tab-label:focus, .yui3-skin-sam .yui3-tab-selected .yui3-tab-label:hover {
        background: none;
        color: none;
      }
      </style>
  ';

  return $html;
}

/**
 * @deprecated
 */
function render_completed_trainings_old() {
  $training = get_past_trainings();

  foreach($training as $t) {
    $html .= '
      <li>
        <div class="tabInfo">
          <img src="' . get_course_image_url($t->courseid) . '" class="first-img" />
          <div class="tabContent">
            <a href="/course/view.php?id=' . $t->courseid . '"><h2>' . $t->fullname . '</h2></a>' . '<span class="course-date time">' . gmdate('m.d.y', $t->startdate) . '&nbsp;at ' . gmdate('H:i', $t->startdate) . 'hrs</span>
          </div><!--end tabContent --><br />
          <span class="attendees"> Attendees: ' . $a . ' | Seats Open: ' . $so . ' of ' . $s . '</span>
        </div><!--end tabInfo -->
          <div class="tabAction">
            <div class="arrow">
              <img src="/theme/msu/pix/btn_row_arrow.png" width="30" height="30" />
            </div><!--end gear-->
            <div class="buttons tabButton">
              <a href="/course/view.php?id=' . $t->courseid . '"><button>Start/Join</button></a>';

    if (has_capability('moodle/course:update', context_course::instance($t->courseid)))
    {
      $html .= '<a href="/course/view.php?id=' . $t->courseid . '&sesskey=' . sesskey() . '&edit=on"><button>Manage</button></a>';
      $html .= '<a href="/course/edit.php?id=' . $t->courseid . '"><button>Edit</button></a>';
    }
  }

  return $html;
}

/**
 * Get users by role.
 *
 * @param  [type] $roleid   [description]
 * @param  [type] $courseid [description]
 * @return [type]           [description]
 */
function get_users_by_role($roleid,$courseid) {
  global $DB;
  //get a list of users based on roleid
    $users_sql = "SELECT *
      FROM {role_assignments}
      JOIN {context} ON {role_assignments}.contextid = {context}.id
      WHERE {role_assignments}.roleid = $roleid
      AND {context}.instanceid = " . $courseid;

    $users = $DB->get_records_sql($users_sql);

    return $users;
}

/**
 * Check if shortname was modified since the course was originally created
 *
 * @deprecated Not needed anymore. The Course Creation Wizard was updated to
 * create a meaningful shortname instead of random values.
 *
 * @param int courseid
 * @return bool
 */
function has_shortname_changed($courseid) {
  global $DB;

  $default_shortname_sql = "SELECT default_shortname FROM {block_cbsi} WHERE courseid = $courseid";
  $current_shortname_sql = "SELECT shortname FROM {course} WHERE id = $courseid";
  $default_shortname     = $DB->get_record_sql($default_shortname_sql);
  $current_shortname     = $DB->get_record_sql($current_shortname_sql);

  if($default_shortname === $current_shortname) {
    return true;
  } else {
    return false;
  }
}

/**
 * Check if a course has a start date
 *
 * @param int courseid
 * @return bool
 */
function is_course_start_set($courseid) {
  global $DB;

  $start_datetime_sql    = "SELECT start_datetime FROM {block_cbsi} WHERE courseid = $courseid";
  $start_datetime = $DB->get_record_sql($start_datetime_sql);

  if(isset($start_datetime->start_datetime)) {
    return true;
  } else {
    return false;
  }
}

/**
 * Check if a course has an enrollment method
 *
 * @param int courseid
 * @return bool
 */
function has_enrollment_method($courseid) {
  global $DB;

  $sql = "SELECT status FROM {enrol} WHERE courseid = $courseid AND status = 1";
  $result = $DB->get_records_sql($sql);

  if(empty($result)) {
    return false;
  } else {
    return true;
  }
}

/**
 * Check if a course has a badge that can be awarded
 *
 * @param  [type]  $courseid [description]
 * @return boolean           [description]
 */
function has_badge($courseid) {
  global $DB;

  $sql    = "SELECT id FROM {badge} WHERE courseid = $courseid LIMIT 1";
  $result = $DB->get_record_sql($sql);

  if(!empty($result)) {
    return true;
  } else {
    return false;
  }
}

/**
 * Check if a course has an instructor assigned (user with instructor role)
 *
 * @param  inte  $courseid
 * @return boolean
 */
function has_instructor($courseid) {
  global $DB;

  $sql = "SELECT {course}.id, {course}.fullname, {user}.id, {user}.firstname, {user}.lastname, {role}.name

  FROM {course}
  JOIN {context}  ON {course}.id = {context}.instanceid
  JOIN {role_assignments} ON {role_assignments}.contextid = {context}.id
  JOIN {user} ON {user}.id = {role_assignments}.userid
  JOIN {role} ON {role}.id = {role_assignments}.roleid

  WHERE {course}.id = $courseid and {role}.shortname = 'instructor' LIMIT 1";

  $teacher = $DB->get_record_sql($sql);

  if(!empty($teacher)) {
    return true;
  } else {
    return false;
  }
}

/**
 * Manually enroll a user into a course
 *
 * @param $userid
 * @param $courseid
 * @return bool
 */
function enroll_user($userid, $courseid) {
  global $CFG;
  require_once($CFG->dirroot. "/enrol/manual/externallib.php");

    $enrollments = array(
      array(
        'roleid'    => 5,
        'userid'    => $userid,
        'courseid'  => $courseid,
        'timestart' => null,
        'timeend'   => null,
        'suspend'   => null)
      );

    $contextid = context_course::instance($courseid);
    if(has_capability('enrol/manual:enrol', $contextid)) {
        try {
          enrol_manual_external::enrol_users($enrollments);
          return true;
        } catch (Exception $e) {
          error_log($e);  
	  return false;
        }
    }
    else{
	error_log("User does not have the capability to manually enroll users for this course.");
    }
}

/**
 * Academies are actually top level categories.
 * The category idnumber is a unique identifier for that category
 *
 * @param $idnumber alphanumeric course identifier
 * @return string
 */
function get_academy_name($idnumber) {
  global $DB;

  $sql      = 'SELECT name FROM mdl_course_categories WHERE idnumber = "' . $idnumber . '"';
  $result   = $DB->get_record_sql($sql);
  $academy  = $result->name;

  return $academy;
}

/**
 * Retrieve course end date from mdl_block_cbsi
 * Course end date is not native to Moodle
 *
 * @param  [type] $courseid [description]
 * @return [type]           [description]
 */
function get_course_enddate($courseid) {
  global $DB;
  $sql    = <<<SQL
    SELECT end_datetime FROM mdl_block_cbsi WHERE courseid = {$courseid} AND end_datetime IS NOT NULL
SQL;

  $result = $DB->get_record_sql($sql);
  return $result->end_datetime;
}

/**
 * Create unix timestamp
 * Date format: yyyy-mm-dd
 * Time format: HH:MM:SS
 *
 * @param string date
 * @param string time
 * @return integer
 */
function get_unix_timestamp($d, $t) {
    $date     = explode('-', $d);
    $time     = explode(':', $t);
    $hour     = $time[0];
    $minute   = $time[1];
    $second   = $time[2];
    $month    = $date[1];
    $day      = $date[2];
    $year     = $date[0];
    $datetime = mktime($hour, $minute, $second, $month, $day, $year);

    return $datetime;
}

function get_instructors($courseid) {
  global $DB, $USER;

  /* WHERE {role}.name = 'teacher' AND {context}.id = 5"; */
  $sql = "SELECT {course}.id, {course}.fullname, {user}.id, {user}.firstname, {user}.lastname, {user}.email, {user}.institution, {role}.name

  FROM {course}
  JOIN {context}  ON {course}.id = {context}.instanceid
  JOIN {role_assignments} ON {role_assignments}.contextid = {context}.id
  JOIN {user} ON {user}.id = {role_assignments}.userid
  JOIN {role} ON {role}.id = {role_assignments}.roleid

  WHERE {course}.id = " . $courseid . " AND {role}.id = 12";

  $result = $DB->get_records_sql($sql);

  return $result;
}

/**
 * Add record to block_cbsi table to track course
 *
 * @param  int $id course id
 * @return string success or error message
 */
function create_block_cbsi_record($id, $start = 1451606400, $end = 3600, $institution = null) {
  global $DB,$USER;

  $course = get_course($id);
  $table  = "block_cbsi";

  if($institution == null) {
    $institution = $USER->institution;
  }


  $parent = <<<SQL
    SELECT parent
    FROM mdl_course_categories
    WHERE id = {$course->category}
SQL;

  $record                 = new stdClass();
  $record->courseid       = $id;
  $record->course_creator = $USER->id;
  $record->start_datetime = $start;
  $record->end_datetime   = $start + $end;
  $record->categoryid     = $course->category;
  $record->parent_category = get_parent_id($institution);

  if($course->format == "topics") {
    $record->default_shortname = "Meeting_{$course->id}";
  } else {
    $record->default_shortname = "Training_{$course->id}";
  }

  try{
    $DB->insert_record($table, $record);
    echo "record added successfully\n";
  } catch(Exception $e) {
    echo $e->getMessage();
  }
}

function set_block_cbsi_enddate($courseid, $end = 1451606400) {
  global $DB;
  $course = $DB->get_record('block_cbsi', array('courseid'=>$courseid), '*', MUST_EXIST);
  $course->end_datetime = $end;
  $DB->update_record("block_cbsi", $course);
}

/*
 * callback to be used for heredocs
 */
// $cb = function($fn) {
//   return $fn;
// };

/**
 * Wizard had bug, changed all categoryid field entries to cateogry of last
 * course created. The wizard has been fixed. This function fixes the incorrect
 * records
 *
 * @return [type] [description]
 */
function fix_block_cbsi_parent_category() {
  global $DB;

  //get all courses from mdl_block_cbsi
  $sql = <<<SQL
      SELECT *
      FROM mdl_block_cbsi
SQL;

  $courses = $DB->get_records_sql($sql);

  //get parent_id for coursecreator
  foreach($courses as $course) {
      //get course creator record
      $user = $DB->get_record('user', array('id' => $course->course_creator));
      //get parent category (academy) of course creator
      $parent_category = get_parent_id($user->institution);
      //set categoryid to parent_id's corresponding categoryid
      $record = $DB->get_record('block_cbsi', array('courseid' => $course->id));
      $course->parent_category = $parent_category;
      $DB->update_record('block_cbsi', $course);
  }
}

/**
 * Fix course and category id in mdl_block_cbsi table
 *
 * @return none
 */
function fix_block_cbsi_categoryid() {
  global $DB;

  //get all courses from mdl_block_cbsi
  $sql = <<<SQL
      SELECT *
      FROM mdl_block_cbsi
SQL;

  $records = $DB->get_records_sql($sql);

  foreach($records as $record) {
      $course = get_course($record->courseid);
      $record->categoryid = $course->category;
      $DB->update_record('block_cbsi', $record);
  }
}

/**
 * Check if course is in shared category
 *
 * @param  int  $course_id
 * @return boolean
 */
function is_shared($course_id) {
  global $DB;

  $sql = <<<SQL
    SELECT *
    FROM mdl_course
    WHERE id = {$course_id}
    AND category = 4;
SQL;

  $results = $DB->get_record_sql($sql);

  if($results) {
    return true;
  } else {
    return false;
  }
}
