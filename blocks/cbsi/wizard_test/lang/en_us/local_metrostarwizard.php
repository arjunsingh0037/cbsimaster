<?php
$string['pluginname'] = 'Metrostar Wizard';

//exists for multiple pages
$string['exit_wizard'] = 'Exit Wizard';
$string['wizard_name'] = 'CBSI-Connect Creation Wizard';
$string['previous'] = 'Previous';
$string['next'] = 'Next';

//page one
$string['page_one_question'] = 'Click Start to Create a New Meeting or Training';
$string['create_new_meeting_training'] = 'Start';


//page two
$string['page_two_question'] = 'Select Meeting or Training';
$string['meeting'] = 'Meeting';
$string['training'] = 'Training';


//page three
$string['page_three_question'] = 'Do you want to make it available to other academies?';
$string['no_private_course'] = 'No:<br>Private';
$string['yes_all'] = 'Yes:<br>All Academies';
$string['yes_selected'] = 'Yes:<br>Selected Academies';


//page four
$string['course_name'] = 'Meeting or Training Title';
$string['course_language'] = 'Choose your language';
$string['course_description'] = 'Provide a brief description?';
$string['course_sections'] = 'How many sections/topics?';

//page six
//$string['page_six_question'] = 'Set the date, time, and duration of the course?';
$string['page_six_question'] = 'Set the start date of the training or meeting?';
$string['page_six_no'] = 'No:<br>Decide Later';
//$string['page_six_yes'] = 'Yes:<br> Set Dates and Time';
$string['page_six_yes'] = 'Yes:<br> Set Date';
$string['page_six_date_startdate'] = 'When do you want this training or meeting to begin?';
$string['page_six_date_starttime'] = 'Choose a start time:';
$string['page_six_date_enddate'] = 'When do you want this course to end?';
$string['page_six_date_endtime'] = 'Choose an end time:';


//pagegive
$string['final_page_header'] = 'Congratulations! Your training or meeting is setup. What would you like to do now?';
$string['save_go_home'] = 'Save and Go Back to Home Page';
$string['delete_go_home'] = 'Delete and Go Back to Home Page';
$string['save_start'] = 'Save and Start Setup';

$string['lang_select'] = "Select";
$string['lang_english'] = "Eglish";
$string['lang_spanish'] = "Spanish";
$string['lang_dutch'] = "Dutch";
?>











