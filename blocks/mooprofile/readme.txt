=== MooProfile "Block Plugin" ===

Author:    Mohamed Alsharaf (mohamed.alsharaf@gmail.com)
Website:   http://jamandcheese-on-phptoast.com
Copyright: 2011-2012 Mohamed Alsharaf
License:   http://www.gnu.org/copyleft/gpl.html
Version:   1.1.5

== Changelog: ==
1.0.0 - First version
1.0.1 - Added missing data in version.php
1.0.2 - Compatible with Moodle 2.3
1.1.0 - New feature:
         - The ability to display a list of users in a course
           based on their role in that course (e.g list all teachers in a course)
1.1.1 - Fixed bug unable to delete existing username
1.1.2 - Fixed bug in block_mooprofile::cleanup_blockdata()
1.1.3 - Bug fixes
1.1.4 - Bug fixes
1.1.5 - Bug fixes

== Installation ==
1. Copy and paste the folder mooprofile into the blocks directory.

4. Log into your Moodle site as an admin user.

5. Go to the notifications page to install this plugin.

== Block Usages ==

List of some examples on what to use this plugin for:

* Show profile(s) of the course teacher(s).
* Show profile of a quiz marker and link to support.
* Show profile of a winner of a competition.
* Show profile of a highest achiever.
