<?php
// $Id: inscriptions_massives.php 356 2010-02-27 13:15:34Z ppollet $
/**
 * A bulk enrolment plugin that allow teachers to massively enrol existing accounts to their courses,
 * with an option of adding every user to a group
 * Version for Moodle 1.9.x courtesy of Patrick POLLET & Valery FREMAUX  France, February 2010
 * Version for Moodle 2.x by pp@patrickpollet.net March 2012
 */

require(dirname(dirname(dirname(__FILE__))).'/config.php');
// require_once ('lib.php');
// require_once ('batch_form.php');       
// $id = optional_param('id',0,PARAM_INT);
// $courseid = required_param('courseid',PARAM_INT);

// $course = $DB->get_record('course', array('id' => $courseid));
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('incourse');
$PAGE->set_title(get_string('pluginname', 'local_coursecreation_wizard'));
$PAGE->set_heading('Course Creation Wizard');
$PAGE->set_url($CFG->wwwroot . '/local/coursecreation_wizard/coursecreation.php');
$PAGE->requires->css('/local/coursecreation_wizard/style/jquerysteps.css');
$PAGE->requires->css('/local/coursecreation_wizard/style/custom.css');
//$PAGE->requires->css('/local/coursecreation_wizard/style/normalize.css');
$PAGE->requires->js('/local/coursecreation_wizard/jquery/jquerysteps.js');
//$PAGE->requires->js('/local/coursecreation_wizard/jquery/bootstrap.js');
/*$PAGE->requires->css('/local/coursecreation_wizard/style/bootstrap.css'); */
$PAGE->requires->js('/local/coursecreation_wizard/jquery/timepicker.js');
$PAGE->requires->css('/local/coursecreation_wizard/style/timepicker.css');

$PAGE->navbar->add(get_string('pluginname', 'local_coursecreation_wizard'));
echo $OUTPUT->header();
global $DB, $CFG;
require_login();


?>
<!-- <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/js/bootstrap-timepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/css/bootstrap-timepicker.min.css">
 -->

 <h1 class="text-center"><?php  echo get_string("pluginname","local_coursecreation_wizard")?></h1>

 <div class="container-fluid firstcont">
<div class="stepwizard">

    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Step 3</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
            <p>Step 4</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
            <p>Step 5</p>
        </div>
        <div class="stepwizard-step" id ="stepnone">
            <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
            <p>Step 6</p>
        </div>
    </div>
</div>

    <div class="row-fluid setup-content" id="step-1">

        <div class="col-xs-12">
            <div class="span12 firstdiv">
                 <h3 class="firsth2"><?php echo get_string('whatwant','local_coursecreation_wizard')?></h3>
         
                  
                    <button class="nextBtn12 designbtn" type="button" ><?php echo get_string('Creatingnew','local_coursecreation_wizard')?></button>
               <!--  <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button> -->
            </div>
        </div>
    </div>
    <div class="row-fluid setup-content" id="step-2">
       
        <div class="col-xs-12">
            <div class="span12">
                <h3 class="firsth2"><?php echo get_string('whatwantclass','local_coursecreation_wizard')?></h3>
                      <?php
                            $data = array();
                            function category_tree($parent = 0, $optional = true) {
                            global $DB;
                            $data = array();
                            $resultset = $DB->get_records('course_categories', array('parent' => (int) $parent));
                            if ($resultset) {
                                foreach ($resultset as $id => $row) {
                                    if ($optional || $id != 1) {
                                        $data[$id] = array(
                                            'id' => $row->id,
                                            'name' => $row->name,
                                            'flag' => category_tree($row->id)
                                        );
                                       
                                    }
                                }
                            }
                            return $data;
                            }

                            $categories = category_tree();
                            //print_object($csdata);
                           
                            foreach ($categories as $category) {
                                
								if ($category['name'] !== 'Shared') {
									echo'<div class="row-fluid">';
									echo'<div class="span12">';
								 
									echo '<h4 class="fistdivh3">'.$category['name'].'  :'.'</h4>';
									echo '</div>';
									echo '</div>';
									if (count($category['flag']) > 0) {
									echo'<div class="row-fluid">';
									echo'<div class="span12">';
										foreach($category['flag'] as $subcategory) {
											echo'<div class="span4">';
											  echo '<button class=" nextBtn12 catcsbtn firstdesignbt" type="button"  value ="'.$category[id].'" id="'.$subcategory['id'].'">'.$subcategory['name'].'</button>';
											echo '</div>';

										}
									 echo '</div>';
									 echo '</div>';
									}
								
								}
                              echo '<hr>';  
                            }
							echo '<hr>';
                            
                      
                       ?>
                     
                     <!-- <button class="btn btn-primary nextBtn12 btn-lg " type="button" >Training</button> -->

                
            </div>
        </div>
    </div>
    <div class="row-fluid setup-content" id="step-3">
      
        <div class="col-xs-12">
            <div class="span12 seconddiv">
               <input type="hidden" name="category" id="category" value="">
               <input type="hidden" name="subcategory" id="subcategory" value="">
               
                <div class="form-group">
                    <p><?php echo get_string('Whattitle','local_coursecreation_wizard')?></p>
                    <input  maxlength="100" type="text" required="required" class="form-control" id="title" placeholder="Enter title"  />
                </div>
                
                <!-- <p><?php //echo get_string('whatlanguage','local_coursecreation_wizard')?></p>
                <select id="language" class="form-control">
                <option value="English">English</option>
                <option value="Chines">Chines</option>
                </select> -->
               
                
                <div class="form-group">
                    <p><?php echo get_string('whatdesc','local_coursecreation_wizard')?></p>
                    <textarea rows="4" cols="50" required class="form-control" placeholder="Enter title" id="description" ></textarea>
                
                    
                </div>
                <button class="btn btn-primary nextBtn btn-lg" type="button" >Next</button>
            </div>
        </div>
    </div>
     <div class="row-fluid setup-content" id="step-4">
       
        <div class="col-xs-12">
            <div class="span12 thirddiv">
                <div id ="demo"> 
                </div>
                  <h3 class="firsth2">  <?php echo get_string('Setdate','local_coursecreation_wizard')?>  </h3>
                  <button class="nextBtn12 seconddesignbt" id= "dlater" type="button" >Decide Later</button>
                  <button class="nextBtn12 seconddesignbt" type="button" id = "setdates" >Set Date</button>
               
            </div>
        </div>
    </div>
    <div class="row-fluid setup-content directdelete" id="step-5">
       
        <div class="col-xs-12">
            <div class="span12 firstdiv">
                <h3 class="firsth2">   <?php echo get_string('Setdate','local_coursecreation_wizard')?>  </h3>
                <p> <?php echo get_string('whattrain','local_coursecreation_wizard')?></p>
                <div class="input-group bootstrap-timepicker timepicker">
                   
                   <input type="date" id="date"  required="required" class="form-control" />
                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div> 
                <p><?php echo get_string('Choosetime','local_coursecreation_wizard')?></p>
                <div class="input-group bootstrap-timepicker timepicker">
                    
                   <input id="timepicker1" type="text" class="form-control input-small">
                   <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                </div>
                <p> <?php echo get_string('whattrainend','local_coursecreation_wizard')?></p>
                <div class="input-group bootstrap-timepicker timepicker">
                   
                   <input type="date" id="enddate"  required="required" class="form-control" />
                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div> 
                <p><?php echo get_string('Choosetime','local_coursecreation_wizard')?></p>
                <div class="input-group bootstrap-timepicker timepicker">
                    
                   <input id="timepicker2" type="text" class="form-control input-small">
                   <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                </div>  
               <button class="btn btn-primary nextBtn btn-lg" type="button" id ="finaldata" >Next</button>
            </div>
        </div>
    </div>
    <div class="row-fluid setup-content directsave" id="step-6">
        <div class="span12">
            <div class="span2"> </div>
            <div class="span8 padlf10"> 
                
                <input type="hidden" id ="path" value="<?php echo $CFG->wwwroot; ?>" />
                <h3 class="firsth2 text-center"><?php echo get_string('trainingsetup','local_coursecreation_wizard')?></h3>
                <div class="row-fluid">    
                <div class="span12 brsd">    
                    
                        <button class="span4 seconddesignbt"  id="finalsave">
                            <a href="<?php echo $CFG->wwwroot; ?>">
                       <?php echo get_string('SaveHomePage','local_coursecreation_wizard')?>
                        </a>
                       </button>
                    <div class="span4"></div>
                    
                    <button class="span4 seconddesignbt"  id ="finaldelete"><?php echo get_string('DeletePage','local_coursecreation_wizard')?></button>
                </div>
                </div>
				<div class="row-fluid"> 
				<hr>
				<center><h2><b>OR </b></h2></center>
				<hr>
				</div>
				<div class="row-fluid"> 
				
                <div class="span12"> 
                    <button class="span4 seconddesignbt"  id ="finalset"><?php echo get_string('GoPage','local_coursecreation_wizard')?></button>
					<div class="span4"></div>
                    <button class=" span4 seconddesignbt"  id ="finalset2"><?php echo get_string('SaveUp','local_coursecreation_wizard')?></button>
               <!--  <button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button> -->
                </div>
                </div>
            </div>
			<div class="span2"> </div>
        </div>
    </div>

</div>


<?php
echo $OUTPUT->footer();

?>


