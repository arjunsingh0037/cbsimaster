<?php

require_once('../../../config.php');
require_once('../../../lib/accesslib.php');
require_once('curl.php');

global $CFG,$course,$DB;

$user_id=$DB->get_record_sql("select id,username,password,institution from mdl_user where email='webservices@metrostarsystems.com'");
$userid=$user_id->id;
$username=$user_id->username;
$userpassword=$user_id->password;
$usercompany=$user_id->institution;

$get_token_url = $CFG->wwwroot."/login/token.php?username=".$username."&password=".$userpassword."&service=metrostar_get_course";

$users_token = file_get_contents($get_token_url);

$users_token=json_decode($users_token);

$token=$users_token->token;


$all_tokens=$DB->get_records_sql("select token from mdl_external_tokens where externalserviceid='4'");
$token_check = array();
foreach($all_tokens as $stored_token){
$token_check[] = $stored_token->token;
}


if (!in_array($token,$token_check)){
   die("You are not allowed to use this service");
} 

$domainname = $CFG->wwwroot;

/// FUNCTION NAME
$functionname = 'local_metrostargetcourse_get_course';

// REST RETURNED VALUES FORMAT
$restformat = 'json';

if (isset($_GET['filter'])){
$filter=htmlspecialchars(trim($_GET['filter']),ENT_QUOTES);
}

if (isset($_GET['id'])){
$id=htmlspecialchars(trim($_GET['id']),ENT_QUOTES);
}


if (isset($_GET['subcat'])){
$subcat=htmlspecialchars(trim($_GET['subcat']),ENT_QUOTES);
}



$timecreated=time();
//restricted to their own category via cohort
if(!(is_siteadmin($id))){
	try{
		$course_cat=$DB->get_record_sql("SELECT b.id FROM mdl_user AS a
		JOIN mdl_course_categories AS b
		WHERE a.institution = b.name AND a.id=$id");
	} catch(Exception $e){
   		die("You are not allowed to use this service");
	}
}
print_r($course_cat);
die();

$coursecategory=$course_cat->id;

if(isset($subcat)){
try{
$course_cat=$DB->get_record_sql("SELECT id FROM mdl_course_categories WHERE parent=$coursecategory AND name='$subcat'");
} catch(Exception $e){
   die("There appears to be something wrong. Please check your query.");
}

$coursecategory=$course_cat->id;
}



$params=array('category'=>$coursecategory,
              'id'=>$userid,
              'filter'=>$filter);
   

/// REST CALL
//header('Content-Type: text/plain');
$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;

$curl = new ws_curl;
//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
$resp = $curl->post($serverurl . $restformat, $params);
print_r($resp);
die();
$new_course_id=(json_decode($resp));

if ((isset($new_course_id->exception))||($new_course_id===NULL)){
echo "You have errors. <br>";
print_r($new_course_id->message);
die();
} else {
print_r($new_course_id);
}








