<?php

$functions = array(
        'local_metrostargetcourse_get_course' => array(
                'classname' => 'local_metrostargetcourse_external',
                'methodname' => 'get_course',
                'classpath' => 'local/metrostargetcourse/externallib.php',
                'description' => 'Retrieves courses based upon filter input. Returns JSON string.',
                'type' => 'write',
                'shortname'=>'metrostargetcourse_get_course'
        )
);


$services = array(
        'Get course' => array(
                'functions' => array ('local_metrostargetcourse_get_course'),
                'restrictedcourses' => 1,
                'enabled'=>1,
        )
);