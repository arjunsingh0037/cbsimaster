<?php

$functions = array(
        'local_metrostaraddcourse_add_course' => array(
                'classname' => 'local_metrostaraddcourse_external',
                'methodname' => 'add_course',
                'classpath' => 'local/metrostaraddcourse/externallib.php',
                'description' => 'Adds a course. Returns new course ID.',
                'type' => 'write',
                'shortname'=>'metrostaraddcourse'
        )
);


$services = array(
        'Add course' => array(
                'functions' => array ('local_metrostaraddcourse_add_course'),
                'restrictedcourses' => 1,
                'enabled'=>1,
        )
);