<?php
require_once('../../../config.php');
require_once('../../../lib/accesslib.php');
require_once('curl.php');
global $CFG, $course, $DB;
$user_id = $_POST['userID'];
//$user_id=htmlspecialchars(trim($_POST['id']),ENT_QUOTES);
//$user_id='5';
if (!isset($user_id)) {
  echo "You must have a valid ID before you can create a course.";
  die();
}

/*debug*/
function debug($name, $bug, $line = '') {
  global $CFG;

  $log = "";

  if(is_array($bug)) {
    $bug = print_r($bug);
  }

  if(is_object($bug)) {
    $bug = var_dump($bug);
  }

  $log .= "\nLine: $line\n";
  $log .= "$name: \n $bug\n";

  file_put_contents($CFG->dataroot.'/debug.log', $log, FILE_APPEND | LOCK_EX);
};

$userid = 5;
$user_info          = $DB->get_record_sql("select * from mdl_user where id='" . $user_id . "'");
$user_info_password = $user_info->password;
$user_info_username = $user_info->username;
$get_token_url      = $CFG->wwwroot . "/login/token.php?username=" . $user_info_username . "&password=" . urlencode($user_info_password) . "&service=metrostar_add_course";
// print_r($get_token_url);
// die(__FILE__.":".__LINE__);
$users_token        = file_get_contents("$get_token_url");

//debug('$users_token', $users_token, __LINE__);
$users_token        = json_decode($users_token);
$token              = $users_token->token;
$title              = '';
$state              = '';
$all_tokens         = $DB->get_records_sql("select token from mdl_external_tokens where externalserviceid='2'");

/*debug*/

// debug('$user_info', $user_info, __LINE__);
// debug('$user_info_password', $user_info_password, __LINE__);
// debug('$user_info_username', $user_info_username, __LINE__);
// debug('$get_token_url', $get_token_url, __LINE__);
// debug('$users_token', $users_token, __LINE__);
// debug('$token', $token, __LINE__);
// debug('$title', $title, __LINE__);
// debug('$state', __LINE__);
// debug('$all_tokens', $all_tokens, __LINE__);

foreach ($all_tokens as $stored_token)
{
  $token_check[] = $stored_token->token;
}


// if (!in_array($token, $token_check))
//   {
//     die("You are not allowed to use this service");
//   }
$domainname   = $CFG->wwwroot;
/// FUNCTION NAME
$functionname = 'local_metrostaraddcourse_add_course';
// REST RETURNED VALUES FORMAT
$restformat   = 'json';
if (isset($_POST['startdate']))
  {
    $coursestartdate = htmlspecialchars(trim($_POST['startdate']), ENT_QUOTES);
  }
if (isset($_POST['coursenofsections']))
  {
    $coursenofsections = htmlspecialchars(trim($_POST['coursenofsections']), ENT_QUOTES);
  }
if (isset($_POST['coursefullname']))
  {
    $coursefullname = htmlspecialchars(trim($_POST['coursefullname']), ENT_QUOTES);
  }
if (isset($_POST['coursesummary']))
  {
    $coursesummary = htmlspecialchars(trim($_POST['coursesummary']), ENT_QUOTES);
  }
if (isset($_POST['lang']))
  {
    $courselang = htmlspecialchars(trim($_POST['lang']), ENT_QUOTES);
  }
if (isset($_POST['selecting_cohort_names']))
  {
    $selecting_cohort_names = htmlspecialchars(trim($_POST['selecting_cohort_names']), ENT_QUOTES);
  }
if (isset($_POST['cohort_selection_response']))
  {
    $cohort_selection_response = htmlspecialchars(trim($_POST['cohort_selection_response']), ENT_QUOTES);
  }
//mmcnairy debug, uncomment 84, 86, 87, 88 after fixing pwruser_acad_one is admin
//restricted to their own category via cohort
//if(!(is_siteadmin($user_id))){
$course_cat     = $DB->get_record_sql("select id from mdl_course_categories where idnumber='" . $user_info->institution . "'");
//} else {
//$course_cat=$DB->get_record_sql("select id from mdl_course_categories where name='Miscellaneous'");
//}
$coursecategory = $course_cat->id;
if (isset($_POST['category']))
  {
    $origcoursecategory = htmlspecialchars(trim($_POST['category']), ENT_QUOTES);
    //echo "select id from mdl_course_categories where name='".ucwords($origcoursecategory)."' and parent='$coursecategory'";
    //die();
    $course_cat         = $DB->get_record_sql("select id from mdl_course_categories where name='" . ucwords($origcoursecategory) . "' and parent='$coursecategory'");
    $coursecategory     = $course_cat->id;
  }
//print_r($coursecategory);
//die();
//forcing this
$courseformat    = 'topics';
//forcing shortname pattern here
$parent_category = $DB->get_record_sql("select id from mdl_course_categories where idnumber='" . $user_info->institution . "'");
$increment_by    = $DB->get_record_sql("SELECT max( cast( SUBSTRING_INDEX( shortname, '_', -1 ) AS unsigned ) ) as myid
FROM mdl_course");
$words           = explode(" ", $user_info->institution);
$acronym         = "";
foreach ($words as $w)
  {
    $acronym .= $w[0];
  }
$increment_by      = $increment_by->myid + 1;
$courseshortname   = $acronym . '_' . $increment_by;
$timecreated       = time();
$id                = null;
$category          = $coursecategory;
$sortorder         = 0;
$fullname          = $coursefullname;
$shortname         = $courseshortname;
$idnumber          = '';
$summary           = $coursesummary;
$summaryformat     = 0;
$format            = $courseformat;
$showgrades        = 0;
$newsitems         = 0;
$startdate         = $coursestartdate;
$marker            = 0;
$maxbytes          = 0;
$legacyfiles       = 0;
$showreports       = 0;
$visible           = 1;
$visibleold        = 0;
$groupmode         = 0;
$groupmodeforce    = 0;
$defaultgroupingid = 0;
$lang              = $courselang;
$calendartype      = '';
$theme             = '';
$timecreated       = $timecreated;
$timemodified      = 0;
$requested         = 0;
$enablecompletion  = 0;
$completionnotify  = 0;
$cacherev          = 0;
$params            = array(
    'selecting_cohort_names' => $selecting_cohort_names,
    'cohort_selection_response' => $cohort_selection_response,
    'coursenofsections' => $coursenofsections,
    //'coursenofsections' => 0,
    'id' => $id,
    'category' => $category,
    'sortorder' => $sortorder,
    'fullname' => $fullname,
    'shortname' => $shortname,
    'idnumber' => $idnumber,
    'summary' => $summary,
    'summaryformat' => $summaryformat,
    'format' => $format,
    'showgrades' => $showgrades,
    'newsitems' => $newsitems,
    'startdate' => $coursestartdate,
    'marker' => $marker,
    'maxbytes' => $maxbytes,
    'legacyfiles' => $legacyfiles,
    'showreports' => $showreports,
    'visible' => $visible,
    'visibleold' => $visibleold,
    'groupmode' => $groupmode,
    'groupmodeforce' => $groupmodeforce,
    'defaultgroupingid' => $defaultgroupingid,
    'lang' => $lang,
    'calendartype' => $calendartype,
    'theme' => $theme,
    'timecreated' => $timecreated = $timecreated,
    'timemodified' => $timemodified,
    'requested' => $requested,
    'enablecompletion' => $enablecompletion,
    'completionnotify' => $completionnotify,
    'cacherev' => $cacherev
);

/// REST CALL
//header('Content-Type: text/plain');
$serverurl         = $domainname . '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=' . $functionname;
$curl              = new ws_curl;
//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
$restformat        = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
$resp              = $curl->post($serverurl . $restformat, $params);
//mcnairy debug
// print_r($resp);
// die(__FILE__.":".__LINE__);
$new_course_id     = (json_decode($resp));
if ((isset($new_course_id->exception)) || ($new_course_id === NULL))
  {
    echo "You have errors in your course creation form. Please fix them by hitting your browser's back button and try again:<br>";
    print_r($new_course_id['message']);
    die();
  }
else
  {
    print_r($new_course_id);
  }
//redirect($CFG->wwwroot);
