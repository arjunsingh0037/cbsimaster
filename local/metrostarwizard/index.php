<?php
require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/../../lib/moodlelib.php');
require_once("HTML/QuickForm/Page.php");
require_once("HTML/QuickForm/Action.php");
require_once("HTML/QuickForm/Controller.php");
require_once("HTML/QuickForm/Action/Submit.php");
require_once("HTML/QuickForm/Action/Jump.php");
require_once("HTML/QuickForm/Action/Display.php");
require_once("HTML/QuickForm/Action/Direct.php");
require_once("HTML/QuickForm/Action/Next.php");
require_once("HTML/QuickForm/Action/Back.php");

global $DB, $USER, $CFG;
require_login();

//session_start();
//get rid of old wizard data
//unset($_SESSION['_Tabbed_container']);
class StepOne extends HTML_QuickForm_Page
{
    function buildForm()
    {
        global $CFG;

        $this->_formBuilt = true;
        $this->addElement('html', '<div id="container">');
        $this->addElement('html', '<div class="wizard">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<button class="exitWizard">' . get_string("exit_wizard", "local_metrostarwizard") . '</button>');
        $this->addElement('html', '<h1>' . get_string('wizard_name', 'local_metrostarwizard') . '</h1>');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButtonDisabled",
            "disabled" => "true"
        ));

        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');

        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('button', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap-->');
        $this->addElement('html', '<h2>' . get_string('page_one_question', 'local_metrostarwizard') . '</h2>');
        $this->addElement('html', '<div class="wizardChoices oneoption">');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p onclick=\'
        document.forms["pageone"].elements["pageAB"].value="pagetwoa";document.forms["pageone"].submit();
        \'>' . get_string("create_new_meeting_training", "local_metrostarwizard") . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '</div><!-- end wizardChoices -->');
        $this->addElement('hidden', 'pageAB', '', array(
            'id' => 'pageAB'
        ));
        $this->addElement('html', '<div class="navWrap bottomNav">');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButtonDisabled",
            "disabled" => "true"
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('button', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap-->');
        $this->addElement('html', '</div><!-- end wizard-->');
        $this->addElement('html', '</div><!-- end container -->');
        $this->setDefaultAction('next');
    }
}

class StepTwo extends HTML_QuickForm_Page
  {
    function buildForm()
      {
        global $CFG;
        $this->_formBuilt = true;
        $this->addElement('html', '<div id="container">');
        $this->addElement('html', '<div class="wizard">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<button class="exitWizard">' . get_string('exit_wizard', 'local_metrostarwizard') . '</button>');
        $this->addElement('html', '<h1>' . get_string('wizard_name', 'local_metrostarwizard') . '</h1>');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'prevwizpage', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton",
            "onclick" => "
        location.href='" . $CFG->wwwroot . "/local/metrostarwizard/index.php?_qf_pageone_display=true';

        "
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        if ($_SESSION['_Tabbed_container']['values']['pageone']['pageAB'] == 'pagetwoa')
          {
            $this->addElement('html', '<h2>' . get_string("page_two_question", "local_metrostarwizard") . '</h2>');
            $this->addElement('html', '<div class="wizardChoices">');
            $this->addElement('html', '<div class="wizardChoice">');
            $this->addElement('html', '<p onclick=\'
        document.forms["pagetwo"].elements["newCourseType"].value="Meeting";document.forms["pagetwo"].submit();
        \'>' . get_string('meeting', 'local_metrostarwizard') . '</p>');
            $this->addElement('html', '</div><!-- end wizardChoice -->');
            $this->addElement('html', '<div class="wizardChoice">');
            $this->addElement('html', '<p onclick=\'
        document.forms["pagetwo"].elements["newCourseType"].value="Training";document.forms["pagetwo"].submit();
        \'>' . get_string("training", "local_metrostarwizard") . '</p>');
            $this->addElement('html', '</div><!-- end wizardChoice -->');
            $this->addElement('html', '</div><!-- end wizardChoices -->');
            // $this->addElement('html', '<div style="clear:both">');
            $this->addElement('html', '<div class="navWrap bottomNav">');
            $this->addElement('html', '<div class="nav">');
            $this->addElement('button', 'prevwizpage', get_string('previous', 'local_metrostarwizard'), array(
                "class" => "wizardButton",
                "onclick" => "
        location.href='" . $CFG->wwwroot . "/local/metrostarwizard/index.php?_qf_pageone_display=true';

        "
            ));
            $this->addElement('html', '<ul>');
            $pages  = array();
            $myName = $current = $this->getAttribute('id');
            while (null !== ($current = $this->controller->getPrevName($current)))
              {
                $pages[] = $current;
              }
            $pages   = array_reverse($pages);
            $pages[] = $current = $myName;
            while (null !== ($current = $this->controller->getNextName($current)))
              {
                $pages[] = $current;
              }
            $x = 0;
            foreach ($pages as $pageName)
              {
                $x++;
                $this->addElement('html', '<li>');
                $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                    'style' => ''
                ));
                $this->addElement('html', '</li>');
              }
            $this->addElement('html', '</ul>');
            $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
                "class" => "wizardButton"
            ));
            $this->addElement('html', '</div><!-- end nav -->');
            $this->addElement('html', '</div><!-- end navwrap -->');
            $this->addElement('html', '</div><!-- end wizard-->');
            $this->addElement('html', '</div><!-- end container -->');
          }
        else
          {
            $this->addElement('html', '<h1>CBSI-Connect Course Creation Wizard</h1>');
            $this->addElement('html', '<h2>Choose an Existing Course</h2>');
            $this->addElement('html', '<div class="wizardChoices">');
            $this->addElement('html', '<div class="wizardChoice">');
            $this->addElement('html', '<div class="wizardChoice">');
            $this->addElement('html', '<select id="existing_course_category" onchange=\'
        if(this.value!="Select"){
            var userid=5;
            var pre_content=ajaxget(userid,this.value,"acad","existing_ccat_select","pagetwo",$CFG->wwwroot."/local/metrostargetcourse/client/client.php");
        }
        \'>
        <option>Select</option>
        <option>Meeting</option>
        <option>Training</option>
        <option>Technical</option>
        </select>');
            $this->addElement('html', '<br>');
            $this->addElement('html', '<span class="wizardInfo">Choose the type of course. Tailor it to your needs (time, duration, content, access, etc.)</span>');
            $this->addElement('hidden', 'changeCourseID', '', array(
                'id' => 'changeCourseID'
            ));
            $this->addElement('html', '</div><!-- end wizardChoice -->');
            $this->addElement('html', '</div><!-- end wizardChoices -->');
            $this->addElement('html', '<div id="existing_ccat_select"></div>');
            //$this->addElement('html', '<div style="clear:both">');
            $this->addElement('html', '<div class="navWrap bottomNav">');
            $this->addElement('html', '<div class="nav">');
            $this->addElement('button', 'prevwizpage', get_string('previous', 'local_metrostarwizard'), array(
                "class" => "wizardButton",
                "onclick" => "
        location.href='" . $CFG->wwwroot . "/local/metrostarwizard/index.php?_qf_pageone_display=true';

        "
            ));
            $this->addElement('html', '<ul>');
            $pages  = array();
            $myName = $current = $this->getAttribute('id');
            while (null !== ($current = $this->controller->getPrevName($current)))
              {
                $pages[] = $current;
              }
            $pages   = array_reverse($pages);
            $pages[] = $current = $myName;
            while (null !== ($current = $this->controller->getNextName($current)))
              {
                $pages[] = $current;
              }
            $x = 0;
            foreach ($pages as $pageName)
              {
                $x++;
                $this->addElement('html', '<li>');
                $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                    'style' => ''
                ));
                $this->addElement('html', '</li>');
              }
            $this->addElement('html', '</ul>');
            $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
                "class" => "wizardButton"
            ));
            $this->addElement('html', '</div><!-- end nav -->');
            $this->addElement('html', '</div><!-- end navwrap -->');
            $this->addElement('html', '</div><!-- end wizard-->');
            $this->addElement('html', '</div><!-- end container -->');
          }
        $this->addElement('hidden', 'newCourseType', '', array(
            'id' => 'newCourseType'
        ));
        $this->setDefaultAction('next');
      }
  }
/*
class StepThree extends HTML_QuickForm_Page
  {
    function buildForm()
      {
        global $CFG;
        $this->_formBuilt = true;
        $this->addElement('html', '<div id="container">');
        $this->addElement('html', '<div class="wizard">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<button class="exitWizard">' . get_string('exit_wizard', 'local_metrostarwizard') . '</button>');
        $this->addElement('html', '<h1>' . get_string('wizard_name', 'local_metrostarwizard') . '</h1>');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'prevwizpage', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton",
            "onclick" => "
        location.href='" . $CFG->wwwroot . "/local/metrostarwizard/index.php?_qf_pagetwo_display=true';
         "
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        $this->addElement('hidden', 'cohort_selection_response', '', array(
            'id' => 'cohort_selection_response'
        ));
        $this->addElement('hidden', 'cohort_selections', '', array(
            'id' => 'cohort_selections'
        ));
        $this->addElement('html', '<h2>' . get_string('page_three_question', 'local_metrostarwizard') . '</h2>');
        $this->addElement('html', '<div class="wizardChoices">');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p onclick=\'
        document.forms["pagethree"].elements["cohort_selection_response"].value="No";
        document.forms["pagethree"].submit();
        \'>' . get_string('no_private_course', 'local_metrostarwizard') . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p onclick=\'
        document.forms["pagethree"].elements["cohort_selection_response"].value="Yes All";
        document.forms["pagethree"].submit();
                \'>' . get_string('yes_all', 'local_metrostarwizard') . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '<div class="wizardChoice showHiddenCountries">');
        $this->addElement('html', '<p onclick=\'
        document.forms["pagethree"].elements["cohort_selection_response"].value="Yes Selected";
        var pre_content=' . file_get_contents($CFG->wwwroot . "/local/metrostargetcohort/client/client.php") . ';
        var content="";

        for(var x = 0; x < pre_content.length; x++){
            content += "<br /><div class=\"checkmark\"><input type=\"checkbox\" onclick=\"if(this.checked){gatherSelected(this.value);}\" id=\"selecting_cohort_names["+x+"]\" name=\"selecting_cohort_names["+x+"]\" value=\""+pre_content[x].id+"\"><span class=\"lineUp "+pre_content[x].name+" \">"+pre_content[x].name+"</span></div>";
        }

        document.getElementById("cohort_selection").innerHTML = content;
        \'>' . get_string('yes_selected', 'local_metrostarwizard') . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '<div id="cohort_selection"></div>');
        $this->addElement('html', '</div><!-- end wizardChoices -->');
        $this->addElement('html', '<div class="navWrap bottomNav">');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        $this->addElement('html', '</div><!-- end wizard-->');
        $this->addElement('html', '</div><!-- end container -->');
        $this->setDefaultAction('next');
      }
  }
*/
class StepFour extends HTML_QuickForm_Page
  {
    function buildForm()
      {
        global $CFG;
        $this->_formBuilt = true;
        $this->addElement('html', '<div id="container">');
        $this->addElement('html', '<div class="wizard">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<button class="exitWizard">' . get_string('exit_wizard', 'local_metrostarwizard') . '</button>');
        $this->addElement('html', '<h1>' . get_string('wizard_name', 'local_metrostarwizard') . '</h1>');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'prevwizpage', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton",
            "onclick" => "
        location.href='" . $CFG->wwwroot . "/local/metrostarwizard/index.php?_qf_pagethree_display=true';

        "
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        if ($_SESSION["_Tabbed_container"]["values"]["pageone"]["pageAB"] == 'pagetwob')
          {
            $cid = $_SESSION["_Tabbed_container"]["values"]["pagetwo"]["changeCourseID"];
            global $DB;
            $get_saved_data = $DB->get_record_sql("select * from mdl_course where id=$cid");
            $this->addElement('html', '</ul>');
            $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
                "class" => "wizardButton"
            ));
            $this->addElement('html', '</div><!-- end nav -->');
            $this->addElement('html', '</div><!-- end navwrap -->');
            $this->addElement('html', '<div class="formalign">');
            $this->addElement('text', 'course_name', get_string('course_name', 'local_metrostarwizard'), array(
                'value' => $get_saved_data->fullname,
                'class' => 'courseName'
            ));
            $this->addElement('html', '<P>&nbsp;</P>');
            $lang   = $get_saved_data->lang;
            $select = $this->addElement('select', 'course_language', get_string('course_language', 'local_metrostarwizard'), array(
                'Select',
                'English',
                'Spanish'
            ), array(
                "class" => "languageName"
            ));
            $select->setSelected($lang);
            $this->addElement('html', '<P>&nbsp;</P>');
            $this->addElement('textarea', 'course_description', get_string('course_description', 'local_metrostarwizard'), 'wrap="virtual" rows="20" cols="50"');
            $this->addElement('html', '<P>&nbsp;</P>');
            if ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == 'Meeting')
              {
                $noofsections = '1';
              }
            elseif ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == 'Training')
              {
                $noofsections = '10';
              }
            $this->addElement('hidden', 'course_sections', $noofsections);
            $this->addElement('html', '</div><!-- end formalign -->');
            $this->addElement('html', '<div class="navWrap bottomNav">');
            $this->addElement('html', '<div class="nav">');
            $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
                "class" => "wizardButton"
            ));
            $this->addElement('html', '<ul>');
            $pages  = array();
            $myName = $current = $this->getAttribute('id');
            while (null !== ($current = $this->controller->getPrevName($current)))
              {
                $pages[] = $current;
              }
            $pages   = array_reverse($pages);
            $pages[] = $current = $myName;
            while (null !== ($current = $this->controller->getNextName($current)))
              {
                $pages[] = $current;
              }
            $x = 0;
            foreach ($pages as $pageName)
              {
                $x++;
                $this->addElement('html', '<li>');
                $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                    'style' => ''
                ));
                $this->addElement('html', '</li>');
              }
            $this->addElement('html', '</ul>');
            $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
                "class" => "wizardButton"
            ));
            $this->addElement('html', '</div><!-- end nav -->');
            $this->addElement('html', '</div><!-- end navwrap -->');
            $this->addElement('html', '</div><!-- end wizard-->');
            $this->addElement('html', '</div><!-- end container -->');
          }
        elseif ($_SESSION["_Tabbed_container"]["values"]["pageone"]["pageAB"] == 'pagetwoa')
          {
            $this->addElement('html', '<div class="formalign">');
            $this->addElement('text', 'course_name', get_string('course_name', 'local_metrostarwizard'), array(
                'value' => $get_saved_data->fullname,
                'class' => 'courseName'
            ));
            $this->addElement('html', '<P>&nbsp;</P>');
            $this->addElement('select', 'course_language', get_string('course_language', 'local_metrostarwizard'), array(
                'Select',
                'English',
                'Spanish'
            ), array(
                'class' => 'languageName'
            ));
            $this->addElement('html', '<P>&nbsp;</P>');
            $this->addElement('textarea', 'course_description', get_string('course_description', 'local_metrostarwizard'), 'wrap="virtual" rows="20" cols="50"');
            $this->addElement('html', '<P>&nbsp;</P>');
            if ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == 'Meeting')
              {
                $noofsections = '1';
              }
            elseif ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == 'Training')
              {
                $noofsections = '10';
              }
            $this->addElement('hidden', 'course_sections', $noofsections);
            $this->addElement('html', '</div><!-- end formalign -->');
            $this->addElement('html', '<div class="navWrap bottomNav">');
            $this->addElement('html', '<div class="nav">');
            $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
                "class" => "wizardButton"
            ));
            $this->addElement('html', '<ul>');
            $pages  = array();
            $myName = $current = $this->getAttribute('id');
            while (null !== ($current = $this->controller->getPrevName($current)))
              {
                $pages[] = $current;
              }
            $pages   = array_reverse($pages);
            $pages[] = $current = $myName;
            while (null !== ($current = $this->controller->getNextName($current)))
              {
                $pages[] = $current;
              }
            $x = 0;
            foreach ($pages as $pageName)
              {
                $x++;
                $this->addElement('html', '<li>');
                $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                    'style' => ''
                ));
                $this->addElement('html', '</li>');
              }
            $this->addElement('html', '</ul>');
            $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
                "class" => "wizardButton"
            ));
            $this->addElement('html', '</div><!-- end nav -->');
            $this->addElement('html', '</div><!-- end navwrap -->');
            $this->addElement('html', '</div><!-- end wizard-->');
            $this->addElement('html', '</div><!-- end container -->');
          }
        $this->setDefaultAction('next');
      }
  }
class StepSix extends HTML_QuickForm_Page
  {
    function buildForm()
      {
        global $CFG;
        $this->_formBuilt = true;
        $this->addElement('html', '<div id="container">');
        $this->addElement('html', '<div class="wizard">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<button class="exitWizard">' . get_string('exit_wizard', 'local_metrostarwizard') . '</button>');
        $this->addElement('html', '<h1>' . get_string('wizard_name', 'local_metrostarwizard') . '</h1>');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'prevwizpage', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton",
            "onclick" => "
        location.href='" . $CFG->wwwroot . "/local/metrostarwizard/index.php?_qf_pagefour_display=true';

        "
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        $this->addElement('html', '<h2>' . get_string("page_six_question", "local_metrostarwizard") . '</h2>');
        $this->addElement('html', '<div class="wizardChoices">');
        $this->addElement('hidden', 'pagesetdatetime', '');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p onclick=\'
        document.forms["pagesix"].elements["pagesetdatetime"].value="No";
        document.forms["pagesix"].submit();
         ;\'>' . get_string("page_six_no", "local_metrostarwizard") . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p class="datesYes" onclick=\'
        document.forms["pagesix"].elements["pagesetdatetime"].value="Yes";
        document.getElementById("class_begin_date").style.display="block";
        \'>' . get_string("page_six_yes", "local_metrostarwizard") . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<div class="formWrap" id="class_begin_date" style="display:none;">');
        $this->addElement('text', 'class_begin_datetxt', get_string("page_six_date_question", "local_metrostarwizard"), array(
            "id" => "class_begin_datetxt"
        ));
        $this->addElement('html', '<img src="http://cbsi-connect.net/theme/msu/pix/ClockGraphic.png" width="39" height="39" class="clock" />');
        $this->addElement('text', 'class_begin_timetxt', ' Choose a time:', array(
            "id" => "class_begin_timetxt"
        ));
        $this->addElement('html', '</div><!-- end formWrap -->');
        $this->addElement('html', '</div><!-- end wizardChoices -->');
        $this->addElement('html', '</div><!-- end wizardChoices -->');
        $this->addElement('html', '<div style="clear:both">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('submit', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        $this->addElement('html', '</div><!-- end wizard-->');
        $this->addElement('html', '</div><!-- end container -->');
        $this->setDefaultAction('next');
      }
  }
class StepSeven extends HTML_QuickForm_Page
  {
    function buildForm()
      {
        $this->_formBuilt = true;
        $this->addElement('html', '<div id="container">');
        $this->addElement('html', '<div class="wizard">');
        $this->addElement('html', '<div style="clear:both">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        //$this->addElement('submit', 'next', get_string('next','local_metrostarwizard'), array("class"=>"wizardButton"));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        $this->addElement('hidden', 'where_to_finish', '');
        $this->addElement('html', '<h2>' . get_string("page_seven_question", "local_metrostarwizard") . '</h2>');
        $this->addElement('html', '<div class="wizardLeft">');
        $this->addElement('html', '<span class="or">Or</span>');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p class="" onclick=\'document.forms["pageseven"].elements["where_to_finish"].value="save_go_home";document.forms["pageseven"].submit("process"); \'>' . get_string('save_go_home', 'local_metrostarwizard') . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p  class="" onclick=\'document.forms["pageseven"].elements["where_to_finish"].value="delete_go_home";document.forms["pageseven"].submit(); \'>' . get_string('delete_go_home', 'local_metrostarwizard') . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '</div><!-- end wizardLeft -->');
        $this->addElement('html', '<div class="wizardRight">');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p  class="" onclick=\'document.forms["pageseven"].elements["where_to_finish"].value="save_go_to_course";document.forms["pageseven"].submit();\'>' . get_string('save_start', 'local_metrostarwizard') . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '</div><!-- end wizardright -->');
        $this->addElement('html', '<div style="clear:both">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        // $this->addElement('submit', 'next', get_string('next','local_metrostarwizard'), array("class"=>"wizardButton"));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap -->');
        $this->addElement('html', '</div><!-- end wizard-->');
        $this->addElement('html', '</div><!-- end container -->');
        $this->setDefaultAction('process');
      }
  }
class PageFirstActionNext extends HTML_QuickForm_Action_Next
  {
    function perform(&$page, $actionName)
      {
        // save the form values and validation status to the session
        $page->isFormBuilt() or $page->buildForm();
        $pageName = $page->getAttribute('id');
        $data =& $page->controller->container();
        $data['values'][$pageName] = $page->exportValues();
        /*if (PEAR::isError($valid = $page->validate())) {
        return $valid;
        }*/
        if ($valid = $page->validate())
          {
            return $valid;
          }
        $data['valid'][$pageName] = $valid;
        // Modal form and page is invalid: don't go further
        if ($page->controller->isModal() && !$data['valid'][$pageName])
          {
            return $page->handle('display');
          }
        $nextName = $data['values'][$pageName]['PageAB'];
        if (empty($nextName))
          {
            $nextName = 'page1';
          }
        if ($nextName == 'page2a')
          {
            $data['valid']['page2b'] = true;
          }
        else
          {
            $data['valid']['page2a'] = true;
          }
        $next =& $page->controller->getPage($nextName);
        $next->handle('jump');
      }
  }
class PageSecondActionNext extends HTML_QuickForm_Action_Next
  {
    function perform(&$page, $actionName)
      {
        // save the form values and validation status to the session
        $page->isFormBuilt() or $page->buildForm();
        $pageName = $page->getAttribute('id');
        $data =& $page->controller->container();
        $data['values'][$pageName] = $page->exportValues();
        /*if (PEAR::isError($valid = $page->validate())) {
        return $valid;
        }*/
        $data['valid'][$pageName]  = TRUE; //$valid;
        // Modal form and page is invalid: don't go further
        if ($page->controller->isModal() && !$data['valid'][$pageName])
          {
            return $page->handle('display');
          }
        // More pages?
        $next =& $page->controller->getPage('page3');
        $next->handle('jump');
      }
  }
class PageSecondActionBack extends HTML_QuickForm_Action_Back
  {
    function perform(&$page, $actionName)
      {
        // save the form values and validation status to the session
        $page->isFormBuilt() or $page->buildForm();
        $pageName = $page->getAttribute('id');
        $data =& $page->controller->container();
        $data['values'][$pageName] = $page->exportValues();
        if (!$page->controller->isModal())
          {
            /*if (PEAR::isError($valid = $page->validate())) {
            return $valid;
            }*/
            $data['valid'][$pageName] = TRUE; // $valid;
          }
        $prev =& $page->controller->getPage('page1');
        $prev->handle('jump');
      }
  }
class PageThirdActionBack extends HTML_QuickForm_Action_Back
  {
    function perform(&$page, $actionName)
      {
        $page->isFormBuilt() or $page->buildForm();
        $pageName = $page->getAttribute('id');
        $data =& $page->controller->container();
        $data['values'][$pageName] = $page->exportValues();
        if (!$page->controller->isModal())
          {
            /*if (PEAR::isError($valid = $page->validate())) {
            return $valid;
            }*/
            if ($valid = $page->validate())
              {
                return $valid;
              }
            $data['valid'][$pageName] = TRUE; //$valid;
          }
        $prev =& $page->controller->getPage($data['values']['page1']['PageAB']);
        //$prev =& $page->controller->getPage($data['values']['page1']);
        $prev->handle('jump');
      }
  }
class ActionDisplay extends HTML_QuickForm_Action_Display
  {
    function _renderForm(&$page)
      {
        $renderer =& $page->defaultRenderer();
        $renderer->setElementTemplate("\n\t<tr>\n\t\t<td align=\"right\" valign=\"top\" colspan=\"2\">{element}</td>\n\t</tr>", 'tabs');
        $renderer->setFormTemplate(<<<_HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Wizard Template</title>
<link rel="stylesheet" type="text/css" href="wizard.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="wizard.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />

<script>
/* worked when we wanted step 3
function gatherSelected(val){
       var existing_vals=document.forms["pagethree"].elements["cohort_selections"].value;
       var new_val = existing_vals+","+val;
       document.forms["pagethree"].elements["cohort_selections"].value=new_val.replace(/(^,)|(,$)/g, "");

       return true;

}
*/
/*
function gatherselected(frm){
    var checkedBoxes = [];

    //For each checkbox see if it has been checked, record the value.
    for (i = 0; i < frm.selecting_cohort_names.length; i++)
      if (frm.selecting_cohort_names[i].checked) {
        checkedBoxes.push(frm.selecting_cohort_names[i].value);
    }

    return checkedBoxes;
}
*/
</script>


</head>

<body>
<form{attributes}>

{content}

</form>
</body>
</html>

_HTML
            );
        $page->display();
      }
  }
class ActionProcess extends HTML_QuickForm_Action
  {
    function perform(&$page, $actionName)
      {
        // print_r($_SESSION['_Tabbed_container']['values']["pagetwo"]["changeCourseID"]);
        // die(__FILE__.":".__LINE__."\n<pre>".print_r($_SESSION)."</pre>");

        global $CFG, $USER;
        if ((isset($_POST['where_to_finish'])) && ($_POST['where_to_finish'] == 'save_go_home'))
          {
            //$courseid          = $_SESSION["_Tabbed_container"]["values"]["pagetwo"]["changeCourseID"];
            $coursefullname    = urlencode(trim($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_name"]));
            $coursesummary     = urlencode($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_description"]);
            $coursenofsections = urlencode($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_sections"]);
            $category          = urlencode($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"]);
            $lang              = urlencode($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_language"]);
            $cohort_selection_response = urlencode("No");
            $selecting_cohort_names    = urlencode('Null');
            // if (urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selection_response"]) == 'No')
            //   {
            //     $cohort_selection_response = urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selection_response"]);
            //     $selecting_cohort_names    = urlencode('Null');
            //   }
            // else
            //   {
            //     $cohort_selection_response = urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selection_response"]);
            //     $selecting_cohort_names    = urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selections"]);
            //   }
            if ($_SESSION["_Tabbed_container"]["values"]["pagesix"]["class_begin_datetxt"])
              {
                $startdate = urlencode($_SESSION["_Tabbed_container"]["values"]["pagesix"]["class_begin_datetxt"]);
                $startdate .= " " . urlencode($_SESSION["_Tabbed_container"]["values"]["pagesix"]["class_begin_timetxt"]);
              }
            else
              {
                $startdate = 0;
              }
            $url     = $CFG->wwwroot . "/local/metrostaraddcourse/client/client.php";
            $options = array(
                "coursefullname" => $coursefullname,
                "coursesummary" => $coursesummary,
                "category" => $category,
                "lang" => $lang,
                "coursenofsections" => $coursenofsections,
                "startdate" => $startdate,
                "cohort_selection_response" => $cohort_selection_response,
                "selecting_cohort_names" => $selecting_cohort_names,
                "userID" => $USER->id
            );

            // print_r($options);
            // die(__FILE__.":".__LINE__);

            require_once('curl.php');

            $curl          = new ws_curl;
            $resp          = $curl->post($url, $options);
            // print_r(json_decode($resp));
            // die(__FILE__.":".__LINE__);
            $mynewcourseid = (json_decode($resp));
            //die("my new course id: {$mynewcourseid}. <br>" . __FILE__.":".__LINE__);

            if (isset($mynewcourseid))
              {
                //header( 'Location:'.$CFG->wwwroot) ;
                echo "<script language=\"javascript\">";
                if ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == "Training")
                  {
                    echo "window.location.href='" . $CFG->wwwroot . "';";
                  }
                if ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == "Meeting")
                  {
                    echo "window.location.href='" . $CFG->wwwroot . "/course/modedit.php?add=bigbluebuttonbn&type=&course=$mynewcourseid&section=0&return=0&sr=';";
                  }
                echo "</script>";
              }
            else
              {
                echo "Unable to create a new course. Please contact the Portal Administrator";
              }
          }
        elseif ((isset($_POST['where_to_finish'])) && ($_POST['where_to_finish'] == 'delete_go_home'))
          {
            //header( 'Location:'.$CFG->wwwroot) ;
            echo "<script language=\"javascript\">";
            echo "window.location.href='" . $CFG->wwwroot . "';";
            echo "</script>";
          }
        elseif ((isset($_POST['where_to_finish'])) && ($_POST['where_to_finish'] == 'save_go_to_course'))
          {
            $coursefullname    = urlencode(trim($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_name"]));
            $coursesummary     = urlencode($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_description"]);
            $coursenofsections = urlencode($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_sections"]);
            $category          = urlencode($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"]);
            $lang              = urlencode($_SESSION["_Tabbed_container"]["values"]["pagefour"]["course_language"]);
            $cohort_selection_response = urlencode("No");
            $selecting_cohort_names    = urlencode('Null');

            // if (urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selection_response"]) == 'No')
            //   {
            //     $cohort_selection_response = urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selection_response"]);
            //     $selecting_cohort_names    = urlencode('Null');
            //   }
            // else
            //   {
            //     $cohort_selection_response = urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selection_response"]);
            //     $selecting_cohort_names    = urlencode($_SESSION["_Tabbed_container"]["values"]["pagethree"]["cohort_selections"]);
            //   }
            if ($_SESSION["_Tabbed_container"]["values"]["pagesix"]["class_begin_datetxt"])
              {
                $startdate = urlencode($_SESSION["_Tabbed_container"]["values"]["pagesix"]["class_begin_datetxt"]);
                $startdate .= " " . urlencode($_SESSION["_Tabbed_container"]["values"]["pagesix"]["class_begin_timetxt"]);
              }
            else
              {
                $startdate = 0;
              }
            $url     = $CFG->wwwroot . "/local/metrostaraddcourse/client/client.php";
            $options = array(
                "coursefullname" => $coursefullname,
                "coursesummary" => $coursesummary,
                "category" => $category,
                "lang" => $lang,
                "coursenofsections" => $coursenofsections,
                "startdate" => $startdate,
                "cohort_selection_response" => $cohort_selection_response,
                "selecting_cohort_names" => $selecting_cohort_names,
                "userID" => $USER->id
            );

            require_once('curl.php');
            $curl          = new ws_curl;
            $resp          = $curl->post($url, $options);

            $mynewcourseid = (json_decode($resp));
            if (isset($mynewcourseid))
              {
                //header('Location:'.$CFG->wwwroot. "/course/view.php?id=$courseid");
                echo "<script language=\"javascript\">";
                if ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == "Training")
                  {
                    echo "window.location.href='" . $CFG->wwwroot . "/course/view.php?id=$mynewcourseid';";
                  }
                if ($_SESSION["_Tabbed_container"]["values"]["pagetwo"]["newCourseType"] == "Meeting")
                  {
                    echo "window.location.href='" . $CFG->wwwroot . "/course/modedit.php?add=bigbluebuttonbn&type=&course=$mynewcourseid&section=0&return=0&sr=';";
                  }
                echo "</script>";
              }
          }
      }
  }
$tabbed =& new HTML_QuickForm_Controller('Tabbed', false);
$tabbed->addPage(new StepOne('pageone'));
$tabbed->addPage(new StepTwo('pagetwo'));
//$tabbed->addPage(new StepThree('pagethree'));
$tabbed->addPage(new StepFour('pagefour'));
$tabbed->addPage(new StepSix('pagesix'));
$tabbed->addPage(new StepSeven('pageseven'));
$tabbed->addAction('pageone', new HTML_QuickForm_Action_Direct());
$tabbed->addAction('pagetwo', new HTML_QuickForm_Action_Direct());
//$tabbed->addAction('pagethree', new HTML_QuickForm_Action_Direct());
$tabbed->addAction('pagefour', new HTML_QuickForm_Action_Direct());
//$tabbed->addAction('pagefive', new HTML_QuickForm_Action_Direct());
$tabbed->addAction('pagesix', new HTML_QuickForm_Action_Direct());
$tabbed->addAction('pageseven', new HTML_QuickForm_Action_Direct());
$tabbed->addAction('next', new HTML_QuickForm_Action_Next());
$tabbed->addAction('back', new HTML_QuickForm_Action_Back());
$tabbed->addAction('jump', new HTML_QuickForm_Action_Jump());
$tabbed->addAction('process', new ActionProcess());
$tabbed->addAction('display', new ActionDisplay());
$tabbed->run();
?>
